<?php
/**
 * Copyright 2017 GoneTone
 *
 * Line Bot
 * 範例 Example Bot 執行主文件
 *
 * 此範例 GitHub 專案：https://github.com/GoneTone/line-example-bot-php
 * 官方文檔：https://developers.line.biz/en/reference/messaging-api/
 */
ini_set("display_errors", "On");
// error_reporting(0); // 不顯示錯誤 (Debug 時請註解掉)
date_default_timezone_set("Asia/Taipei"); // 設定時區為台北時區

require_once('LINEBotTiny.php');
require_once('includes/product.php');
require_once('includes/cart.php');

$botType = 0;
$botData = 0;
$botPrice = 0;
$botKG = 0;
$botId = 0;

$base_url = "https://faceborder.at.tw/oyster/";

/*========================================================*/
// 取得config Id 的Toekn Secret
/*========================================================*/
if (file_exists(__DIR__ . '/config.ini')) {
	$config = parse_ini_file("config.ini", true); // 解析配置檔
	if ($config['Channel']['Token'] == Null || $config['Channel']['Secret'] == Null) {
		error_log("config.ini 配置檔未設定完全！", 0); // 輸出錯誤
	} else {
		$channelAccessToken = $config['Channel']['Token'];
		$channelSecret = $config['Channel']['Secret'];
	}
} else {
	$configFile = fopen("config.ini", "w");
	$configFileContent = '; Copyright 2019 GoneTone
;
; Line Bot
; 範例 Example Bot 配置文件
;
; 此範例 GitHub 專案：https://github.com/GoneTone/line-example-bot-php
; 官方文檔：https://developers.line.biz/en/reference/messaging-api/

[Channel]
; 請在雙引號內輸入您的 Line Bot "Channel access token"
Token = ""

; 請在雙引號內輸入您的 Line Bot "Channel secret"
Secret = ""
';
	fwrite($configFile, $configFileContent); // 建立文件並寫入
	fclose($configFile); // 關閉文件
	error_log("config.ini 配置檔建立成功，請編輯檔案填入資料！", 0); // 輸出錯誤
}

/*========================================================*/
// 將各自LINE的專有ID進行加密
/*========================================================*/
function public_encrypt($plain_text){
    $fp = fopen(__DIR__.'/assets/public.key', "r");
    $pub_key = fread($fp, 8192);
    fclose($fp);
    $pub_key_res = openssl_get_publickey($pub_key);
    if(!$pub_key_res) {
        throw new Exception('Public Key invalid');
    }
    openssl_public_encrypt($plain_text, $crypt_text, $pub_key_res, OPENSSL_PKCS1_OAEP_PADDING);
    openssl_free_key($pub_key_res);    
    $crypt_text = str_replace('/','---',base64_encode($crypt_text));
    $crypt_text = str_replace('+','___',$crypt_text);
    $crypt_text = str_replace('=',':::',$crypt_text);
    return $crypt_text; // 加密後的內容為 binary 透過 base64_encode() 轉換為 string 方便傳輸
}

/*========================================================*/
// 將各自LINE的專有ID去查詢檔案，並將它目前的狀態取得出來
/*========================================================*/
function getPageIni($userLineKey){
	if (file_exists(__DIR__ . '/session/'.$userLineKey.'.ini')) {
		global $botType,$botData,$botId,$botPrice,$botKG;
		$config = parse_ini_file("session/".$userLineKey.".ini", true); // 解析配置檔
		$botType = $config['Channel']['botType'];
		$botData = $config['Channel']['botData'];
		$botId = $config['Channel']['botId'];
		$botPrice = $config['Channel']['botPrice'];
		$botKG = $config['Channel']['botKG'];
	}
}

/*========================================================*/
// 將每個人目前點擊的狀態儲存起來，名稱為各自LINE的專有ID
/*========================================================*/
function writePageIni($userLineKey,$botType,$botData,$botId,$botPrice,$botKG){
	$configFile = fopen(__DIR__."/session/".$userLineKey.".ini", "w");
	$configFileContent = 
		'[Channel]
		botType = ' . $botType .'
		botData = ' . $botData .'
		botId = ' . $botId.'
		botPrice = ' . $botPrice.'
		botKG = ' . $botKG;
	fwrite($configFile, $configFileContent); // 建立文件並寫入
	fclose($configFile); // 關閉文件
}

/*========================================================*/
// 將UTF-32轉UTF-8的Line Emoji方法(表情符號)
/*========================================================*/
function emoji($code){
	$bin = hex2bin(str_repeat('0', 8 - strlen($code)) . $code);
	$emoticon =	mb_convert_encoding($bin, 'UTF-8', 'UTF-32BE');
	return $emoticon;
} 

function getURL($base_url,$url,$data = null){
	$test = $base_url.$url;
	for($i=0 ;$i<count($data) ;$i++){
		$test = $test . "/" . $data[$i];
	}
	$html = file_get_contents($test);
	$json = json_decode($html,true);
	return $json;
}
	
$client = new LINEBotTiny($channelAccessToken, $channelSecret);
foreach ($client->parseEvents() as $event) {
	getPageIni($event['source']['userId']);
	switch ($event['type']) {
		case 'message':
			$message = $event['message'];

			/*========================================================*/
			// 回傳userId
			/*========================================================*/
			if ($message['text'] == "儲存"){
				$client->replyMessage(array(
					'replyToken' => $event['replyToken'],
					'messages' => array(
						array(
							'type' => 'text', // 訊息類型 (文字)
							'text' => $event['source']['userId'] // 回復訊息
						)
					)
				));
			}

			/*========================================================*/
			// 登入 -> 傳Line ID到購物網站的登入畫面
			/*========================================================*/
			if ($message['text'] == "登入") {
				$botType = "login";
				$botData = "0";
				$botId = "0";
				$botPrice = "0";
				$botKG = "0";

				writePageIni($event['source']['userId'],$botType,$botData,$botId,$botPrice,$botKG);

				$client->replyMessage(array(
			        'replyToken' => $event['replyToken'],
			        'messages' => array(
			            array(
			                'type' => 'text', // 訊息類型 (文字)
			                'text' => '請向文蛤工社索取店家編號，請直接輸入店家編號(Ex: ef8112ft)' // 回復訊息
			            )
			        )
			    ));
			}

			/*========================================================*/
			// 登入 -> 傳Line ID到購物網站的登入畫面
			/*========================================================*/
			if ($botType == "login" && $message['text'] != "登入") {
				$json = getURL($base_url,"LineAPI/login/",array($message['text'],$event['source']['userId']));

				$botType = "none";
				$botData = "0";
				$botId = "0";
				$botPrice = "0";
				$botKG = "0";

				if(count($json) == 0){
					$client->replyMessage(array(
				        'replyToken' => $event['replyToken'],
				        'messages' => array(
				            array(
				                'type' => 'text', // 訊息類型 (文字)
				                'text' => '輸入錯誤\r\n請向文蛤工社索取店家編號，請直接輸入店家編號(Ex: ef8112ft)，' // 回復訊息
				            )
				        )
				    ));
				}else{
					$botId = $json[0]['user_key'];

					$client->replyMessage(array(
				        'replyToken' => $event['replyToken'],
				        'messages' => array(
				            array(
				                'type' => 'text', // 訊息類型 (文字)
				                'text' => '登入成功！' // 回復訊息
				            )
				        )
				    ));
				}

				writePageIni($event['source']['userId'],$botType,$botData,$botId,$botPrice,$botKG);
			}

			/*========================================================*/
			// 商品介紹:輪播 介紹店家內的每個商品，有上/下一頁 利用字串分割的方式，將店家名稱傳至後端搜尋
			/*========================================================*/
			if($message['text'] == "商品介紹"){
				$json = getURL($base_url,"LineAPI/getProductInfo/");

				$botType = "product";
				$botData = "0";
				$botPrice = "0";
				$botKG = "0";

				writePageIni($event['source']['userId'],$botType,$botData,$botId,$botPrice,$botKG);

				$actions = array(array("postback","斤數","斤數:","product_key","請輸入您要的斤數"),array("postback","價錢","價錢:","product_key","請輸入您要的價錢"));

				$output = initFlexProductInfo($event['source']['userId'],$json,"product_img","product_name","product_price","product_unit",$actions);

				$client->replyMessage(array(
					'replyToken' => $event['replyToken'],
					'messages' => array($output)
				));
			}

			/*========================================================*/
			// 輸入蛤蜊的重量
			/*========================================================*/
			if($botType == "KG" && $message['text'] != "請輸入您要的斤數"){
				if($botId == "0"){
					$client->replyMessage(array(
						'replyToken' => $event['replyToken'],
						'messages' => array(
							array(
								'type' => 'text', // 訊息類型 (文字)
								'text' => "找不到使用者，請先進行登入".emoji("100088") // 回復訊息
							)
						)
					));
				}else if($botId != "0"){
					$url = $base_url."LineAPI/addCartKG/".$botId."/".$botData."/".$botPrice."/".$message['text'];
					$html = file_get_contents($url);
					$json = json_decode($html,true);

					$botType = "none";
					$botData = "0";
					$botPrice = "0";
					$botKG = "0";

					writePageIni($event['source']['userId'],$botType,$botData,$botId,$botPrice,$botKG);

					
					$client->replyMessage(array(
						'replyToken' => $event['replyToken'],
						'messages' => array(
							array(
								'type' => 'text', // 訊息類型 (文字)
								'text' => "成功新增至購物車".emoji("100088") // 回復訊息
							)
						)
					));
				}
			}

			/*========================================================*/
			// 輸入蛤蜊的價錢
			/*========================================================*/
			if($botType == "price" && $message['text'] != "請輸入您要的價錢"){
				if($botId == "0"){
					$client->replyMessage(array(
						'replyToken' => $event['replyToken'],
						'messages' => array(
							array(
								'type' => 'text', // 訊息類型 (文字)
								'text' => "找不到使用者，請先進行登入".emoji("100088") // 回復訊息
							)
						)
					));
				}else if($botId != "0"){
					$url = $base_url."LineAPI/addCartPrice/".$botId."/".$botData."/".$botPrice."/".$message['text'];
					$html = file_get_contents($url);
					$json = json_decode($html,true);

					$botType = "none";
					$botData = "0";
					$botPrice = "0";
					$botKG = "0";

					writePageIni($event['source']['userId'],$botType,$botData,$botId,$botPrice,$botKG);

					if($json['status'] == "success"){
						$client->replyMessage(array(
							'replyToken' => $event['replyToken'],
							'messages' => array(
								array(
									'type' => 'text', // 訊息類型 (文字)
									'text' => "成功新增至購物車".emoji("100088") // 回復訊息
								)
							)
						));
					}else{
						$client->replyMessage(array(
							'replyToken' => $event['replyToken'],
							'messages' => array(
								array(
									'type' => 'text', // 訊息類型 (文字)
									'text' => "無法新增至購物車".emoji("100088") // 回復訊息
								)
							)
						));
					}
				}
			}

			/*========================================================*/
			// 購物車查詢:查詢購物車內的東西
			/*========================================================*/
			if($message['text'] == "購物車"){
				$botType = "none";
				$botData = "0";
				$botPrice = "0";
				$botKG = "0";

				writePageIni($event['source']['userId'],$botType,$botData,$botId,$botPrice,$botKG);

				$url = $base_url."LineAPI/getUserCart/".$botId;
				$html = file_get_contents($url);
				$json = json_decode($html,true);

				if($botId == "0"){
					$client->replyMessage(array(
						'replyToken' => $event['replyToken'],
						'messages' => array(
							array(
								'type' => 'text', // 訊息類型 (文字)
								'text' => "找不到使用者，請先進行登入".emoji("100088") // 回復訊息
							)
						)
					));
				} else if ($json != null){
					$actions = array(array("postback","移除","刪除購物車內商品:","product_key","從購物車中移除此商品中..."));

					$output = initFlexCart($json,"product_img","product_name","product_price","cart_amount","cart_price",$actions,$botType);

					$client->replyMessage(array(
						'replyToken' => $event['replyToken'],
						'messages' => array($output)
					));
				} else {
					$client->replyMessage(array(
						'replyToken' => $event['replyToken'],
						'messages' => array(
							array(
								'type' => 'text', // 訊息類型 (文字)
								'text' => "無商品在購物車內，請去購物唷：>" // 回復訊息
							)
						)
					));
				}
			}

			/*========================================================*/
			// 送出訂單
			/*========================================================*/
			if($message['text'] == "送出訂單"){
				$botType = "none";
				$botData = "0";
				$botPrice = "0";
				$botKG = "0";

				writePageIni($event['source']['userId'],$botType,$botData,$botId,$botPrice,$botKG);

				if($botId == "0"){
					$client->replyMessage(array(
						'replyToken' => $event['replyToken'],
						'messages' => array(
							array(
								'type' => 'text', // 訊息類型 (文字)
								'text' => "找不到使用者，請先進行登入".emoji("100088") // 回復訊息
							)
						)
					));
				}else{
					$url = $base_url."LineAPI/addOrder/".$botId;
					$html = file_get_contents($url);
					$json = json_decode($html,true);
					if ($json['status'] == "success"){
						$client->replyMessage(array(
							'replyToken' => $event['replyToken'],
							'messages' => array(
								array(
									'type' => 'text', // 訊息類型 (文字)
									'text' => "已將購物車清空，並送出訂單給予文蛤工社的負責人" // 回復訊息
								)
							)
						));
					} else if ($json['status'] == "no cart"){
						$client->replyMessage(array(
							'replyToken' => $event['replyToken'],
							'messages' => array(
								array(
									'type' => 'text', // 訊息類型 (文字)
									'text' => "無商品在購物車內，請去購物唷" // 回復訊息
								)
							)
						));
					} else if ($json['status'] == "error"){
						$client->replyMessage(array(
							'replyToken' => $event['replyToken'],
							'messages' => array(
								array(
									'type' => 'text', // 訊息類型 (文字)
									'text' => "送出訂單發生錯誤，請通知技術人員" // 回復訊息
								)
							)
						));
					}
				}
			}

			/*========================================================*/
			// 訂單查詢功能
			/*========================================================*/
			if($message['text'] == "訂單查詢"){
				$botType = "product";
				$botData = "0";
				$botPrice = "0";
				$botKG = "0";

				writePageIni($event['source']['userId'],$botType,$botData,$botId,$botPrice,$botKG);

				$url = $base_url."LineAPI/getOrderTotal/".$botId;
				$html = file_get_contents($url);
				$json = json_decode($html,true);

				$today = date("Y-m-d");

				$client->replyMessage(array(
					'replyToken' => $event['replyToken'],
					'messages' => array(
						array(
							'type' => 'text', // 訊息類型 (文字)
							'text' => "今日(".$today.")訂單數量".
										"\n未出貨訂單數量：".$json['noShipped'].
										"\n已出貨訂單數量：".$json['beginShipped'], // 文字
						)
					)
				));

				// $client->replyMessage(array(
				// 	'replyToken' => $event['replyToken'],
				// 	'messages' => array(
				// 		array(
				// 			'type' => 'template', // 訊息類型 (模板)
				// 			'altText' => 'Example buttons template', // 替代文字
				// 			'template' => array(
				// 				'type' => 'buttons', // 類型 (按鈕)
				// 				'title' => '訂單查詢', // 標題 <不一定需要>
				// 				'text' => "今日(".$today.")訂單數量".
				// 						"\n未出貨訂單數量：".$json['noShipped'].
				// 						"\n已出貨訂單數量：".$json['beginShipped'], // 文字
				// 				'actions' => array(
				// 					array(
				// 						'type' => 'message', // 類型 (連結)
				// 						'label' => '未出貨訂單', // 標籤 3
				// 						'text' =>  '未出貨訂單'	// 連結網址
				// 					),array(
				// 						'type' => 'message', // 類型 (連結)
				// 						'label' => '已出貨訂單', // 標籤 3
				// 						'text' =>  '已出貨訂單'	// 連結網址
				// 					)
				// 				)
				// 			)
				// 		)
				// 	)
				// ));
			}

			/*========================================================*/
			// 未出貨訂單
			/*========================================================*/
			if ($message['text'] == "訂單出貨") {
			$url = $base_url . "LineBotAPI/getOrderShipped/".$event['source']['userId'];
			$html = file_get_contents($url);
			$json = json_decode($html,true);

			$data = array();

			if(count($json)>0){
				for($i=0 ; $i<count($json) ; $i++){
					$tempData = array();
					$tempData['thumbnailImageUrl'] = 'https://line.stuhub.at.tw/line/assets/images/error.png';
					$tempData['title'] = "訂單資訊 - 商品未出貨";
					$tempData['text'] = "店家：『".$json[$i]["shop_name"]."』\n訂單編號：".SHA1("order_".$json[$i]["order_key"]);
					$actionData = array();
					$tempActionData = array();
					$tempActionData['type'] = "postback";
					$tempActionData['label'] = "取消訂單";
					$tempActionData['data'] = "取消訂單:".$json[$i]["order_key"];
					$actionData[] = $tempActionData;
					$tempData['actions'] = $actionData;
					$data[] = $tempData;
				}

				$client->shopMessage(array(
					'replyToken' => $event['replyToken'],
					'messages' => array(
						array(
								'type' => 'template', // 訊息類型 (模板)
				                'altText' => 'Example buttons template', // 替代文字
				                'template' => array(
				                    'type' => 'carousel', // 類型 (輪播)
				                    'columns' => $data
				                )
				            )
					)
				));
			}else{
				$client->shopMessage(array(
					'replyToken' => $event['replyToken'],
					'messages' => array(
						array(
								'type' => 'text', // 訊息類型 (文字)
								'text' => "目前沒有訂單需要出貨" // 回復訊息
							)
					)
				));
			}
		}

			break;
		case 'postback':
			/*========================================================*/
			// 新增購物車，選擇斤數
			/*========================================================*/
			if(strstr($event['postback']['data'],"斤數:")){
				$product_key = explode(":",$event['postback']['data'])[1];
				$product_price = explode(":",$event['postback']['data'])[2];

				$botType = "KG";
				$botData = $product_key;
				$botPrice = $product_price;
				$botKG = "0";

				writePageIni($event['source']['userId'],$botType,$botData,$botId,$botPrice,$botKG);
			}

			/*========================================================*/
			// 新增購物車，選擇價錢
			/*========================================================*/
			if(strstr($event['postback']['data'],"價錢:")){
				$product_key = explode(":",$event['postback']['data'])[1];
				$product_price = explode(":",$event['postback']['data'])[2];

				$botType = "price";
				$botData = $product_key;
				$botPrice = $product_price;
				$botKG = "0";

				writePageIni($event['source']['userId'],$botType,$botData,$botId,$botPrice,$botKG);
			}

			/*========================================================*/
			// 刪除購物車:將購物車的此商品刪除
			/*========================================================*/
			if(strstr($event['postback']['data'],"刪除購物車內商品:")){
				$botType = "none";
				$botData = "0";
				$botPrice = "0";
				$botKG = "0";

				writePageIni($event['source']['userId'],$botType,$botData,$botId,$botPrice,$botKG);

				$pruductKey = explode(":",$event['postback']['data'])[1];
				$url = $base_url."LineAPI/deleteUserCart/".$pruductKey."/".$botId;
				$html = file_get_contents($url);
				$json = json_decode($html,true);

				if($json["status"] == "success"){
					$client->replyMessage(array(
						'replyToken' => $event['replyToken'],
						'messages' => array(
							array(
									'type' => 'text', // 訊息類型 (文字)
									'text' => "已成功從購物車刪除此商品" // 回復訊息
								)
						)
					));
				}else{
					$client->replyMessage(array(
						'replyToken' => $event['replyToken'],
						'messages' => array(
							array(
									'type' => 'text', // 訊息類型 (文字)
									'text' => "無法刪除此商品" // 回復訊息
								)
						)
					));
				}
			}
			//require_once('postback.php'); // postback
			break;
		case 'follow': // 加為好友觸發
			$client->replyMessage(array(
				'replyToken' => $event['replyToken'],
				'messages' => array(
					array(
						'type' => 'text',
						'text' => '大家好，文蛤工社的專屬Line機器人'
					)
				)
			));
			break;
		case 'join': // 加入群組觸發
			$client->replyMessage(array(
				'replyToken' => $event['replyToken'],
				'messages' => array(
					array(
						'type' => 'text',
						'text' => '大家好，文蛤工社的專屬Line機器人'
					)
				)
			));
			break;
		default:
			//error_log("Unsupporeted event type: " . $event['type']);
			break;
	}
};
?>
