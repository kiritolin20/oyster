<?php

function initFlexProductInfo($id,$json,$img,$title,$text,$unit,$action){
	$output = array();
	$output["type"] = "flex";
	$output["altText"]= "Restaurant";

	$outputContents = array();

	$outputContents["type"] = "carousel";

	$outputContentsContents = array();
	for($i=0 ; $i<count($json) ; $i++){

		$data = array();
		$data["type"] = "bubble";

		$dataHero = array();
		$dataHero['type'] = "image";
		$dataHero["size"] = "full";
		$dataHero["aspectRatio"] = "20:13";
        $dataHero["aspectMode"] = "cover";
        $dataHero["url"] = "https://faceborder.at.tw/img/".$json[$i][$img];
        // $dataHero["url"] = "https://line.stuhub.at.tw/assets/stulogo2.png";


		$data["hero"] = $dataHero;
		$dataBody = array();
		$dataBody["type"] = "box";
        $dataBody["layout"] = "vertical";
        $dataBody["spacing"] = "sm";
     	$dataBodyContents = array();
     	$dataBodyContentsTitle = array();
     	$dataBodyContentsTitle["type"] = "text";
        $dataBodyContentsTitle["text"] = $json[$i][$title]."(".$json[$i][$unit].")";
        $dataBodyContentsTitle["wrap"] = true;
        $dataBodyContentsTitle["weight"] = "bold";
        $dataBodyContentsTitle["size"] = "xl";
        $dataBodyContentsText = array();
        $dataBodyContentsText["type"] = "box";
        $dataBodyContentsText["layout"] = "baseline";

        $dataBodyContentsTextContents = array();
        $dataBodyContentsTextContentsOne = array();
        $dataBodyContentsTextContentsOne["type"] = "text";
        $dataBodyContentsTextContentsOne["text"] = "$".$json[$i][$text] . " / 斤";
        $dataBodyContentsTextContentsOne["wrap"] = true;
        $dataBodyContentsTextContentsOne["weight"] = "bold";
        $dataBodyContentsTextContentsOne["size"] = "xl";
        $dataBodyContentsTextContentsOne["flex"] = 0;
        $dataBodyContentsTextContents[] = $dataBodyContentsTextContentsOne;

        $dataBodyContentsText["contents"] = $dataBodyContentsTextContents;
        $dataBodyContents[] = $dataBodyContentsTitle;
        $dataBodyContents[] = $dataBodyContentsText;
        $dataBody['contents'] = $dataBodyContents;
		$data["body"] = $dataBody;

		$dataFooter = array();
		$dataFooter["type"] = "box";
        $dataFooter["layout"] = "vertical";
        $dataFooter["spacing"] = "sm";

        $dataFooterContents = array();

        $dataFooterContentsOne = array();
        $dataFooterContentsOne["type"] = "button";
        $dataFooterContentsOne["style"] = "primary";
        $dataFooterContentsAction = array();
        $dataFooterContentsAction["type"] = "postback";
        $dataFooterContentsAction["label"] = $action[0][1];
        $dataFooterContentsAction['data'] = $action[0][2].$json[$i][$action[1][3]].":".$json[$i][$text];
		$dataFooterContentsAction['text'] = $action[0][4];
        $dataFooterContentsOne["action"] = $dataFooterContentsAction;
        $dataFooterContents[] = $dataFooterContentsOne;
        $dataFooterContentsTwo = array();
        $dataFooterContentsTwo["type"] = "button";
        $dataFooterContentsTwo["style"] = "primary";
        $dataFooterContentsAction = array();
        $dataFooterContentsAction["type"] = "postback";
        $dataFooterContentsAction["label"] = $action[1][1];
		$dataFooterContentsAction['data'] = $action[1][2].$json[$i][$action[1][3]].":".$json[$i][$text];
		$dataFooterContentsAction['text'] = $action[1][4];
        $dataFooterContentsTwo["action"] = $dataFooterContentsAction;
        $dataFooterContents[] = $dataFooterContentsTwo;
        $dataFooter["contents"] = $dataFooterContents;
		$data["footer"] = $dataFooter;
		$outputContentsContents[] = $data;
	}
	$outputContents["contents"] = $outputContentsContents;
	$output["contents"] = $outputContents;
	return $output;
}
?>