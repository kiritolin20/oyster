<?php

function initFlexCart($json,$img,$title,$text,$cart_amount,$cart_price,$action,$page){
	$base_url = "https://faceborder.at.tw/oyster/";

	$output = array();

	$output["type"] = "flex";

	$output["altText"]= "Restaurant";



	$outputContents = array();

	$outputContents["type"] = "carousel";



	$outputContentsContents = array();

	if(count($json)>10){

		for($i=9*$page-9 ; $i<9*$page && $i<count($json); $i++){

			$data = array();

			$data["type"] = "bubble";

			$dataHero = array();

			$dataHero['type'] = "image";

			$dataHero["size"] = "full";

			$dataHero["aspectRatio"] = "20:13";

	        $dataHero["aspectMode"] = "cover";

	        // $dataHero["url"] = $admin_base_url."/uploads/".$json[$i][$img];

        	$dataHero["url"] = "https://line.stuhub.at.tw/assets/stulogo2.png";

			$data["hero"] = $dataHero;



			$dataBody = array();

			$dataBody["type"] = "box";

	        $dataBody["layout"] = "vertical";

	        $dataBody["spacing"] = "sm";



	     	$dataBodyContents = array();

	     	$dataBodyContentsTitle = array();

	     	$dataBodyContentsTitle["type"] = "text";

	        $dataBodyContentsTitle["text"] = $json[$i][$title];

	        $dataBodyContentsTitle["wrap"] = true;

	        $dataBodyContentsTitle["weight"] = "bold";

	        $dataBodyContentsTitle["size"] = "xl";



	        $dataBodyContentsText = array();

	        $dataBodyContentsText["type"] = "box";

	        $dataBodyContentsText["layout"] = "baseline";



	        $dataBodyContentsTextContents = array();

	        $dataBodyContentsTextContentsOne = array();

	        $dataBodyContentsTextContentsOne["type"] = "text";

	        $dataBodyContentsTextContentsOne["text"] = "$".$json[$i][$text] . " X " . $json[$i][$cart_amount] . "斤 = $" . $json[$i][$cart_price];

	        $dataBodyContentsTextContentsOne["wrap"] = true;

	        $dataBodyContentsTextContentsOne["weight"] = "bold";

	        $dataBodyContentsTextContentsOne["size"] = "xl";

	        $dataBodyContentsTextContentsOne["flex"] = 0;

	        $dataBodyContentsTextContents[] = $dataBodyContentsTextContentsOne;

	        $dataBodyContentsText["contents"] = $dataBodyContentsTextContents;



	        $dataBodyContents[] = $dataBodyContentsTitle;

	        $dataBodyContents[] = $dataBodyContentsText;



	        $dataBody['contents'] = $dataBodyContents;

			$data["body"] = $dataBody;

			$dataFooter = array();

			$dataFooter["type"] = "box";
	        $dataFooter["layout"] = "baseline";
	        $dataFooter["height"] = "60px";
	        $dataFooter["backgroundColor"] = "#f15c2c";

	        $dataFooterContents = array();

	        $dataFooterContentsOne = array();
	        $dataFooterContentsOne["type"] = "filler";
            $dataFooterContents[] = $dataFooterContentsOne;

	        $dataFooterContentsTwo = array();
	        $dataFooterContentsTwo["type"] = "icon";
	        $dataFooterContentsTwo["url"] = "https://faceborder.at.tw/img/delete.png";
	        $dataFooterContentsTwo["size"] = "4xl";
	        $dataFooterContentsTwo["offsetBottom"] = "10px";
            $dataFooterContents[] = $dataFooterContentsTwo;

	        // $dataFooterContentsThree = array();
	        // $dataFooterContentsThree["type"] = "text";
	        // $dataFooterContentsThree["text"] = "移除";
	        // $dataFooterContentsThree["flex"] = 0;
	        // $dataFooterContentsThree["color"] = "#FFFFFF";
	        // $dataFooterContentsThree["offsetTop"] = "-15px";
	        // $dataFooterContentsThree["size"] = "xxl";
	        // $dataFooterContentsThree["weight"] = "bold";
	        // $dataFooterContentsThree["wrap"] = true;
         //    $dataFooterContents[] = $dataFooterContentsThree;

	        $dataFooterContentsFour = array();
	        $dataFooterContentsFour["type"] = "filler";
            $dataFooterContents[] = $dataFooterContentsFour;
	        $dataFooter["contents"] = $dataFooterContents;

	        $dataFooterAction = array();
	        $dataFooterAction["type"] = $action[0][0];
	        $dataFooterAction["label"] = $action[0][1];
            $dataFooterAction['data'] = $action[0][2].$json[$i][$action[0][3]];
			// $dataFooterAction['displayText'] = $action[0][4];
	        $dataFooter["action"] = $dataFooterAction;

			$data["footer"] = $dataFooter;

			// $dataFooter = array();

			// $dataFooter["type"] = "box";

	  //       $dataFooter["layout"] = "vertical";

	  //       $dataFooter["spacing"] = "sm";



	  //       $dataFooterContents = array();

	  //       $dataFooterContentsOne = array();

	  //       $dataFooterContentsOne["type"] = "button";

   //          $dataFooterContentsOne["style"] = "primary";

   //          $dataFooterContentsOne["color"] = "#FF0000";



   //          $dataFooterContentsAction = array();

   //          $dataFooterContentsAction["type"] = $action[0][0];

   //          $dataFooterContentsAction["label"] = $action[0][1];

   //          $dataFooterContentsAction['data'] = $action[0][2].$json[$i][$action[0][3]];

			// $dataFooterContentsAction['text'] = $action[0][4];



   //          $dataFooterContentsOne["action"] = $dataFooterContentsAction;

   //          $dataFooterContents[] = $dataFooterContentsOne;

	  //       $dataFooter["contents"] = $dataFooterContents;

			// $data["footer"] = $dataFooter;

			$outputContentsContents[] = $data;

		}

		$data = array();

		$data["type"] = "bubble";



		$dataBody = array();

		$dataBody["type"] = "box";

        $dataBody["layout"] = "vertical";

        $dataBody["spacing"] = "sm";



     	$dataBodyContents = array();

     	$dataBodyContentsBefore = array();

     	$dataBodyContentsBefore["type"] = "button";

        $dataBodyContentsBefore["flex"] = "2";

        $dataBodyContentsBefore["style"] = "primary";

        $dataBodyContentsBefore["color"] = "aaaaaa";

   		$dataBodyContentsBefore["gravity"] = "bottom"; 



   		$dataBodyContentsBeforeAction = array();

        $dataBodyContentsBeforeAction["type"] = "message";

        $dataBodyContentsBeforeAction["label"] = "上一頁";

        $dataBodyContentsBeforeAction["text"] = $action[0]."上一頁";;

        $dataBodyContentsBefore["action"] = $dataBodyContentsBeforeAction;



        $dataBodyContentsAfter = array();

        $dataBodyContentsAfter["type"] = "button";

        $dataBodyContentsAfter["flex"] = "2";

        $dataBodyContentsAfter["style"] = "primary";

        $dataBodyContentsAfter["color"] = "aaaaaa";

   		$dataBodyContentsAfter["gravity"] = "bottom"; 



   		$dataBodyContentsAfterAction = array();

        $dataBodyContentsAfterAction["type"] = "message";

        $dataBodyContentsAfterAction["label"] = "下一頁";

        $dataBodyContentsAfterAction["text"] = $action[0]."下一頁";;

        $dataBodyContentsAfter["action"] = $dataBodyContentsAfterAction;



        $dataBodyContents[] = $dataBodyContentsBefore;

        $dataBodyContents[] = $dataBodyContentsAfter;



        $dataBody['Contents'] = $dataBodyContents;

		$data["body"] = $dataBody;



		$outputContentsContents[] = $data;

	}else{

		for($i=0 ; $i<count($json) ; $i++){

			$data = array();

			$data["type"] = "bubble";



			$dataHero = array();

			$dataHero['type'] = "image";

			$dataHero["size"] = "full";

			$dataHero["aspectRatio"] = "20:13";

	        $dataHero["aspectMode"] = "cover";

	        // $dataHero["url"] = $admin_base_url."/uploads/".$json[$i][$img];

	        $dataHero["url"] = "https://faceborder.at.tw/image/".$json[$i][$img];

			$data["hero"] = $dataHero;



			$dataBody = array();

			$dataBody["type"] = "box";

	        $dataBody["layout"] = "vertical";

	        $dataBody["spacing"] = "sm";



	     	$dataBodyContents = array();

	     	$dataBodyContentsTitle = array();

	     	$dataBodyContentsTitle["type"] = "text";

	        $dataBodyContentsTitle["text"] = $json[$i][$title];

	        $dataBodyContentsTitle["wrap"] = true;

	        $dataBodyContentsTitle["weight"] = "bold";

	        $dataBodyContentsTitle["size"] = "xl";



	        $dataBodyContentsText = array();

	        $dataBodyContentsText["type"] = "box";

	        $dataBodyContentsText["layout"] = "baseline";



	        $dataBodyContentsTextContents = array();

	        $dataBodyContentsTextContentsOne = array();

	        $dataBodyContentsTextContentsOne["type"] = "text";

	        $dataBodyContentsTextContentsOne["text"] = "$".$json[$i][$text] . " X " . $json[$i][$cart_amount] . "斤 = $" . $json[$i][$cart_price];

	        $dataBodyContentsTextContentsOne["wrap"] = true;

	        $dataBodyContentsTextContentsOne["weight"] = "bold";

	        $dataBodyContentsTextContentsOne["size"] = "xl";

	        $dataBodyContentsTextContentsOne["flex"] = 0;

	        $dataBodyContentsTextContents[] = $dataBodyContentsTextContentsOne;

	        $dataBodyContentsText["contents"] = $dataBodyContentsTextContents;



	        $dataBodyContents[] = $dataBodyContentsTitle;

	        $dataBodyContents[] = $dataBodyContentsText;



	        $dataBody['contents'] = $dataBodyContents;

			$data["body"] = $dataBody;



			$dataFooter = array();

			$dataFooter["type"] = "box";
	        $dataFooter["layout"] = "baseline";
	        $dataFooter["height"] = "60px";
	        $dataFooter["backgroundColor"] = "#f15c2c";

	        $dataFooterContents = array();

	        $dataFooterContentsOne = array();
	        $dataFooterContentsOne["type"] = "filler";
            $dataFooterContents[] = $dataFooterContentsOne;

	        $dataFooterContentsTwo = array();
	        $dataFooterContentsTwo["type"] = "icon";
	        $dataFooterContentsTwo["url"] = "https://faceborder.at.tw/img/delete.png";
	        $dataFooterContentsTwo["size"] = "4xl";
	        $dataFooterContentsTwo["offsetBottom"] = "10px";
            $dataFooterContents[] = $dataFooterContentsTwo;

	        // $dataFooterContentsThree = array();
	        // $dataFooterContentsThree["type"] = "text";
	        // $dataFooterContentsThree["text"] = "移除";
	        // $dataFooterContentsThree["flex"] = 0;
	        // $dataFooterContentsThree["color"] = "#FFFFFF";
	        // $dataFooterContentsThree["offsetTop"] = "-15px";
	        // $dataFooterContentsThree["size"] = "xxl";
	        // $dataFooterContentsThree["weight"] = "bold";
	        // $dataFooterContentsThree["wrap"] = true;
         //    $dataFooterContents[] = $dataFooterContentsThree;

	        $dataFooterContentsFour = array();
	        $dataFooterContentsFour["type"] = "filler";
            $dataFooterContents[] = $dataFooterContentsFour;
	        $dataFooter["contents"] = $dataFooterContents;

	        $dataFooterAction = array();
	        $dataFooterAction["type"] = $action[0][0];
	        $dataFooterAction["label"] = $action[0][1];
            $dataFooterAction['data'] = $action[0][2].$json[$i][$action[0][3]];
			// $dataFooterAction['displayText'] = $action[0][4];
	        $dataFooter["action"] = $dataFooterAction;

			$data["footer"] = $dataFooter;

			$outputContentsContents[] = $data;

		}

	}

	$outputContents["contents"] = $outputContentsContents;

	$output["contents"] = $outputContents;

	return $output;

}

?>