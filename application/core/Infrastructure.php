<?php
class Infrastructure extends CI_Controller {

	//false=未登入 true=已登入
	private $login;
	//private $menu;
	//private $permissionKey;
	private $memberAccount;
	private $adminType;
	private $shopKey;
	private $key;

	
	public function __construct(){
		parent::__construct();
		// $this->serverFunction();
		$this->startSession(8*3600);		
		header("X-Frame-Options: DENY");
		$this->login = isset($_SESSION['key']);
		if($this->login){
			$this->key = $_SESSION['key'];
			$this->memberAccount = $_SESSION['member_account'];
		}
		$this->load->model('Infrastructure_model');
		//$this->menu = $this->Infrastructure_model->getMenu($this->getPermissionKey());
		$this->load->library('Light_datatables');
	}

	function startSession($expire = 0){
		//如果$expire設定為0的話以PHP.INI裡設定的時間為主
		if ($expire == 0){
			$expire = ini_get('session.gc_maxlifetime');
		}else{
			ini_set('session.gc_maxlifetime', $expire);
		}
		//如果$_COOKIE['PHPSESSID']不存在,在指定時間內SESSION過期
		if (empty($_COOKIE['PHPSESSID'])) {
			session_set_cookie_params($expire);
			session_start();
		} else {
			//如果$_COOKIE['PHPSESSID']存在
			session_start();
			setcookie('PHPSESSID', session_id(), time() + $expire);
		}
	}
		
	/**
	 * Https轉址
	 */
	private function serverFunction(){
		if(!((isset($_SERVER['HTTPS'])&&$_SERVER['HTTPS']=='on')||(isset($_SERVER['HTTP_X_FORWARDED_PROTO'])&&$_SERVER['HTTP_X_FORWARDED_PROTO']=='https'))){
			Header("HTTP/1.1 301 Moved Permanently");
			if(preg_match("/www/i", $_SERVER['SERVER_NAME'])){
			header('Location: https://XXXX.com'.$_SERVER['REQUEST_URI']);
			}else{
			header('Location: https://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
			}
			exit();
		}else{
			if(preg_match("/www/i", $_SERVER['SERVER_NAME'])){
			Header("HTTP/1.1 301 Moved Permanently");
			header('Location: https://XXXX.com'.$_SERVER['REQUEST_URI']);
			exit();
			}
		}
	}

	/**
	 * 回傳是否擁有登入狀態
	 * @return Boolean
	 */
	public function getLogin(){
		return $this->login;
	}

	/**
	 * 回傳用戶主鍵
	 * @return Int
	 */
	public function getkey(){
		return $this->key;
	}

	/**
	 * 回傳用戶帳戶
	 * @return String
	 */
	public function getMemberAccount(){
		return $this->memberAccount;
	}

	/**
	 * 回傳商店主鍵
	 * @return String
	 */
	public function getshopKey(){
		return $this->shopKey;
	}

	/**
	 * 回傳權限
	 * 1為管理員
	 * 0為後臺
	 * @return String
	 */
	public function getAdminType(){
		return $this->adminType;
	}

	/**
	 * 主鍵解密
	 * @param  String,String
	 * @return Atring
	 */
	public function getKeyDecode($encode,$keyName){
		if($result = $this->Infrastructure_model->getKeyDecode($encode,$keyName)){
			return $result;
		}
	}

	/**
	 * 替傳入陣列消毒，回傳消毒完成的結果
	 * @param  Array
	 * @return Array
	 */
	public function xss(array $array){

		foreach ($array as &$value){
			if(is_array($value)){
				$value = $this->xss($value);
			}else{
				$value = $this->security->xss_clean($value);
			}
		}
		
		return $array;
	}

	/**
	 * 初始化view
	 * @param  String,Array
	 * @return Array
	 */
	public function initView($entryName,$data = array(),$functionName = ""){
		$this->load->view($entryName);
		eval("\$entryName = new {$entryName}(\$data);");
		if($functionName == ""){
			$entryName->index();
		}else if($functionName == "admin"){
			$entryName->adminSet();
		}else if ($functionName == "seller") {
			$entryName->sellerSet();
		}else if ($functionName == "reply") {
			$entryName->reply();
		}else{
			$entryName->index();
		}
	}


	/**
	 * 回傳View初始化內容
	 * @return Array
	 */
	public function getTransferData(){
		$data = array();
		$data["account"] = $this->getMemberAccount();
		$data["key"] = $this->getkey();
		$data["shopKey"] = $this->getshopKey();
		$data["adminType"] = $this->getAdminType();
		return $data;
	}
	/**
	 * 取得隨機亂數
	 * @return String
	 */
	function getRandom()
	{
		$key = '';
		$word = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$len = strlen($word);
		for ($i = 0; $i < 15; $i++) {
			$key = $word[rand() % $len];
		}
		return base64_encode($key);
	}
	/**
	 * 解密
	 * @param  String
	 * @return String
	 */
	public function private_decrypt($encrypted_text)
	{
		$encrypted_text = str_replace("---", "/", $encrypted_text);
		$encrypted_text = str_replace("___", "+", $encrypted_text);
		$encrypted_text = str_replace(":::", "=", $encrypted_text);
		$fp = fopen(base_url('assets/private.key'), "r");
		$priv_key = fread($fp, 8192);
		fclose($fp);
		$private_key_res = openssl_get_privatekey($priv_key);
		// $private_key_res = openssl_get_privatekey($priv_key, PASSPHRASE); // 如果使用密碼
		if (!$private_key_res) {
			throw new Exception('Private Key invalid');
		}

		// 先將密文做 base64_decode() 解釋
		openssl_private_decrypt(base64_decode($encrypted_text), $decrypted, $private_key_res, OPENSSL_PKCS1_OAEP_PADDING);
		openssl_free_key($private_key_res);
		return $decrypted;
	}
	/**
	 * Line
	 */
	public function curl($type, $postData)
	{

		//選擇Line群組
		if ($type == "admin") {
			$accessToken = 'Ar01bkmYS7GVjcpi9nUWIkj3hAnZYCZb67aoVMYscSp04VV5xyVoL31u1CIZCbs0f4CgYrOB2sraZRq8KqS1VkuoQhJv9XWyO0K9X39GLjIAf0au8cFh/Uoj0EjxHb1LXTyEZtH5ZqGr/dstPzx5UQdB04t89/1O/w1cDnyilFU=';
		} else if ($type == "user") {
			$accessToken = '2DYgJs7+z815zXsdtlUxgD81GACFb6a6RmW7gy/Ym35zPmHtrROOYmwNHPN+r1NZNmqPGJyOJAjv2SWnwbDlUHTkYlICOPD0ifQWGgin244HfJFrZF69aCJ83/AYgM6D1Y6rR93DzHuyYKWiIb4uTQdB04t89/1O/w1cDnyilFU=';
		}

		//post url init
		$ch = curl_init("https://api.line.me/v2/bot/message/push");

		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Authorization: Bearer ' . $accessToken
		));

		curl_exec($ch);
		curl_close($ch);
	}

	public function sendMessage($type, $LineKey, $message)
	{
		//回覆的訊息,replyToken
		$postData = [
			"to" => $LineKey,
			"messages" => [
				[
					"type" => "text",
					"text" => $message
				]
			]
		];

		$this->curl($type, $postData);
	}
	public function userJudge($line_key,$order_key)
	{
		$postData = [
			"to" => $line_key,
			"messages" => [
				[
					'type' => 'template', // 訊息類型 (模板)
					'altText' => '訂單通知', // 替代文字
					'template' => [
						'type' => 'buttons', // 類型 (按鈕)
						'title' => '訂單審核', // 標題 <不一定需要>
						'text' => "訂單編號：$order_key ，請在3-7天內付款", // 文字
						'actions' => [
							[
								'type' => 'postback', // 類型 (連結)
								'label' => '付款確認', // 標籤 2
								'data' => "付款確認:". $order_key,
								'text' => '確認' //
							]
						]
					]
				]
			]
		];

		$this->curl("user", $postData);
	}

	public function userShip($line_key, $order_key)
	{
		//回覆的訊息,replyToken
		$postData = [
			"to" => $line_key,
			"messages" => [
				[
					"type" => "text",
					"text" => "賣家已出貨，訂單編號". $order_key
				]
			]
		];
		$this->curl("user", $postData);
	}

	/**
	 * 功能：取得給定日期所在週的開始日期和結束日期
	 * 參數：$getdate 日期，默認為當天，格式：YYYY-MM-DD
	 * $first_day 一周以星期一還是星期天開始，0為星期天，1為星期一
	 * 返回：array
	 *
	 */
	public function thisweek($getdate = "", $first_day = 0)
	{
		if (!$getdate) $getdate = date("Y-m-d");

		//取得一周的第幾天,星期天開始0-6
		$weekday = date("w", strtotime($getdate));

		//要減去的天數

		$del_day = $weekday - $first_day;
		//本週開始日期
		$week_start_day = date("Y-m-d", strtotime("$getdate -" . $del_day . " days"));


		//本週結束日期
		$week_end_day = date("Y-m-d", strtotime("$week_start_day +6 days"));


		//上週開始日期
		$lastweek_start_day = date('Ym-d', strtotime("$week_start_day - 7 days"));


		//上週結束日期
		$lastweek_end_day = date('Ym-d', strtotime("$week_start_day - 1 days"));

		//返回開始和結束日期
		return array($week_start_day, $week_end_day, $lastweek_start_day, $lastweek_end_day);
	}

    /* 各別取出來使用的方式如下

    $week_array = thisweek();
    echo $week_array[0]; //本週開始日期
    echo $week_array[1]; //本週結束日期
    echo $week_array[2]; //上週開始日期
    echo $week_array[3]; //上週結束日期
    */

}

?>