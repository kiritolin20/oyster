<!-- google font -->

<link href="<?php echo base_url() ?>Vertical/favicon.ico" rel="shortcut icon" type="image/x-icon">
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url() ?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url() ?>assets/css/ionicons.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url() ?>assets/css/simple-line-icons.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url() ?>assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/css/weather-icons.min.css" rel="stylesheet">

<!--Morris Chart -->
<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/index/morris-chart/morris.css">
<link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet">
<!-- dataTables -->
<link href="<?php echo base_url() ?>assets/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- File Input -->
<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" crossorigin="anonymous"> -->
<link href="<?php echo base_url() ?>assets/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/css/all-krajee.min.css" media="all" rel="stylesheet" type="text/css" />
<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous"> -->
<link href="<?php echo base_url() ?>assets/themes/explorer-fas/theme.css" media="all" rel="stylesheet" type="text/css" />