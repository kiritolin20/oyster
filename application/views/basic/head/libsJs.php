<script type="text/javascript" src="<?php echo base_url() ?>assets/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>

<!--vectormap-->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/index/jquery-jvectormap-1.2.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/index/jquery-jvectormap-world-mill-en.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/index/vmap-init.js"></script>
<!--Morris Chart-->
<!-- <script type="text/javascript" src="<?php echo base_url() ?>assets/js/index/morris-chart/morris.js"></script> -->
<!-- <script type="text/javascript" src="<?php echo base_url() ?>assets/js/index/morris-chart/raphael-min.js"></script> -->
<!--morris chart initialization-->
<!-- <script type="text/javascript" src="<?php echo base_url() ?>assets/js/index/morris-chart/morris-init.js"></script> -->
<!-- chart js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/index/Chart.bundle.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/index/utils.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url() ?>assets/js/index/chart.js"></script> -->

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/custom.js"></script>
<!-- dataTables -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/dataTables.bootstrap4.min.js"></script>
<!-- File Input -->
<!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script> -->
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script> -->
<script src="<?php echo base_url() ?>assets/js/plugins/piexif.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/sortable.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/js/fileinput.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/js/locales/fr.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/js/locales/es.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/themes/fas/theme.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/themes/explorer-fas/theme.js" type="text/javascript"></script>
<!-- 折線圖套件 -->
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>