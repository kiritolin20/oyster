<!--JavaScript Load-->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url() ?>assets/libs/jquery/jquery-2.2.4.js"></script> -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/sweetalert.min.js"></script>
<script type="text/javascript">
    //全域變數，用於儲存Modal開啟時傳遞的內容
    var viewData = {};
    $(document).ready(function() {
        basic.documentReady.forEach(element => {
            element();
        });
        leftBar();

        if (controllerName['2'] != 'Login' && controllerName['2'] != 'Regist') {
            setTimeout(() => {
                alert("連線逾時，請重新登錄");

                ajaxPost(base_url('Login/out'))
                    .done(function(e) {
                        location.reload();
                    });
            }, 1440000);
        }

        /**
         * 送出新增資料表單，並呼叫insertProduct function
         */
        $("form[id='insertForm']").submit(function(e) {
            e.preventDefault();
            insertProduct($("#insertForm"));
        });

        /**
         * 送出更改表單，並呼叫modifyData function
         */
        $("form[id='modifyForm']").submit(function(e) {
            e.preventDefault();
            modifyData($("#modifyForm"));
        });


        /**
         * 送出更新圖片表單，並呼叫modifyImg function
         */
        $("form[id='modifyImg']").submit(function(e) {
            e.preventDefault();
            modifyImg($("#modifyImg"));
        });

        /**
         * 送出問卷，並呼叫modifyQuestion function
         */
        $("form[id='question']").submit(function(e) {
            e.preventDefault();
            modifyQuestion($("#question"), 'admin');
        });
        /**
         * 送出問卷，並呼叫modifyQuestion function
         */
        $("form[id='sellerQuestion']").submit(function(e) {
            e.preventDefault();
            modifyQuestion($("#sellerQuestion"), 'seller');
        });

        datatable.get();
    });
    //全域方法，擴充jquery，將表單序列化後製作成物件回傳
    $.fn.getFormObject = function() {
        var obj = {};
        var arr = this.serializeArray();
        arr.forEach(function(item, index) {
            if (obj[item.name] === undefined) { // New
                obj[item.name] = item.value || ' ';
            } else { // Existing
                if (!obj[item.name].push) {
                    obj[item.name] = [obj[item.name]];
                }
                obj[item.name].push(item.value || '');
            }
        });
        return obj;
    };
    //全域方法，映射正確的URL
    let base_url = (str = "") => '<?php echo base_url() ?>' + str;

    //全域方法，開啟彈跳視窗，並且儲存傳遞內容，傳遞內容數量可變．它們會被序列化包裝在全域變數中．
    function openModal(modalName, value = null) {
        var data = [];
        for (var i = 1; i < arguments.length; i++) {
            data.push(arguments[i]);
        }
        viewData[modalName] = data;
        $('#' + modalName).modal('show');
    }

    //全域方法，再次封裝ajax，定義錯誤碼；只能使用POST請求，並且給予JSON．
    function ajaxPost(postUrl, postData = {}) {
        return $.ajax({
            url: postUrl,
            type: 'POST',
            dataType: 'json',
            data: {
                data: JSON.stringify(postData)
            },
        }).pipe(function(e) {
            if (e.status == "dbErrDev") {
                return $.Deferred().reject({
                    status: e.status,
                    statusText: e.msg,
                    statusCode: e.code
                });
            } else if ((e.status == "dbErr")) {
                return $.Deferred().reject({
                    status: e.status,
                    statusCode: e.code
                });
            } else {
                return e;
            }
        }).fail(function(e) {
            swal('執行失敗', errorText(e), 'error')
                .then(function() {
                    location.reload();
                });
        });
        //區域方法，回傳錯誤訊息
        function errorText(e) {
            var text = "";
            console.log(e);
            if (e.status == "dbErrDev") {
                if (e.statusCode == 1451) {
                    text = "您欲刪除的對象在資料庫內已有所關聯，為求資料完整性無法刪除。";
                    text += '\n網址：' + postUrl;
                    text += '\n狀態碼：(' + e.status + ')';
                    text += '\n伺服器訊息：(' + e.statusCode + ')' + e.statusText;
                } else {
                    text = "資料庫出現錯誤，請重新再試，若錯誤重複出現請回傳本畫面給予資訊人員。";
                    text += '\n網址：' + postUrl;
                    text += '\n狀態碼：(' + e.status + ')';
                    text += '\n伺服器訊息：(' + e.statusCode + ')' + e.statusText;
                }
            } else if (e.status == "dbErr") {
                if (e.statusCode == 1451) {
                    text = "您欲刪除的對象在資料庫內已有所關聯，為求資料完整性無法刪除。";
                    text += '\n狀態碼：(' + e.status + ')';
                } else {
                    text = "資料庫出現錯誤，請重新再試，若錯誤重複出現請回傳本畫面給予資訊人員。";
                    text += '\n網址：' + postUrl;
                    text += '\n狀態碼：(' + e.status + ')';
                }
            } else if (e.status == 200) {
                text = "無法解析伺服器回傳內容，請重新再試，若錯誤重複出現請回傳本畫面給予資訊人員。";
                text += '\n網址：' + postUrl;
                text += '\n伺服器訊息：(' + e.status + ')' + "Server return data not a json-text.";
            } else {
                text = "連線出現異常，請重新再試，若錯誤重複出現請回傳本畫面給予資訊人員。";
                text += '\n網址：' + postUrl;
                if (e.status == "dbErrDev") {
                    text += '\n伺服器訊息：(' + e.status + ')' + e.statusText;
                } else {
                    text += '\n狀態碼：(' + e.status + ')';
                }
            }
            return text;
        }
    }
    /**
     * 確認傳入的input是否為空
     * @param  Array
     * @return bool
     */
    function checkArguments() {
        for (var i = 0; i < arguments.length; i++) {
            if (arguments[i].trim() == "") {
                return false;
            }
        };
        return true;
    }
    var basic = {
        documentReady: [],
        pushReady: function(fun) {
            this.documentReady.push(fun);
        },
    }

    var pathname = location.pathname;
    var controllerName = new Array();
    controllerName = pathname.split("/");



    // basic.pushReady(function() {
    //     childRow();
    // });

    let leftBar = () => {
        $(".dropdown-menu li a").on('click', function() {
            $(this).parents(".dropdown").find('.btn').html($(this).html() + ' <span class="caret"></span>');
            $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
        });

        //Full_Screen
        $(".fullscreen-btn").on("click", function() {
            document.fullScreenElement && null !== document.fullScreenElement || !document.mozFullScreen && !document.webkitIsFullScreen ? document.documentElement.requestFullScreen ? document.documentElement.requestFullScreen() : document.documentElement.mozRequestFullScreen ? document.documentElement.mozRequestFullScreen() : document.documentElement.webkitRequestFullScreen && document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT) : document.cancelFullScreen ? document.cancelFullScreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.webkitCancelFullScreen && document.webkitCancelFullScreen()
        });

        // collapse button in panel
        $(document).on('click', '.t-collapse', function() {
            var el = $(this).parents(".card").children(".card_chart");
            if ($(this).hasClass("fa-chevron-down")) {
                $(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
                el.slideUp(200);

            } else {
                $(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
                el.slideDown(200);

            }
        });


        //close button in panel
        $(document).on('click', '.t-close', function() {
            $(this).parents(".card, .stats-wrap").parent().remove();
        });

        //Scroll_BAr

        $(".scroll_auto").mCustomScrollbar({
            setWidth: false,
            setHeight: false,
            setTop: 0,
            setLeft: 0,
            axis: "y",
            scrollbarPosition: "inside",
            scrollInertia: 950,
            autoDraggerLength: true,
            autoHideScrollbar: false,
            autoExpandScrollbar: false,
            alwaysShowScrollbar: 0,
            snapAmount: null,
            snapOffset: 0
        });

        //Click_menu_icon_Add_Class_body
        $(".icon_menu").on('click', function() {
            if ($(window).width() > 767) {
                $('body').toggleClass("nav_small");
            } else {
                $('body').toggleClass("mobile_nav");
            }
        });




        // back-to-top
        $(window).on('scroll', function() {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').on('click', function() {

            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        //===ToolTip
        $(function() {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover();
        });

        //Add_li
        $(".todo--panel").on("submit", "form", function(a) {
            a.preventDefault();
            a = $(this);
            var c = a.find(".form-control");

            $('<li class="list-group-item" style="display: none;"><label class="todo--label"><input type="checkbox" name="" value="1" class="todo--input"><span class="todo--text">' + c.val() + '</span></label><a href="#" class="todo--remove">&times;</a></li>').appendTo(".list-group").slideDown("slow");
            c.val("");
        }).on("click", ".todo--remove", function(a) {
            a.preventDefault();
            var c = $(this).parent("li");
            c.slideUp("slow", function() {
                c.remove();
            });
        });
        $('#dc_accordion').dcAccordion();
    }
    /**
     * 顯示新增帶有圖片資料的Modal
     * @param  int $maxCount
     */
    let showInsertModal = $maxCount => {
        $("#insertfile").html("<input class='file input-lg' type='file' id='insertfileBox' name='setFile[]' multiple>");
        $("#insertfileBox").fileinput({
            language: "zh-TW",
            uploadAsync: true,
            uploadUrl: base_url(controllerName['2'] + '/fileInput'),
            showZoom: true,
            showClose: false,
            showUpload: false,
            showRemove: false,
            showPreview: true,
            showBrowse: true,
            browseOnZoneClick: true,
            fileActionSettings: {
                showUpload: false,
                showDrag: true
            },
            // initialPreviewDownloadUrl: "{filename}",
            maxFileCount: $maxCount,
            removeClass: "btn btn-warning",
            maxFileSize: 20480,
            dropZoneEnabled: true,
            initialPreviewAsData: true,
            browseOnZoneClick: true,
            // previewSettings: {
            //     image: {
            //         width: "160px",
            //         height: "120px"
            //     },
            // },
            allowedFileExtensions: ['jpg', 'png', 'jpeg'],
            overwriteInitial: false
        })
        $('#insertModal').modal('show');
    }

    /**
     * 新增資料
     * @param  Object $formDom
     * @return bool
     */
    let insertProduct = $formDom => {
        swal("確定要新增嗎?", {
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function(value) {
            if (value) {
                let data = $formDom.getFormObject();
                const key = location.pathname.split('/');
                if (key[4] != undefined) {
                    data["lineKey"] = key[4];
                    console.log(key[4]);
                } else {}
                ajaxPost(base_url(controllerName['2'] + '/insertProduct'), data)
                    .done(function(e) {
                        if (e.status == 1) {
                            if ($('#insertfileBox').val() == undefined) {
                                swal("成功", "資料修改成功", "success");
                                $("#insertForm  input.form-control").val("");
                                $("#insertForm  select.form-control").val("1");
                                $("#insertForm  textarea.form-control").val("");
                                $('#insertModal').modal('hide');
                                datatable.get();
                            } else {
                                $('#insertfileBox').fileinput('upload');
                                $('#insertfileBox').on('fileuploaded', function(event, data, id, index) {
                                    let obj = {
                                        "img": data.response.name,
                                        "key": e.key,
                                    };
                                    ajaxPost(base_url(controllerName['2'] + '/modifyInfoImg'), obj)
                                        .done(function(e) {
                                            // console.log(e);
                                            swal("成功", "資料修改成功", "success");
                                            $("#insertForm  input.form-control").val("");
                                            $("#insertForm  select.form-control").val("1");
                                            $("#insertForm  textarea.form-control").val("");
                                            $('#insertModal').modal('hide');
                                            datatable.get();
                                            if (key[4] != undefined) {
                                                window.location.reload();
                                            } else {}
                                        });
                                })
                            }

                        } else {
                            swal("失敗", "資料修改失敗，請重新再試", "error");
                            return true;
                        }
                    });
            };
        });
    }

    /**
     * 取得該頁面的dataTable
     * @param  String $datatableName,String $controllerName,Array $colArray 不排列的資料欄位
     * @return datatable
     */
    let getDataTable = ($datatableName, col) => {
        $('#' + $datatableName + 'DataTable').DataTable({
            "language": {
                "lengthMenu": "顯示 _MENU_ 筆消息",
                "emptyTable": "沒有資料",
                "info": "目前顯示 _START_ 至 _END_ 筆的資料，共 _TOTAL_ 筆資料",
                "infoFiltered": "，從 _MAX_ 筆資料中過濾",
                "infoEmpty": "沒有資料能夠顯示",
                "zeroRecords": "沒有資料，可以鍵入其他內容進行搜索",
                "search": "搜尋資料：",
                "paginate": {
                    "next": "下一頁",
                    "previous": "上一頁"
                },
            },
            destroy: true,
            autoWidth: false,
            searching: true,
            autoWidth: false,
            columnDefs: col,
            "order": [],
            "ajax": {
                url: base_url(controllerName['2'] + '/getDataTable'),
                type: 'POST'
            }
        })
    }

    /**
     * 取得隱藏欄位的datatable
     * @param  String $datatableName,String $controllerName,Array $colArray 不排列的資料欄位
     * @return datatable
     */
    var tableRow;

    let getChildRowsDataTable = ($datatableName, ...col) => {
        var table = $('#' + $datatableName + 'DataTable').DataTable({
            "language": {
                "lengthMenu": "顯示 _MENU_ 筆消息",
                "emptyTable": "沒有資料",
                "info": "目前顯示 _START_ 至 _END_ 筆的資料，共 _TOTAL_ 筆資料",
                "infoFiltered": "，從 _MAX_ 筆資料中過濾",
                "infoEmpty": "沒有資料能夠顯示",
                "zeroRecords": "沒有資料，可以鍵入其他內容進行搜索",
                "search": "搜索消息：",
                "paginate": {
                    "next": "下一頁",
                    "previous": "上一頁"
                },
            },
            destroy: true,
            searching: true,
            autoWidth: false,
            "ajax": {
                url: base_url(controllerName['2'] + '/getDataTable'),
                type: 'POST'
            },
            "columns": [{
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '',
                },
                {
                    width: '8%',
                    targets: 1,
                    "data": 0
                },
                {
                    width: '11%',
                    targets: 2,
                    "data": 1
                },
                {
                    width: '11%',
                    targets: 3,
                    "data": 2
                },
                {
                    width: '16%',
                    targets: 4,
                    "data": 3
                },
                {
                    width: '13%',
                    targets: 4,
                    "data": 4
                },
                {
                    width: '11%',
                    targets: 4,
                    "data": 5
                },
                {
                    width: '11%',
                    targets: 4,
                    "data": 6
                }
            ],
            "order": [
                [1, 'asc']
            ]
        })
        tableRow = table;

        // console.log(tableRow.row($(this).closest('tr')));
        $('#' + $datatableName + 'DataTable tbody').on('click', 'td.details-control', function() {
            let tr = $(this).closest('tr');
            let row = table.row(tr);
            let monthPrice;
            let weekPrice;


            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                ajaxPost(base_url(controllerName['2'] + '/getPrice'), tableRow.row(tr).data()[0])
                    .done(function(e) {
                        if (e.price[0] == null) {
                            monthPrice = 0;
                        } else {
                            monthPrice = e.price[0];
                        }
                        if (e.price[1] == null) {
                            weekPrice = 0;
                        } else {
                            weekPrice = e.price[1];
                        }
                        row.child(format(tableRow.row(tr).data(), monthPrice, weekPrice)).show();
                        tr.addClass('shown');
                    });
            }
        });

    }



    function format(data, monthPrice, weekPrice) {

        let table = `<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;"> 
                        <tr> 
                            <td>店家地址:</td> 
                            <td>${data[7]}</td> 
                        </tr> 
                        <tr> 
                            <td>負責人姓名:</td> 
                            <td>${data[8]}</td> 
                        </tr> 
                        <tr> 
                            <td>負責人電話:</td> 
                            <td>${data[9]}</td> 
                        </tr>
                        <tr> 
                            <td>當月消費金額:</td> 
                            <td>${monthPrice}</td> 
                        </tr>
                        <tr> 
                            <td>當週消費金額:</td> 
                            <td>${weekPrice}</td> 
                        </tr>
                    </table>`;

        return table;
    }

    /**
     * 取得要更改的資料進modal
     * @param  String $key,String $datatable,String $controller,String value
     * @return bool
     */
    let getModifyData = (key, value = null) => {
        $obj = {
            key,
            value
        };
        ajaxPost(base_url(controllerName['2'] + '/getModifyData'), $obj)
            .done(function(e) {
                for (let i = 0; i < Object.keys(e.data).length; i++) {
                    $("#modifyForm > div > div > input[name = '" + Object.keys(e.data)[i] + "']").val(Object.values(e.data)[i]);
                    $("#modifyForm > div > div > select[name = '" + Object.keys(e.data)[i] + "']").val(Object.values(e.data)[i]);
                    $("#modifyForm > div > div > textarea[name = '" + Object.keys(e.data)[i] + "']").val(Object.values(e.data)[i]);
                };
                // $("input[name = '" + Object.keys(e.data)[i] + "']").val(Object.values(e.data)[i]);
                $('#modifyModal').modal('show');
                $("#setFile").fileinput("refresh", {
                    initialPreview: e.initialPreview,
                    initialPreviewConfig: e.initialPreviewConfig,
                    initialPreviewAsData: true
                });
                get_file(e.initialPreview, e.initialPreviewConfig, '1');
            });
    }
    /**
     * 更改資料
     * @param  Object $formDom
     * @return bool
     */
    let modifyData = $formDom => {
        swal("確定要修改嗎?", {
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function(value) {
            if (value) {
                let data = $formDom.getFormObject();
                data.judge = (data.admin_password == " ");
                ajaxPost(base_url(controllerName['2'] + '/modifyData'), data)
                    .done(function(e) {
                        if (e.status == 1) {
                            if ($('#setFile').val() == "") {
                                swal("成功", "資料修改成功", "success");
                                $("#modifyForm > div > div > input[name = 'admin_password']").val("");
                                $('#modifyModal').modal('hide');
                                datatable.get();
                            } else if ($('#setFile').val() == undefined) {
                                swal("成功", "資料修改成功", "success");
                                $('#modifyModal').modal('hide');
                                datatable.get();
                            } else {
                                $('#setFile').fileinput('upload');
                                $('#setFile').on('fileuploaded', function(event, data, id, index) {
                                    let obj = {
                                        "img": data.response.name,
                                        "key": $("#modifyForm > div > div > input[name = 'product_key']").val()
                                    };
                                    ajaxPost(base_url(controllerName['2'] + '/modifyInfoImg'), obj)
                                        .done(function(e) {
                                            swal("成功", "資料修改成功", "success");
                                            $("#modifyForm > div > div > input[name = 'admin_password']").val("");
                                            $('#modifyModal').modal('hide');
                                            datatable.get();
                                        });
                                })
                            }
                        } else {
                            swal("失敗", "資料修改失敗，請重新再試", "error");
                            return true;
                        }
                    });
            };
        });
    }

    /**
     * 取得要更改的商品詳細照片
     * @param  String $key,String $datatable,String $controller
     * @return bool
     */
    let getModifyImg = key => {
        $obj = {
            key
        };

        $("#modifyImg > div > div > input[name = product_key ]").val(key);
        ajaxPost(base_url(controllerName['2'] + '/getModifyImg'), $obj)
            .done(function(e) {
                $('#imgModal').modal('show');
                $("#setFileInfo").fileinput("refresh", {
                    initialPreview: e.initialPreview,
                    initialPreviewConfig: e.initialPreviewConfig,
                    initialPreviewAsData: true
                });
                get_file(e.initialPreview, e.initialPreviewConfig, 5 - e.fileCount, 'Info');
            });
    }

    /**
     * 讀出資料庫的照片並Render上頁面
     * @param  Object data,Object data,int $maxCount
     */
    let get_file = (data, dataConfig, $maxCount, $judge = null) => {
        if ($judge != null) {
            $("#fileBoxInfo").html("<input class='file input-lg' type='file' id='setFileInfo' name='setFile[]' multiple>");
            $setFile = "setFileInfo";
        } else {
            $("#fileBox").html("<input class='file input-lg' type='file' id='setFile' name='setFile[]' multiple>");
            $setFile = "setFile";
        }

        // console.log(data);
        // console.log(dataConfig);
        $("#" + $setFile).fileinput({
            language: "zh-TW",
            uploadAsync: true,
            uploadUrl: base_url(controllerName['2'] + '/fileInput/'),
            showZoom: true,
            showClose: false,
            showUpload: false,
            showRemove: false,
            showPreview: true,
            showBrowse: true,
            browseOnZoneClick: true,
            fileActionSettings: {
                showUpload: false,
                showDrag: true
            },
            initialPreview: data,
            initialPreviewConfig: dataConfig,
            // initialPreviewDownloadUrl: "{filename}",
            maxFileCount: $maxCount,
            removeClass: "btn btn-warning",
            maxFileSize: 20480,
            dropZoneEnabled: true,
            initialPreviewAsData: true,
            browseOnZoneClick: true,
            // previewSettings: {
            //     image: {
            //         width: "160px",
            //         height: "120px"
            //     },
            // },
            allowedFileExtensions: ['jpg', 'png', 'jpeg'],
            overwriteInitial: false
            // allowedPreviewTypes: null,
            // allowedPreviewMimeTypes: ["image/bmp", "image/jpeg", "image/png", "image/gif", "application/pdf", "text/plain", "text/csv"],
        }).on('change', function(event) {
            $('#thumb-setFile-init-0').remove();
        }).on('filepreremove', function(event, id, index) {
            // console.log('id = ' + id + ', index = ' + index);
        }).on('filepredelete', function(event, key, jqXHR, data) {
            // console.log('Key = ' + key);
        });
    }
    /**
     * 要刪除的資料
     * @param  String $key,String $datatable,String $controller
     * @return bool
     */
    let deleteData = $key => {
        swal("確定要刪除嗎?", {
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function(value) {
            if (value) {
                ajaxPost(base_url(controllerName['2'] + '/deleteData'), $key)
                    .done(function(e) {
                        swal("成功", "刪除成功", "success");
                        datatable.get();
                    });
            };
        });
    }
    /**
     * 更改商品詳細圖片
     * @param  Object $formDom
     * @return bool
     */
    let modifyImg = $formDom => {
        swal("確定要修改嗎?", {
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function(value) {
            if (value) {
                let count = 0;
                let data = $formDom.getFormObject();
                $('#setFileInfo').fileinput('upload');
                $('#setFileInfo').on('fileuploaded', function(event, data, id, index) {
                    let Arr = [data.files.length];
                    for (let i = 0; i < data.files.length; i++) {
                        Arr[i] = data.files[i].name;
                    }
                    var obj = {
                        "img": Arr,
                        "length": data.files.length,
                        "key": $("#modifyImg > div > div > input[name = product_key ]").val()
                    };
                    if (count == 0) {
                        // console.log(obj);
                        count++;
                        ajaxPost(base_url(controllerName['2'] + '/modifyImg'), obj)
                            .done(function(e) {
                                console.log(e);
                                swal("成功", "資料修改成功", "success");
                                $('#imgModal').modal('hide');
                                datatable.get();
                            });
                    } else {

                    }


                })



            };
        });
    }
    /**
     * 新增問卷題目
     */
    let addQuestion = () => {
        let formCount = $("div [class='form-group']").length;
        if (formCount < 5) {
            let formString = `
                            <div class="form-group" name="group${formCount+1}">
                                <div class= "row">
                                    <div class="col-md-3">
                                        <label for="question${formCount+1}">問題${formCount+1}</label>
                                    </div>
                                    <div class="col-md-8 col-sm-4"></div>
                                    <div class="col-md-1 col-sm-4">
									    <i class="fa fa-trash fa-2x" aria-hidden="true" style="cursor: pointer" onclick="deleteQuestion(${formCount+1})"></i>
								    </div>
                                </div>
                                <input type="text" class="form-control" name="question${formCount+1}" placeholder="問卷">
                            </div>
                            `;
            $('#questionForm').append(formString);
            if ($("div [class='form-group']").length >= 5) {
                $('#addBut').css('display', 'none');
            }
        }
    }
    /**
     * 刪除問卷題目
     * @param  int key
     */
    let deleteQuestion = key => {
        let count = $("div [class='form-group']").length;
        $('div[name="group' + key + '"]').remove();
        for (i = key + 1; i <= count; i++) {
            $('#questionForm > div > div > div > label[for="question' + (i) + '"]').text('問題' + (i - 1));
            $('#questionForm > div > div > div > label[for="question' + (i) + '"]').attr('for', 'question' + (i - 1));
            $('#questionForm > div[name="group' + (i) + '"]').attr('name', 'group' + (i - 1));
            $('#questionForm > div > div > div > i[onclick="deleteQuestion(' + (i) + ')"]').attr('onclick', 'deleteQuestion(' + (i - 1) + ')');
            $('input[name="question' + i + '"]').attr('name', 'question' + (i - 1));
        }
        if ($("div [class='form-group']").length < 5) {
            $('#addBut').css('display', 'inline');
        }
    }
    /**
     * 修改問卷內容
     * @param  Object $formDom, String judge
     */
    let modifyQuestion = ($formDom, $judge) => {
        swal("確定要儲存嗎?", {
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function(value) {
            if (value) {
                let data = $formDom.getFormObject();
                data['judge'] = $judge;
                ajaxPost(base_url(controllerName['2'] + '/modifyQuestion'), data)
                    .done(function(e) {
                        // console.log(e);
                        swal("成功", "儲存成功", "success");
                    });
            };
        });
    }

    let getQuestion = $judge => {
        if ($judge == "admin") {
            ajaxPost(base_url(controllerName['2'] + '/getQuestion'))
                .done(function(e) {
                    for (let i = 1; i <= 5; i++) {
                        // console.log(Object.values(e.data)[0]['question' + i]);
                        if (Object.values(e.data)[0]['question' + i] != null && Object.values(e.data)[0]['question' + i] != " ") {
                            let formString = `
                            <div class="form-group" name="group${i}">
                                <div class= "row"> 
                                    <div class="col-md-3">
                                        <label for="question${i}">問題${i}</label>
                                    </div>
                                    <div class="col-md-8 col-sm-4"></div>
                                    <div class="col-md-1 col-sm-4">
                        			    <i class="fa fa-trash fa-2x" aria-hidden="true" style="cursor: pointer" onclick="deleteQuestion(${i})"></i>
                        		    </div>
                                </div>
                                <input type="text" class="form-control" name="question${i}" placeholder="問卷" value="${Object.values(e.data)[0]['question' + i]}">
                            </div>
                            `;
                            $('#questionForm').append(formString);
                        }
                    }
                    if ($("div [class='form-group']").length >= 5) {
                        $('#addBut').css('display', 'none');
                    }
                });
        } else {
            ajaxPost(base_url(controllerName['2'] + '/getSellerQuestion'))
                .done(function(e) {
                    // console.log(e);
                    for (let i = 1; i <= 5; i++) {
                        // console.log(Object.values(e.data)[0]['question' + i]);
                        if (Object.values(e.data)[0]['question' + i] != null && Object.values(e.data)[0]['question' + i] != " ") {
                            let formString = `
                            <div class="form-group" name="group${i}">
                                <div class= "row"> 
                                    <div class="col-md-3">
                                        <label for="question${i}">問題${i}</label>
                                    </div>
                                    <div class="col-md-8 col-sm-4"></div>
                                    <div class="col-md-1 col-sm-4">
                        			    <i class="fa fa-trash fa-2x" aria-hidden="true" style="cursor: pointer" onclick="deleteQuestion(${i})"></i>
                        		    </div>
                                </div>
                                <input type="text" class="form-control" name="question${i}" placeholder="問卷" value="${Object.values(e.data)[0]['question' + i]}">
                            </div>
                            `;
                            $('#questionForm').append(formString);
                        }
                    }
                    if ($("div [class='form-group']").length >= 5) {
                        $('#addBut').css('display', 'none');
                    }
                });
        }
    }

    /**
     * 查看當筆訂單詳細資料
     * @param  int id
     */
    let checkOrder = key => {
        ajaxPost(base_url(controllerName['2'] + '/checkOrder'), key)
            .done(function(e) {
                let str = "";
                for (const {
                        order_key,
                        user_key,
                        price,
                        amount,
                        order_time,
                        user_pay_time,
                        admin_pay_time,
                        shipped_time,
                        user_member,
                        user_phone,
                        user_priceType,
                        user_encryption,
                        user_area,
                        user_name,
                        user_shop_address,
                        user_shop_phone,
                    } of e.order) {
                    $("#order_key").text(order_key);
                    $("#user_key").text(user_key);
                    $("#price").text(price);
                    $("#amount").text(amount);
                    $("#order_time").text(order_time);
                    // $("#user_pay_time").text(user_pay_time);
                    $("#admin_pay_time").text(admin_pay_time);
                    $("#shipped_time").text(shipped_time);
                    $("#user_member").text(user_member);
                    $("#user_phone").text(user_phone);
                    $("#user_priceType").text(user_priceType);
                    $("#user_encryption").text(user_encryption);
                    $("#user_area").text(user_area);
                    $("#user_name").text(user_name);
                    $("#user_member").text(user_member);
                    $("#user_shop_address").text(user_shop_address);
                    $("#user_shop_phone").text(user_shop_phone);
                    console.log(e);

                }
                for (const {
                        product_key,
                        name,
                        info_name,
                        amount,
                        unit,
                        price
                    } of e.product) {
                    str += `
                    <tr>
                        <td width="10%">${product_key}</td>
                        <td width="15%">${name}</td>
                        <td width="15%">${unit}</td>
                        <td width="15%">${amount}斤</td>
                        <td width="15%">${price}</td>
                    </tr>
                    `;
                }
                $('#productDetail').html(str);
                $("#orderListModal").modal('show');
                // $("#modifyImg > div > div > input[name = info_key ]").val(e.info_key);
            });
    }


    function report_print(href) {
        window.open(href, "_blank", "width=1000, height=" + window.innerHeight + ", left=" + window.innerWidth / 4);
    }
</script>