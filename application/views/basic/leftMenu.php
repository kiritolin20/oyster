<div class="side_bar bg_color scroll_auto">
    <div class="user-panel">
        <!-- <div class="user_image">
            <img src="assets/images/about-1.jpg" class="img-circle mCS_img_loaded" alt="User Image">
        </div>
        <div class="info">
            <p>
                Alexander Pierce
            </p>
            <a href="#"> <i class="fa fa-circle text-success"></i> Online</a>
        </div> -->
    </div>

    <ul id="dc_accordion" class="sidebar-menu tree">
        <li class="menu_sub">
            <a href="<?php echo base_url('Home') ?>"> <i class="fa fa-home"></i> <span>首頁</span></a>
        </li>

        <li class="menu_sub">
            <a href="#"> <i class="fa fa-shopping-cart"></i> <span>商品</span>
                <span class="arrow"></span> </a>
            <ul class="down_menu">
                <li>
                    <a href="<?php echo base_url('Product') ?>">商品管理</a>
                </li>
            </ul>
        </li>

        <li class="menu_sub">
            <a href="#"> <i class="fa fa-user-circle-o"></i> <span>買家</span> <span class="arrow"></span> </a>
            <ul class="down_menu">
                <li>
                    <a href="<?php echo base_url('User') ?>">買家資訊管理</a>
                </li>
            </ul>
        </li>

        <li class="menu_sub">
            <a href="#"> <i class="fa fa-laptop"></i> <span>訂單</span> <span class="arrow"></span> </a>
            <ul class="down_menu">
                <li>
                    <a href="<?php echo base_url('AddOrder') ?>">新增訂單</a>
                </li>
                <li>
                    <a href="<?php echo base_url('Order') ?>">未出貨訂單查詢</a>
                </li>
                <li>
                    <a href="<?php echo base_url('Pay') ?>">未收款訂單查詢</a>
                </li>
                <li>
                    <a href="<?php echo base_url('History') ?>">歷史訂單查詢</a>
                </li>
            </ul>
        </li>

        <li class="menu_sub">
            <a href="#"> <i class="fa fa-pie-chart"></i> <span>統計</span> <span class="arrow"></span> </a>
            <ul class="down_menu">
                <li>
                    <a href="<?php echo base_url('Statistics') ?>">月報表</a>
                    <a href="<?php echo base_url('DayStatistics') ?>">日報表</a>
                    <!-- <a href="<?php echo base_url('Shipper') ?>">出貨單</a> -->
                </li>
            </ul>
        </li>
    </ul>
</div>