<footer class="footer ptb-20">
    <div class="row">
        <div class="col-md-5 text-center">

            <?php $this->load->view('basic/backTop') ?>
        </div>
        <div class="col-md-7">
            <div class="copy_right" style="margin-left:150px">
                <p>
                    2020 © 樹德科技大學L0726
                </p>
            </div>
        </div>
    </div>
</footer>