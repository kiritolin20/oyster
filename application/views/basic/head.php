<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <!-- headSet -->
    <?php $this->load->view('basic/head/headSet') ?>
    <!-- Libs CSS -->
    <?php $this->load->view('basic/head/libsCss') ?>
    <!-- basic Functions-->
    <?php $this->load->view('basic/head/basicViewFunctions') ?>
    <!-- Libs JS -->
    <?php $this->load->view('basic/head/libsJs') ?>