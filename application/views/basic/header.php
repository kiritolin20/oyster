<header class="main-header bg_orange">
    <!-- Logo -->
    <div class="container_header">

        <?php $this->load->view('basic/header/topHeader/logo') ?>

        <div class="right_detail">
            <div class="d-flex align-items-center min-h pos-md-r">
                <!-- <div class="col-xl-5 col-1 search_col ">
                    <div class="top_function">
                       <?php // $this->load->view('basic/header/topHeader/searchBox') ?>
                    </div>
                </div> -->


                <div class="col-xl-12 col-12 d-flex justify-content-end">
                    <div class="right_bar_top d-flex align-items-center">
                        <!-- 通知選單 -->
                        <div class="dropdown dropdown-notification">
                            <?php $this->load->view('basic/header/menuRight/notifications') ?>
                        </div>
                        <!-- 個人選單 -->
                        <div class="dropdown dropdown-user">
                            <?php $this->load->view('basic/header/menuRight/user') ?>
                        </div>
                        <!-- Dropdown_User_End -->
                    </div>
                </div>
            </div>
        </div>
    </div>

</header>