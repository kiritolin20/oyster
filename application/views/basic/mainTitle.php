<!-- breadcrumb -->
<div class="page-heading">
    <div class="row d-flex align-items-center">
        <div class="col-md-6">
            <div class="page-breadcrumb">
                <h1><?php echo $mainTitle ?></h1>
            </div>
        </div>
        <div class="col-md-6 justify-content-md-end d-md-flex">
            <div class="breadcrumb_nav">
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a class="parent-item" href="#"><?php echo $upperTitle ?></a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">
                        <?php echo $mainTitle ?>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb_End -->