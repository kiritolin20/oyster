<!--頁頭載入-->
<?php $this->load->view('basic/head')  ?>
    <!--每個頁面若是有獨立用到JS套件或獨立的樣式都在這裡做設定-->
    <?php
        if($extraCSS) $this->load->view("{$pageName}/{$extraCSS}");
        if($extraJS) $this->load->view("{$pageName}/{$extraJS}");
    ?>
</head>

<!--body的Class在模板中不同頁面皆不同-->
<body class="<?php echo $bodyClass ?>">
    <div class="all">
        <?php $this->load->view('basic/header') ?>

        <!--main為一個頁面內容的開始處-->
        <main>
            <?php
                if($mainTitle) $this->load->view('basic/mainTitle');
                if($component){
                    foreach ($component as $key => $name) {
                        $this->load->view("{$pageName}/{$name}");
                    }
                }
            ?>
        </main>
        <!-- 頁腳 -->
        <?php $this->load->view('basic/footer') ?>
    </div>

    <!-- 回頂部按鈕 -->
    <?php $this->load->view('basic/backTop') ?>

    <!-- 頁面載入動畫 -->
    <?php $this->load->view('basic/pageLoader') ?>
</body>
</html>
