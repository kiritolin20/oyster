<div class="chart_section" style="min-height:76.6vh">
    <!-- state start-->
    <div class="row">
        <div class=" col-md-6">
            <div class="card card-shadow mb-4">
                <div class="card-header">
                    <div class="card-title">
                        <h2 style="padding-top:7px"><strong>賣家問卷設定</strong></h2>
                    </div>
                </div>
                <div class="card-body">
                    <form id="sellerQuestion">
                        <div id="questionForm">
                        </div>
                        <div class="preview col-md-12 col-sm-12" id="addBut">
                            <div class="row">
                                <div class="preview col-md-5 col-sm-5"></div>
                                <div class="preview col-md-6 col-sm-6">
                                    <i class="fa fa-plus-circle fa-3x" aria-hidden="true" style="cursor: pointer" onclick="addQuestion()"></i>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">
                            儲存問卷
                        </button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    basic.pushReady(function() {
        getQuestion('seller');
    });
</script>