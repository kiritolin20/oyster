<section class="chart_section" style="min-height:76.6vh">
    <div class="row">

        <div class="col-lg-12 mb-12">
            <div class="card card-shadow">
                <div class="card-header">
                    <div class="card-title">
                        <div class="row">
                            <div class="col-md-3">
                                <h2 style="padding-top:7px"><strong>賣家問卷回答</strong></h2>
                            </div>

                            <div class="col-md-10"></div>
                            <!-- <div class="col-md-1">
                                <button type="button" class="btn btn-outline-info" onclick="showInsertModal(1)">
                                    商品上架
                                </button>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="card-body">
                            <div class=table-responsive">
                                <table id="sellerDataTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
                                    <thead>
                                        <tr id="sellerQuestionTr">
                                        </tr>
                                    </thead>
                                    <tbody id="tbody">
                                    </tbody>
                                </table>
                                <!-- Large modal -->
                                <!-- <button type="button" class="btn btn-dark" data-toggle="modal" data-target=".bd-example-modal-lg">
                                Large modal
                            </button> -->
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="row" style="margin-top: 25px">

        <div class="col-lg-12 mb-12">
            <div class="card card-shadow">
                <div class="card-header">
                    <div class="card-title">
                        <div class="row">
                            <div class="col-md-3">
                                <h2 style="padding-top:7px"><strong>管理員問卷回答</strong></h2>
                            </div>

                            <div class="col-md-10"></div>
                            <!-- <div class="col-md-1">
                                <button type="button" class="btn btn-outline-info" onclick="showInsertModal(1)">
                                    商品上架
                                </button>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="card-body">
                            <div class=table-responsive">
                                <table id="adminDataTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
                                    <thead>
                                        <tr id="adminQuestionTr">
                                            </th>
                                    </thead>
                                    <tbody id="tbody">
                                    </tbody>
                                </table>
                                <!-- Large modal -->
                                <!-- <button type="button" class="btn btn-dark" data-toggle="modal" data-target=".bd-example-modal-lg">
                                Large modal
                            </button> -->
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

</section>


<script type="text/javascript">
    var datatable = {
        get: function() {
            getDataTable('seller',2);
            getSecDataTable('admin',3);
        }
    }
    let getQuestionTh = () => {
        ajaxPost(base_url(controllerName['2'] + '/getQuestionTh'))
            .done(function(e) {
                let seller = "";
                let admin = "";
                
                for (let i = 1; i <= 5; i++) {
                    if (Object.values(e.data)[0][0]['question' + i] != null && Object.values(e.data)[0][0]['question' + i] != " ") {
                        admin += `<th >${Object.values(e.data)[0][0]['question' + i]}</th>`;
                    }
                }
                for (let i = 1; i <= 5; i++) {
                    if (Object.values(e.data)[1][0]['question' + i] != null && Object.values(e.data)[1][0]['question' + i] != " ") {
                        seller += `<th >${Object.values(e.data)[1][0]['question' + i]}</th>`;
                    }
                }

                $('#sellerQuestionTr').append(seller);
                $('#adminQuestionTr').append(admin);
            });
    }
    /**
     * 取得該頁面的dataTable
     * @param  String $datatableName,String $controllerName,Array $colArray 不排列的資料欄位
     * @return datatable
     */
    let getSecDataTable = ($datatableName, ...col) => {
        $('#' + $datatableName + 'DataTable').DataTable({
            "language": {
                "lengthMenu": "顯示 _MENU_ 筆消息",
                "emptyTable": "沒有資料",
                "info": "目前顯示 _START_ 至 _END_ 筆的資料，共 _TOTAL_ 筆資料",
                "infoFiltered": "，從 _MAX_ 筆資料中過濾",
                "infoEmpty": "沒有資料能夠顯示",
                "zeroRecords": "沒有資料，可以鍵入其他內容進行搜索",
                "search": "搜索消息：",
                "paginate": {
                    "next": "下一頁",
                    "previous": "上一頁"
                },
            },
            destroy: true,
            searching: true,
            "order": [],
            "ajax": {
                url: base_url(controllerName['2'] + '/getSecDataTable'),
                type: 'POST'
            },
            "columnDefs": [{
                "targets": col,
                "orderable": false,
            }, ],
        })
    }
    basic.pushReady(function() {
        getQuestionTh();
    });
</script>