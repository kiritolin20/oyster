<section class="chart_section" style="min-height:76.6vh">
    <div class="row">

        <div class="col-lg-12 mb-5">
            <div class="card card-shadow">
                <div class="card-header">
                    <div class="card-title">
                        <div class="row">
                            <div class="col-md-3">
                                <h2 style="padding-top:7px"><strong>歷史訂單管理</strong></h2>
                            </div>
                            <!-- <div class="col-md-1">
                                <button type="button" class="btn btn-outline-info" onclick="showInsertModal(1)">
                                    商品上架
                                </button>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="card-body">
                            <table id="orderDataTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>編號</th>
                                        <th>總額</th>
                                        <th>狀態</th>
                                        <th>下訂時間</th>
                                        <th>出貨時間</th>
                                        <th>訂購店家</th>
                                        <th>連絡電話</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                </tbody>
                            </table>
                            <!-- Large modal -->
                            <!-- <button type="button" class="btn btn-dark" data-toggle="modal" data-target=".bd-example-modal-lg">
                                Large modal
                            </button> -->
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

</section>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="insertModalLabel" aria-hidden="true" id="insertModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="insertModalLabel">新增商品資料</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="insertForm" method="post">
                    <div class="row">
                        <div class="form-group  col-md-12">
                            <label style="font-size:16px">商品展示圖片</label>
                            <div id="insertfile"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>商品種類名稱</label>
                            <input name="product_name" class="form-control" placeholder="Name">
                        </div>
                        <div class="form-group  col-md-6">
                            <label>商品詳細名稱</label>
                            <input name="product_info_name" type="text" class="form-control" placeholder="Info">
                        </div>
                        <div class="form-group  col-md-3">
                            <label>商品價錢</label>
                            <input name="product_price" type="text" class="form-control" placeholder="Price">
                        </div>
                        <div class="form-group  col-md-3">
                            <label>商品數量</label>
                            <input name="product_amount" type="number" class="form-control" placeholder="Amount">
                        </div>
                        <div class="form-group  col-md-3">
                            <label>商品是否顯示</label>
                            <select class="form-control" name="product_display">
                                <option value="1">是</option>
                                <option value="0">否</option>
                            </select>
                        </div>
                        <div class="form-group  col-md-3">
                            <label>商品介紹是否顯示</label>
                            <select class="form-control" name="product_info_display">
                                <option value="1">是</option>
                                <option value="0">否</option>
                            </select>
                        </div>
                        <div class="form-group  col-md-12">
                            <label>商品介紹</label>
                            <textarea name="product_introduce" class="form-control" cols="60" rows="5" style="margin-top: 0px; margin-bottom: 0px; height: 115px;"></textarea>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    關閉
                </button>
                <button type="submit" class="btn btn-primary">
                    新增
                </button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="modifyModalLabel" aria-hidden="true" id="modifyModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modifyModalLabel">修改商品資料</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="modifyForm" method="post">
                    <div class="row">
                        <div class="form-group  col-md-12">
                            <label style="font-size:16px">商品展示圖片</label>
                            <div id="fileBox"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>商品種類名稱</label>
                            <input name="product_name" class="form-control" placeholder="Name">
                        </div>
                        <div class="form-group  col-md-6">
                            <label>商品詳細名稱</label>
                            <input name="product_info_name" type="text" class="form-control" placeholder="Info">
                        </div>
                        <div class="form-group  col-md-3">
                            <label>商品價錢</label>
                            <input name="product_price" type="text" class="form-control" placeholder="Price">
                        </div>
                        <div class="form-group  col-md-3">
                            <label>商品數量</label>
                            <input name="product_amount" type="number" class="form-control" placeholder="Amount">
                        </div>
                        <div class="form-group  col-md-3">
                            <label>商品是否顯示</label>
                            <select class="form-control" name="product_display">
                                <option value="1">是</option>
                                <option value="0">否</option>
                            </select>
                        </div>
                        <div class="form-group  col-md-3">
                            <label>商品介紹是否顯示</label>
                            <select class="form-control" name="product_info_display">
                                <option value="1">是</option>
                                <option value="0">否</option>
                            </select>
                        </div>
                        <div class="form-group  col-md-12">
                            <label>商品介紹</label>
                            <textarea name="product_introduce" class="form-control" cols="60" rows="5" style="margin-top: 0px; margin-bottom: 0px; height: 115px;"></textarea>
                        </div>

                        <div class="form-group  col-md-12" style="display:none">
                            <label>key</label>
                            <input name="product_key" class="form-control" placeholder="key">
                        </div>
                        <div class="form-group  col-md-12" style="display:none">
                            <label>info_key</label>
                            <input name="product_info_key" class="form-control" placeholder="key">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    關閉
                </button>
                <button type="submit" class="btn btn-primary">
                    修改
                </button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="imgModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myLargeModalLabel">修改商品圖片</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="modifyImg" method="post">
                    <div class="row">
                        <div class="form-group  col-md-12">
                            <label style="font-size:16px">商品詳細圖片</label>
                            <div id="fileBoxInfo"></div>
                        </div>
                        <div class="form-group  col-md-6" style="display:none">
                            <label>product_info_key</label>
                            <input name="info_key" class="form-control" placeholder="key">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    關閉
                </button>
                <button type="submit" class="btn btn-primary" onclick="modifyImg()">
                    修改
                </button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="modifyModalLabel" aria-hidden="true" id="orderListModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="text-align: center">
            <div class="modal-header">
                <h3 style="font-family:微軟正黑體;text-align: center;margin-left: 45%;">
                    <strong>訂單明細</strong>
                </h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <table class="table table-striped m-0">
                <thead>
                    <tr>
                        <th style="width:33.3%">訂單編號</th>
                        <th style="width:33.3%">總額</th>
                        <th style="width:33.3%">總數量</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td id="order_key"></td>
                        <td id="price"></td>
                        <td id="amount"></td>

                    </tr>
                </tbody>
            </table>
            <table class="table table-striped m-0">
                <thead>
                    <tr>
                        <th style="width:15%">店家序號</th>
                        <th style="width:10%">區域</th>
                        <th style="width:20%">訂購店家</th>
                        <th style="width:35%">運送地址</th>
                        <th style="width:20%">店家電話</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td id="user_encryption"></td>
                        <td id="user_area"></td>
                        <td id="user_name"></td>
                        <td id="user_shop_address"></td>
                        <td id="user_shop_phone"></td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-striped m-0">
                <thead>
                    <tr>
                        <th style="width:33%">店家負責人</th>
                        <th style="width:33%">負責人電話</th>
                        <th style="width:33%">結帳方式</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td id="user_member"></td>
                        <td id="user_phone"></td>
                        <td id="user_priceType"></td>

                    </tr>
                </tbody>
            </table>
            <table class="table table-striped m-0">
                <thead>
                    <tr>
                        <th style="width:33%">訂單建立時間</th>
                        <th style="width:33%">管理員確認付款時間</th>
                        <th style="width:33%">出貨時間</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td id="order_time"></td>
                        <td id="admin_pay_time"></td>
                        <td id="shipped_time"></td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-striped m-0" id="productTable">
                <thead>
                    <tr>
                        <th>商品序號</th>
                        <th>商品名稱</th>
                        <th>商品售價單位</th>
                        <th>數量</th>
                        <th>小計</th>
                    </tr>
                </thead>
                <tbody id="productDetail">

                </tbody>
            </table>
            <!-- Example split danger button -->
            <div class="modal-footer">
                <button class="btn btn-close" aria-hidden="true" data-dismiss="modal" id="Recancel">取消</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var datatable = {
        get: function() {
            getDataTable('order', [{
                    "targets": [0, 7],
                    "orderable": false,
                },
                {
                    width: '10%',
                    targets: 0
                },
                {
                    width: '14%',
                    targets: 1
                },
                {
                    width: '14%',
                    targets: 2
                },
                {
                    width: '13%',
                    targets: 3
                },
                {
                    width: '14%',
                    targets: 4
                },
                {
                    width: '15%',
                    targets: 5
                },
                {
                    width: '10%',
                    targets: 6
                },
                {
                    width: '7%',
                    targets: 7
                }
            ]);
        }
    }
    // basic.pushReady(function() {
    // });
</script>