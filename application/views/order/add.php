<section class="chart_section" style="min-height:76.6vh">
    <div class="row">
        <div class="col-lg-12 mb-5">
            <div class="card card-shadow">
                <div class="card-header">
                    <div class="card-title">
                        <div class="row">
                            <div class="col-md-3">
                                <h2 style="padding-top:7px"><strong>新增訂單</strong></h2>
                            </div>

                            <div class="col-md-7"></div>
                            <div class="col-md-2" style="text-align: right;">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="inputEmail4">區域</label>
                                    <select name="user_area" class="form-control" id="user_area">
                                        <option value>不設限制</option>
                                        <option value="e">東</option>
                                        <option value="s">南</option>
                                        <option value="w">西</option>
                                        <option value="n">北</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="inputEmail4">店家</label>
                                    <select name="addUserList" class="form-control" id="addUserList">
                                        <option value="e">東</option>
                                        <option value="s">南</option>
                                        <option value="w">西</option>
                                        <option value="n">北</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="inputEmail4">功能</label>
                                    <button type="button" style="border-radius: 5px" class="btn btn-outline-info form-control" onclick="addProduct()">
                                        新增商品
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="inputEmail4">商品</label>
                                </div>
                            </div>
                            <div id="product_list">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-header">
                    <div class="card-title">
                        <div class="row">
                            <div class="col-md-8">
                                <h2 style="padding-top:7px"><strong>總金額：<span id="prodcutTotal"></span></strong></h2>
                            </div>
                            <div class="col-md-2" style="text-align: right">
                                <button type="button" style="border-radius: 5px" class="btn btn-outline-warning" onclick="calcTotal()">
                                    計算總金額
                                </button>
                            </div>
                            <div class="col-md-2">
                                <button type="button" style="border-radius: 5px" class="btn btn-outline-info" onclick="setOrder()">
                                    送出訂單
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

<script type="text/javascript">
    basic.pushReady(function() {
        getDB.getUserData();
        getDB.getPrductData();
    });
    let getDB = {
        getUserData: function() {
            ajaxPost(base_url(controllerName['2'] + '/getUserData'))
                .done(function(data) {
                    let str = `<option value selected disabled>請選擇店家</option>`;
                    for (let index = 0; index < data.length; index++) {
                        str +=
                            `<option value="${data[index]["hash_key"]}">${data[index]["user_name"]}</option>`;
                    }
                    $("#addUserList").html(str);
                });
        },
        getPrductData: function(getfavorite = "") {
            let str = `<option value selected disabled>請選擇商品</option>`;
            /**
             * [開啟同步]
             */
            $.ajax({
                url: base_url(controllerName['2'] + '/getPrductData'),
                type: 'POST',
                dataType: 'json',
                async: false,
            }).done(function(data) {
                str = `<option value selected disabled>請選擇商品</option>`;
                for (let index = 0; index < data.length; index++) {
                    if (data[index]["key"] == getfavorite) {
                        str +=
                            `<option value="${data[index]["key"]}" data-name="${data[index]["name"]}" data-img="${data[index]["img"]}" data-price="${data[index]["price"]}" data-unit="${data[index]["unit"]}" selected>${data[index]["name"]}${data[index]["unit"]}</option>`;
                    } else {
                        str +=
                            `<option value="${data[index]["key"]}" data-name="${data[index]["name"]}" data-img="${data[index]["img"]}" data-price="${data[index]["price"]}" data-unit="${data[index]["unit"]}">${data[index]["name"]}${data[index]["unit"]}</option>`;
                    }
                }
            });
            return str;
        }
    }
    var product_options = getDB.getPrductData();
    let addProduct = () => {
        let size = $("#product_list select").length + 1;
        if (size > 5) {
            swal("商品最多新增五筆", "", "warning");
        } else {
            let html = `
            <div class="delPrdouctDiv" style="display: flex;">
                <div class="form-group col-md-3">
                    <select name="product" class="form-control prdouctSelect">
                        ${getDB.getPrductData()}
                    </select>
                </div>
                <div class="input-group form-group col-md-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">NT.</span>
                    </div>
                    <input type="number" class="form-control priceValue" value="" disabled>
                    <div class="input-group-prepend">
                        <span class="input-group-text">元</span>
                    </div>
                </div>
                <div class="input-group form-group col-md-3">
                    <input type="text" class="form-control priceInput" placeholder="填寫數量(斤)">
                    <div class="input-group-prepend">
                        <span class="input-group-text">斤</span>
                    </div>
                </div>
                <div class="form-group col-md-2">
                    <button type="button" style="border-radius: 5px" class="btn btn-outline-danger" onclick="delPrdouctDiv(this)">
                        移除商品
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>`;
            $("#product_list").append(html);
        }
    }

    /**
     * [移除商品列]
     */
    let delPrdouctDiv = (e) => {
        $(e).parent().parent().remove();
    }
    /**
     * [區域變動時，更新店家資料]
     */
    $("body").on("change", "#user_area", function() {
        ajaxPost(base_url(controllerName['2'] + '/getUserData'), $(this).val())
            .done(function(data) {
                let str = `<option value selected disabled>請選擇店家</option>`;
                for (let index = 0; index < data.length; index++) {
                    str +=
                        `<option value="${data[index]["hash_key"]}">${data[index]["user_name"]}</option>`;
                }
                $("#addUserList").html(str);
            });
    });
    /**
     * [店家變動時，更新最愛商品]
     */
    $("body").on("change", "#addUserList", function() {
        ajaxPost(base_url(controllerName['2'] + '/getUserfavorite'), $(this).val())
            .done(function(data) {
                let html = "";
                for (let index = 0; index < data.length; index++) {
                    html += `
            <div class="delPrdouctDiv" style="display: flex;">
                <div class="form-group col-md-3">
                    <select name="product" class="form-control prdouctSelect">
                        ${getDB.getPrductData(data[index]["key"])}
                    </select>
                </div>
                <div class="input-group form-group col-md-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">NT.</span>
                    </div>
                    <input type="number" class="form-control priceValue" value="${data[index]["price"]}" disabled>
                    <div class="input-group-prepend">
                        <span class="input-group-text">元</span>
                    </div>
                </div>
                <div class="input-group form-group col-md-3">
                    <input type="text" class="form-control priceInput" placeholder="填寫數量(斤)">
                    <div class="input-group-prepend">
                        <span class="input-group-text">斤</span>
                    </div>
                </div>
                <div class="form-group col-md-2">
                    <button type="button" style="border-radius: 5px" class="btn btn-outline-danger" onclick="delPrdouctDiv(this)">
                        移除商品
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>`;
                }
                $("#product_list").html(html);
            });
    });
    /**
     * [選擇商品後更新價錢]
     */
    $("body").on("change", ".prdouctSelect", function() {
        let price = $(this).find("option:selected").data("price");
        $(this).parent().parent().find("input")[0].value = price;
    })
    /**
     * [計算總金額]
     */
    let calcTotal = () => {
        let size = $("#product_list select").length;
        let totalPrice = 0;
        for (let index = 0; index < size; index++) {
            let price = parseFloat($(".priceValue")[index].value);
            let amount = parseFloat($(".priceInput")[index].value);
            totalPrice += parseFloat(price * amount);
        }
        $("#prodcutTotal").text(totalPrice);
    }
    /**
     * [計算總金額]
     */
    let setOrder = () => {
        if($("#addUserList").val() == "" || $("#addUserList").val() == null){
            swal("請選擇購買店家!","","warning")
            return false;
        }
        let size = $("#product_list select").length;
        for (let index = 0; index < size; index++) {
            let amount = parseFloat($(".priceInput")[index].value);
            if(isNaN(amount) || amount == 0 || amount == null){
                swal("請正確填寫商品購買數量!","","warning")
                return false;
            }
        }
        let user = $("#addUserList").val();
        let data = [{
            user: user
        }];
        for (let index = 0; index < size; index++) {
            let prdouctSelect = $(".prdouctSelect")[index].value;
            let price = parseFloat($(".priceValue")[index].value);
            let amount = parseFloat($(".priceInput")[index].value);
            let img = $(".prdouctSelect").find("option:selected")[index].dataset["img"];
            let unit = $(".prdouctSelect").find("option:selected")[index].dataset["unit"];
            let name = $(".prdouctSelect").find("option:selected")[index].dataset["name"]
            data.push({
                prdouct: prdouctSelect,
                price: price,
                amount: amount,
                img: img,
                name: name,
                unit: unit
            });
        }
        ajaxPost(base_url(controllerName['2'] + '/setOrder'), data)
        .done(function (data) {
            if (data) {
                swal("已完成訂單","","success").then(function() {
                    location.reload();
                });
            } else {
                swal("訂單失敗","","error")
            }
        });
    }
</script>