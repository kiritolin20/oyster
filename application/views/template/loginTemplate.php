<!--頁頭載入-->
<?php $this->load->view('basic/head')  ?>
<!-- 頁面所需的額外Script載入放在這裡 -->
<?php echo $headComponents ?? "" ?>
<!-- 頁面所需的額外Script載入放在這裡 -->
</head>

<!--body的Class在模板中不同頁面皆不同-->

<body class="<?php echo $bodyClass ?>">
    <!--main為一個頁面內容的開始處-->
    <main>
        <div class="sufee-login d-flex align-content-center flex-wrap">
            <div class="container">
                <?php echo $components ?? "" ?>
            </div>
        </div>
    </main>
</body>

</html>