<!--頁頭載入-->
<?php $this->load->view('basic/head')  ?>
<!-- 頁面所需的額外Script載入放在這裡 -->
<?php echo $headComponents ?? "" ?>
<!-- 頁面所需的額外Script載入放在這裡 -->
</head>

<!--body的Class在模板中不同頁面皆不同-->

<body class="<?php echo $bodyClass ?>">
    <div class="wrapper">
        <?php $this->load->view('basic/header') ?>

        <!--main為一個頁面內容的開始處-->
        <div class="container_full">
            <?php $this->load->view('basic/leftMenu') ?>
            <main>
                <div class="content_wrapper">
                    <div class="container-fluid">
                        <?php $this->load->view('basic/mainTitle') ?>
                        <?php echo $components ?? "" ?>
                    </div>
                </div>
            </main>
        </div>
        <!-- 頁腳 -->
        <?php $this->load->view('basic/footer') ?>
    </div>

    <!-- 回頂部按鈕 -->
    <?php $this->load->view('basic/backTop') ?>

    <!-- 頁面載入動畫 -->
    <?php $this->load->view('basic/pageLoader') ?>
</body>

</html>