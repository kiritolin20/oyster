<section class="chart_section" style="min-height:76.6vh">
    <div class="row">

        <div class="col-lg-12 mb-5">
            <div class="card card-shadow">
                <div class="card-header">
                    <div class="card-title">
                        <div class="row">
                            <div class="col-md-4">
                                <h2 style="padding-top:7px"><strong>本月報表</strong></h2>
                            </div>

                            <div class="col-md-4">
                                <select id="selectYear" class="form-control">
                                    <option value="0" disabled>請選擇一個年份</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <select id="selectMonth" class="form-control">
                                    <option value="0" disabled>請選擇一個月份</option>
                                </select>
                            </div>
                            <!-- <div class="col-md-1">
                                <button type="button" class="btn btn-outline-info" onclick="showInsertModal(1)">
                                    商品上架
                                </button>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="card-body" style="display: flex;">
                            <div class=table-responsive" style="width:50%">
                                <h1 style="text-align: center;" id="titleYear"></h1>
                                <div id="turnoverPrice" style="height:500px">
                                </div>
                            </div>
                            <div class=table-responsive" style="width:50%">
                                <h1 style="text-align: center;" id="titleMonth"></h1>
                                <div id="turnoverAmount" style="height:500px">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    var datatable = {
        get: function() {
            getDataTable('history', 6);
        }
    }
    var d = new Date();
    var getNowYear = d.getFullYear();
    var getNowMonth = d.getMonth() + 1;
    basic.pushReady(function() {
        SelectYear();
        $('#selectYear').val(getNowYear);
        $('#selectMonth').val(getNowMonth);
    });

    $('#selectYear').change(function(event) {
        if ($('#selectYear').val() != 0) {
            getTurnover($('#selectYear').val(), $('#selectMonth').val());
            $('#titleYear').text(`${$('#selectYear').val()}年度${$('#selectMonth').val()}月營業額`);
            $('#titleMonth').text(`${$('#selectYear').val()}年度${$('#selectMonth').val()}月營業總數`);
        }
    });

    $('#selectMonth').change(function(event) {
        if ($('#selectMonth').val() != 0) {
            getTurnover($('#selectYear').val(), $('#selectMonth').val());
            $('#titleYear').text(`${$('#selectYear').val()}年度${$('#selectMonth').val()}月營業額`);
            $('#titleMonth').text(`${$('#selectYear').val()}年度${$('#selectMonth').val()}月營業總數`);
        }
    });

    function SelectYear() {
        var selectYear = "";
        var selectMonth = "";
        var d = new Date();
        var getYear = d.getFullYear();
        var getMonth = d.getMonth() + 1;
        for (var i = 1; i <= 10; i++) {
            selectYear += "<option value=" + (getYear - i + 1) + ">" + (getYear - i + 1) + "年</option>";
        }
        for (var i = 1; i <= 12; i++) {
            selectMonth += "<option value=" + i + ">" + i + "月</option>";
        }
        $('#selectYear').append(selectYear);
        $('#selectMonth').append(selectMonth);
        $('#titleYear').text(`${getYear}年度${getMonth}月營業額`);
        $('#titleMonth').text(`${getYear}年度${getMonth}月營總數`);
        getTurnover(getYear, getMonth);
    }

    function getTurnover(year = getNowYear, month = getNowMonth) {
        ajaxPost(base_url(controllerName['2'] + '/getTurnover'), {
                year,
                month
            })
            .done(function(e) {
                turnOverChart(e);
                console.log(e);
            });
        ajaxPost(base_url(controllerName['2'] + '/getTotalAmountChart'), {
                year,
                month
            })
            .done(function(e) {
                getTotalAmountChart(e);
                // console.log(e);
            });
    }

    function turnOverChart(data) {
        let datatArr = [];
        for (let index = 0; index < data.length; index++) {
            let obj = {
                "unit": data[index]['unit'],
                "turnover": data[index]['turnover']
            };
            datatArr.push(obj);
        }
        am4core.ready(function() {

            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("turnoverPrice", am4charts.XYChart);
            chart.scrollbarX = new am4core.Scrollbar();

            // Add data
            chart.data = datatArr;
            // 原始格式
            // [{
            //     "unit": data[0]['unit'],
            //     "turnover": data[0]['turnover']
            // }]
            prepareParetoData();

            function prepareParetoData() {
                var total = 0;

                for (var i = 0; i < chart.data.length; i++) {
                    var value = chart.data[i].turnover;
                    total += value;
                }

                var sum = 0;
                for (var i = 0; i < chart.data.length; i++) {
                    var value = chart.data[i].turnover;
                    sum += value;
                    chart.data[i].pareto = sum / total * 100;
                }
            }

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "unit";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 60;
            categoryAxis.tooltip.disabled = true;

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.minWidth = 50;
            valueAxis.min = 0;
            valueAxis.cursorTooltipEnabled = false;

            // Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.sequencedInterpolation = true;
            series.dataFields.valueY = "turnover";
            series.dataFields.categoryX = "unit";
            series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
            series.columns.template.strokeWidth = 0;

            series.tooltip.pointerOrientation = "vertical";

            series.columns.template.column.cornerRadiusTopLeft = 10;
            series.columns.template.column.cornerRadiusTopRight = 10;
            series.columns.template.column.fillOpacity = 0.8;

            // on hover, make corner radiuses bigger
            var hoverState = series.columns.template.column.states.create("hover");
            hoverState.properties.cornerRadiusTopLeft = 0;
            hoverState.properties.cornerRadiusTopRight = 0;
            hoverState.properties.fillOpacity = 1;

            series.columns.template.adapter.add("fill", (fill, target) => {
                return chart.colors.getIndex(target.dataItem.index);
            })


            var paretoValueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            paretoValueAxis.renderer.opposite = true;
            paretoValueAxis.min = 0;
            paretoValueAxis.max = 100;
            paretoValueAxis.strictMinMax = true;
            paretoValueAxis.renderer.grid.template.disabled = true;
            paretoValueAxis.numberFormatter = new am4core.NumberFormatter();
            paretoValueAxis.numberFormatter.numberFormat = "#'%'"
            paretoValueAxis.cursorTooltipEnabled = false;

            // var paretoSeries = chart.series.push(new am4charts.LineSeries())
            // paretoSeries.dataFields.valueY = "pareto";
            // paretoSeries.dataFields.categoryX = "month";
            // paretoSeries.yAxis = paretoValueAxis;
            // paretoSeries.tooltipText = "pareto: {valueY.formatNumber('#.0')}%[/]";
            // paretoSeries.bullets.push(new am4charts.CircleBullet());
            // paretoSeries.strokeWidth = 2;
            // paretoSeries.stroke = new am4core.InterfaceColorSet().getFor("alternativeBackground");
            // paretoSeries.strokeOpacity = 0.5;

            // Cursor
            chart.cursor = new am4charts.XYCursor();
            chart.cursor.behavior = "panX";
        });
    }

    function getTotalAmountChart(data) {
        let datatArr = [];
        for (let index = 0; index < data.length; index++) {
            let obj = {
                "unit": data[index]['unit'],
                "turnover": data[index]['turnover']
            };
            datatArr.push(obj);
        }
        am4core.ready(function() {

            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("turnoverAmount", am4charts.XYChart);
            chart.scrollbarX = new am4core.Scrollbar();

            // Add data
            chart.data = datatArr;

            prepareParetoData();

            function prepareParetoData() {
                var total = 0;

                for (var i = 0; i < chart.data.length; i++) {
                    var value = chart.data[i].turnover;
                    total += value;
                }

                var sum = 0;
                for (var i = 0; i < chart.data.length; i++) {
                    var value = chart.data[i].turnover;
                    sum += value;
                    chart.data[i].pareto = sum / total * 100;
                }
            }

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "unit";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 60;
            categoryAxis.tooltip.disabled = true;

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.minWidth = 50;
            valueAxis.min = 0;
            valueAxis.cursorTooltipEnabled = false;

            // Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.sequencedInterpolation = true;
            series.dataFields.valueY = "turnover";
            series.dataFields.categoryX = "unit";
            series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
            series.columns.template.strokeWidth = 0;

            series.tooltip.pointerOrientation = "vertical";

            series.columns.template.column.cornerRadiusTopLeft = 10;
            series.columns.template.column.cornerRadiusTopRight = 10;
            series.columns.template.column.fillOpacity = 0.8;

            // on hover, make corner radiuses bigger
            var hoverState = series.columns.template.column.states.create("hover");
            hoverState.properties.cornerRadiusTopLeft = 0;
            hoverState.properties.cornerRadiusTopRight = 0;
            hoverState.properties.fillOpacity = 1;

            series.columns.template.adapter.add("fill", (fill, target) => {
                return chart.colors.getIndex(target.dataItem.index);
            })


            var paretoValueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            paretoValueAxis.renderer.opposite = true;
            paretoValueAxis.min = 0;
            paretoValueAxis.max = 100;
            paretoValueAxis.strictMinMax = true;
            paretoValueAxis.renderer.grid.template.disabled = true;
            paretoValueAxis.numberFormatter = new am4core.NumberFormatter();
            paretoValueAxis.numberFormatter.numberFormat = "#'%'"
            paretoValueAxis.cursorTooltipEnabled = false;

            // var paretoSeries = chart.series.push(new am4charts.LineSeries())
            // paretoSeries.dataFields.valueY = "pareto";
            // paretoSeries.dataFields.categoryX = "month";
            // paretoSeries.yAxis = paretoValueAxis;
            // paretoSeries.tooltipText = "pareto: {valueY.formatNumber('#.0')}%[/]";
            // paretoSeries.bullets.push(new am4charts.CircleBullet());
            // paretoSeries.strokeWidth = 2;
            // paretoSeries.stroke = new am4core.InterfaceColorSet().getFor("alternativeBackground");
            // paretoSeries.strokeOpacity = 0.5;

            // Cursor
            chart.cursor = new am4charts.XYCursor();
            chart.cursor.behavior = "panX";
        });
    }
</script>