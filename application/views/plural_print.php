<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>出貨單列印</title>
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jQuery.print-master/dist/jQuery.print.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <style>
        * {
            font-family: 微軟正黑體 !important;
        }

        table,tr,th,td {
            font-size: 25px;
            /* border: 3px solid black !important; */
            border: 3px solid transparent !important;
        }

        .hidden-1{
            visibility:hidden;
        }
        /* .hidden-2{
            visibility:hidden;
        } */
    </style>
</head>

<body>
    <?php foreach ($product as $order){ ?>
        <div class="container" id="ele1" style="page-break-after:always">
            <div class="row text-center">
                <div class="col">
                </div>
                <div class="col">
                    <h1 class="hidden-1">
                        <b>七股文蛤工社</b>
                    </h1>
                </div>
                <div class="col">
                </div>
            </div>
            <div class="row">
                <div class="col text-left">
                    <h4>
                        <strong class="hidden-1">店址：台南市北區長榮路五段426號</strong>
                    </h4>
                </div>
                <div class="col text-right">
                    <h4><strong class="hidden-1">聯絡電話：(06)2811-892</strong></h4>
                </div>
            </div>
            <div class="row">
                <div class="col">
                </div>
                <div class="col text-center">
                    <h1>
                        <strong class="hidden-1">出貨單</strong>
                    </h1>
                </div>
                <div class="col" style="align-items: center;justify-content: right; display: flex;">
                    <h4>
                        <strong class="hidden-1">NO.</strong>
                        <strong class="hidden-2">&nbsp;<?php printf("%s%d", date("Ymd"), $order[0]['order_key'])?></strong>
                    </h4>
                </div>
            </div>
            <div class="row">
                <div class="col text-left">
                    <h4 style="margin-botton: 2px;">
                        <strong class="hidden-1">廠商：</strong>
                        <strong class="hidden-2"><?php echo $order[0]['user_name']?></strong>
                    </h4>
                </div>
                <div class="col text-right">
                    <h4>
                        <strong class="hidden-1">出貨日期：</strong>
                        <strong class="hidden-2">
                            <?php
                                $shipped_time = $order[0]['shipped_time']??"";
                                if(!empty($shipped_time)){
                                    $shipped_time = explode(" ",$order[0]['shipped_time'])[0];
                                }
                                echo $shipped_time;
                            ?>
                        </strong>
                    </h4>
                </div>
            </div>
            <div class="row text-center">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col" width="25%" class="hidden-1">尺寸</th>
                            <th scope="col" width="25%" class="hidden-1">數量</th>
                            <th scope="col" width="25%" class="hidden-1">單價</th>
                            <th scope="col" width="25%" class="hidden-1">金額</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $total = 0;
                            for ($i = 0; $i < 5; $i++) {
                                if($i<count($order)){
                                    echo '<tr>
                                            <td>'.$product[$i][0]["unit"].'</td>
                                            <td>'.$product[$i][0]["amount"].'</td>
                                            <td>'.($product[$i][0]["price"]/$product[$i][0]["amount"]).'</td>
                                            <td>'.$product[$i][0]["price"].'</td>
                                        </tr>';
                                    // echo '<tr>
                                    // <td><br></td>
                                    // <td></td>
                                    // <td></td>
                                    // <td></td>
                                    // </tr>';
                                    $total += ($product[$i][0]["price"]*$product[$i][0]["amount"]);
                                } else {
                                    echo '<tr>
                                        <td><br></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        </tr>';
                                }
                            }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col text-left">
                    <h4 class="hidden-1">
                        <strong>經手人：________________</strong>
                    </h4>
                </div>
                <div class="col text-left">
                    <h4 style="margin-top: 5px;">
                        <strong class="hidden-1">合計：</strong>
                        <strong class="hidden-2"><?php echo $total?></strong>
                    </h4>
                </div>
            </div>
        </div>
    <?php } ?>
    <!-- <div id="controller">
        <hr class="border-dash" style="border-top:1px dashed #666;">
        <div class="text-center">
            <button id="printing" class="btn btn-primary" style="background-color: #217dbe;border-color: #217dbe;">列印</button>
        </div>
    </div> -->
    <script type="text/javascript">
        let pathname = location.pathname;
        let controllerName = new Array();
        url = pathname.split("/");
        const key = url['4']

        function report_print(href) {
            window.open(href, "_blank", "width=1000, height=" + window.innerHeight + ", left=" + window.innerWidth / 4);
        }
        $("body").on("click", "#printing", function() {
            window.print();
        });
    </script>
</body>

</html>