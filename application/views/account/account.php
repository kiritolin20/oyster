<section class="chart_section" style="min-height:76.6vh">
    <div class="row">

        <div class="col-lg-12 mb-5">
            <div class="card card-shadow">
                <div class="card-header">
                    <div class="card-title">
                        <h2><strong>全部賣家帳號</strong></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="card-body">
                            <table id="adminDataTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th style="width:30%">key</th>
                                        <th style="width:40%">帳號</th>
                                        <th style="width:40%">信箱</th>
                                        <th style="width:40%">操作</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                </tbody>
                            </table>
                            <!-- Large modal -->
                            <!-- <button type="button" class="btn btn-dark" data-toggle="modal" data-target=".bd-example-modal-lg">
                                Large modal
                            </button> -->
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

</section>


<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modifyModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myLargeModalLabel">修改賣家資料</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="modifyForm" method="post">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>帳號</label>
                            <input name="admin_account" class="form-control" placeholder="Account">
                        </div>
                        <div class="form-group  col-md-6">
                            <label>信箱</label>
                            <input name="admin_email" type="email" class="form-control" placeholder="Email">
                        </div>
                        <div class="form-group  col-md-12">
                            <label>密碼</label>
                            <input name="admin_password" type="password" class="form-control" placeholder="Password">
                        </div>
                        <div class="form-group  col-md-12" style="display:none">
                            <label>key</label>
                            <input name="admin_key" class="form-control" placeholder="key">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    關閉
                </button>
                <button type="submit" class="btn btn-primary">
                    修改
                </button>
            </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    var datatable = {
        get: function() {
            getDataTable('admin', 3);
        }
    }
</script>