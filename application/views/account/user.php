<section class="chart_section" style="min-height:76.6vh">
    <div class="row">

        <div class="col-lg-12 mb-5">
            <div class="card card-shadow">
                <div class="card-header">
                    <div class="card-title">
                        <div class="row">
                            <div class="col-md-4">
                                <h2 style="padding-top:7px"><strong>買家帳號管理</strong></h2>
                            </div>

                            <div class="col-md-6"></div>
                            <div class="col-md-2" style="text-align: right;">
                                <button type="button" class="btn btn-outline-info" onclick="showInsertModal(1)">
                                    新增買家
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="card-body">
                            <table id="userDataTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="1%"></th>
                                        <th>序號</th>
                                        <th>區域</th>
                                        <th>名稱</th>
                                        <th>運送地址</th>
                                        <th>付款方式</th>
                                        <th>店家電話</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                </tbody>
                            </table>
                            <!-- Large modal -->
                            <!-- <button type="button" class="btn btn-dark" data-toggle="modal" data-target=".bd-example-modal-lg">
                                Large modal
                            </button> -->
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

</section>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="insertModalLabel" aria-hidden="true" id="insertModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="insertModalLabel">新增買家</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="insertForm" method="post">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>店家名稱</label>
                            <input name="user_name" class="form-control" placeholder="店家名稱">
                        </div>
                        <div class="form-group  col-md-6">
                            <label>店家地址</label>
                            <input name="user_address" type="text" class="form-control" placeholder="店家地址">
                        </div>
                        <div class="form-group  col-md-6">
                            <label>店家電話</label>
                            <input name="user_shop_phone" type="text" class="form-control" placeholder="店家電話">
                        </div>
                        <div class="form-group  col-md-3">
                            <label>買家區域</label>
                            <select name="user_area" class="form-control">
                                <option value="e">東</option>
                                <option value="s">南</option>
                                <option value="w">西</option>
                                <option value="n">北</option>
                            </select>
                        </div>
                        <div class="form-group  col-md-3">
                            <label>買家付款方式</label>
                            <select name="user_priceType" class="form-control">
                                <option value="month">月結</option>
                                <option value="week">週結</option>
                                <option value="cash">現金結</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>負責人姓名</label>
                            <input name="user_member" class="form-control" placeholder="負責人姓名">
                        </div>
                        <div class="form-group  col-md-6">
                            <label>負責人電話</label>
                            <input name="user_phone" type="text" class="form-control" placeholder="負責人電話">
                        </div>
                        <div class="form-group  col-md-6">
                            <label>送貨地址</label>
                            <input name="user_shop_address" type="text" class="form-control" placeholder="送貨地址">
                        </div>
                        <div class="form-group col-md-3">
                            <label class="control control-solid control-solid-primary control--checkbox" style="margin-bottom: 0px;margin-top: 2.5em;">
                                同店家地址
                                <input type="checkbox" name="check_address">
                                <div class="control__indicator"></div>
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            關閉
                        </button>
                        <button type="submit" class="btn btn-primary">
                            新增
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="modifyModalLabel" aria-hidden="true" id="modifyModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modifyModalLabel">修改商品資料</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="modifyForm" method="post">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>店家名稱</label>
                            <input name="user_name" class="form-control" placeholder="店家名稱">
                        </div>
                        <div class="form-group  col-md-6">
                            <label>店家地址</label>
                            <input name="user_address" type="text" class="form-control" placeholder="店家地址">
                        </div>
                        <div class="form-group  col-md-6">
                            <label>店家電話</label>
                            <input name="user_shop_phone" type="text" class="form-control" placeholder="店家電話">
                        </div>
                        <div class="form-group  col-md-3">
                            <label>買家區域</label>
                            <select name="user_area" class="form-control">
                                <option value="e">東</option>
                                <option value="s">南</option>
                                <option value="w">西</option>
                                <option value="n">北</option>
                            </select>
                        </div>
                        <div class="form-group  col-md-3">
                            <label>買家付款方式</label>
                            <select name="user_priceType" class="form-control">
                                <option value="month">月結</option>
                                <option value="week">週結</option>
                                <option value="cash">現金結</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>負責人姓名</label>
                            <input name="user_member" class="form-control" placeholder="負責人姓名">
                        </div>
                        <div class="form-group  col-md-6">
                            <label>負責人電話</label>
                            <input name="user_phone" type="text" class="form-control" placeholder="負責人電話">
                        </div>
                        <div class="form-group  col-md-6">
                            <label>送貨地址</label>
                            <input name="user_shop_address" type="text" class="form-control" placeholder="送貨地址">
                        </div>
                        <div class="form-group col-md-3">
                            <label class="control control-solid control-solid-primary control--checkbox" style="margin-bottom: 0px;margin-top: 2.5em;">
                                同店家地址
                                <input type="checkbox" name="check_address">
                                <div class="control__indicator"></div>
                            </label>
                        </div>
                        <div class="form-group col-md-12" style="display:none">
                            <label>Key</label>
                            <input name="user_key" type="text" class="form-control">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    關閉
                </button>
                <button type="submit" class="btn btn-primary">
                    修改
                </button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="modifyModalLabel" aria-hidden="true" id="favoriteModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modifyModalLabel">選擇買家最愛商品</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="favoriteForm" method="post">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12">
                            <div class="card-body">
                                <table id="favoriteDataTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>選取</th>
                                            <th>圖片</th>
                                            <th>名稱</th>
                                            <th>單位</th>
                                            <th>價錢</th>
                                            <th>介紹</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody">
                                    </tbody>
                                </table>
                                <!-- Large modal -->
                                <!-- <button type="button" class="btn btn-dark" data-toggle="modal" data-target=".bd-example-modal-lg">
                                Large modal
                            </button> -->
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    關閉
                </button>
                <button type="submit" class="btn btn-primary">
                    修改
                </button>
            </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    var datatable = {
        get: function() {
            getChildRowsDataTable('user');

            // getDataTable('user');
        }
    }



    basic.pushReady(function() {
        $("#insertForm input[name = 'check_address']").change(function() {
            if (this.checked) {
                $("#insertForm input[name = 'user_shop_address']").val($("#insertForm input[name = 'user_address']").val());
            } else {
                $("#insertForm input[name = 'user_shop_address']").val('');
            }
        });
        $("#modifyForm input[name = 'check_address']").change(function() {
            if (this.checked) {
                $("#modifyForm input[name = 'user_shop_address']").val($("#modifyForm input[name = 'user_address']").val());
            } else {
                $("#modifyForm input[name = 'user_shop_address']").val('');
            }
        });
    });

    var user_key;

    let editFavorite = key => {

        user_key = key;

        getfavoriteDataTable('favorite', [{
                "targets": [0, 1],
                "orderable": false,
            },
            {
                width: '10%',
                targets: 0
            },
            {
                width: '20%',
                targets: 1
            },
            {
                width: '15%',
                targets: 2
            },
            {
                width: '15%',
                targets: 3
            },
            {
                width: '20%',
                targets: 4
            },
            {
                width: '20%',
                targets: 5
            }
        ]);



    }

    let getfavoriteDataTable = ($datatableName, col) => {
        $('#' + $datatableName + 'DataTable').DataTable({
            "language": {
                "lengthMenu": "顯示 _MENU_ 筆消息",
                "emptyTable": "沒有資料",
                "info": "目前顯示 _START_ 至 _END_ 筆的資料，共 _TOTAL_ 筆資料",
                "infoFiltered": "，從 _MAX_ 筆資料中過濾",
                "infoEmpty": "沒有資料能夠顯示",
                "zeroRecords": "沒有資料，可以鍵入其他內容進行搜索",
                "search": "搜尋資料：",
                "paginate": {
                    "next": "下一頁",
                    "previous": "上一頁"
                },
            },
            destroy: true,
            autoWidth: false,
            searching: true,
            autoWidth: false,
            columnDefs: col,
            "initComplete": () => {
                data = {
                    user_key
                }

                ajaxPost(base_url(controllerName['2'] + '/getFavorite'), data)
                    .done(function(e) {
                        for (const product_key of e.product_key) {
                            $("#favoriteDataTable input[value=" + product_key.product_key + "]").prop('checked', 'true');
                        }
                        $('#favoriteModal').modal('show');
                    });
            },
            "order": [],
            "ajax": {
                url: base_url(controllerName['2'] + '/getfavoriteDataTable'),
                type: 'POST'
            }
        })
    }

    /**
     * 送出新增資料表單，並呼叫insertProduct function
     */
    $("form[id='favoriteForm']").submit(function(e) {
        e.preventDefault();
        modifyFavorite();
    });

    /**
     * 更改資料
     * @param  Object $formDom
     * @return bool
     */
    let modifyFavorite = () => {
        swal("確定要修改嗎?", {
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function(value) {
            if (value) {
                let checkCount = $('#favoriteDataTable [class="custom-control-input"]:checked').length;
                if (checkCount == 0) {
                    swal("失敗", "請先選取商品", "error");
                } else {
                    let keyArr = new Array();
                    for (let i = 0; i < checkCount; i++) {
                        keyArr.push($('#favoriteDataTable [class="custom-control-input"]:checked')[i].value);
                    }
                    let keyObj = {
                        "user_key": user_key,
                        "product_key": keyArr
                    }
                    ajaxPost(base_url(controllerName['2'] + '/modifyFavorite'), keyObj)
                        .done(function(e) {
                            swal("成功", "資料修改成功", "success");
                            $('#favoriteModal').modal('hide');

                        });
                }
            };
        });
    }
</script>