<section class="chart_section" style="min-height:76.6vh">
    <div class="row">

        <div class="col-lg-12 mb-5">
            <div class="card card-shadow">
                <div class="card-header">
                    <div class="card-title">
                        <div class="row">
                            <div class="col-md-2">
                                <h2 style="padding-top:7px"><strong>新增商品</strong></h2>
                            </div>

                            <div class="col-md-10"></div>
                            <!-- <div class="col-md-1">
                                <button type="button" class="btn btn-outline-info" onclick="showInsertModal(1)">
                                    商品上架
                                </button>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="card-body">
                            <form id="insertForm" method="post">
                                <div class="row">
                                    <div class="form-group  col-md-12">
                                        <label style="font-size:16px">商品展示圖片</label>
                                        <div id="insertfile"></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>商品名稱</label>
                                        <input name="product_name" class="form-control" placeholder="Name">
                                    </div>
                                    <div class="form-group  col-md-3">
                                        <label>商品價錢</label>
                                        <input name="product_price" type="text" class="form-control" placeholder="Price">
                                    </div>
                                    <div class="form-group  col-md-3">
                                        <label>商品單位</label>
                                        <input name="product_unit" type="text" class="form-control" placeholder="Price">
                                    </div>
                                    <div class="form-group  col-md-3">
                                        <label>商品數量</label>
                                        <input name="product_amount" type="number" class="form-control" placeholder="Amount">
                                    </div>
                                    <div class="form-group  col-md-3">
                                        <label>商品是否顯示</label>
                                        <select class="form-control" name="product_display">
                                            <option value="1">是</option>
                                            <option value="0">否</option>
                                        </select>
                                    </div>
                                    <div class="form-group  col-md-12">
                                        <label>商品介紹</label>
                                        <textarea name="product_introduct" class="form-control" cols="60" rows="5" style="margin-top: 0px; margin-bottom: 0px; height: 115px;"></textarea>
                                    </div>
                                    <div class="form-group  col-md-12" style="display:none">
                                        <label>key</label>
                                        <input name="product_key" class="form-control" placeholder="key">
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                關閉
                            </button>
                            <button type="submit" class="btn btn-primary">
                                新增
                            </button>
                        </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>
    </div>

</section>



<script type="text/javascript">
    var datatable = {
        get: function() {
            getDataTable('order', 7);
        }
    }
    basic.pushReady(function() {
        $("#insertfile").html("<input class='file input-lg' type='file' id='insertfileBox' name='setFile[]' multiple>");
        $("#insertfileBox").fileinput({
            language: "zh-TW",
            uploadAsync: true,
            uploadUrl: base_url(controllerName['2'] + '/fileInput'),
            showZoom: true,
            showClose: false,
            showUpload: false,
            showRemove: false,
            showPreview: true,
            showBrowse: true,
            browseOnZoneClick: true,
            allowedFileExtensions: ['jpg', 'gif', 'png', 'jpeg'],
            fileActionSettings: {
                showUpload: false,
                showDrag: true
            },
            // initialPreviewDownloadUrl: "{filename}",
            maxFileCount: '1',
            removeClass: "btn btn-warning",
            maxFileSize: 20480,
            dropZoneEnabled: true,
            initialPreviewAsData: true,
            browseOnZoneClick: true,
            // previewSettings: {
            //     image: {
            //         width: "160px",
            //         height: "120px"
            //     },
            // },
            allowedFileExtensions: ['jpg', 'gif', 'png', 'jpeg'],
            overwriteInitial: false
        });
    });
</script>