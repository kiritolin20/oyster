<?php
defined('BASEPATH') or exit('No direct script access allowed');
class History_view extends ViewLoad
{

    private $viewData;

    public function __construct($transfer = array())
    {
        parent::__construct();
        //存放由Controller傳遞過來的資訊

        //他的流程是home(Controller)->Infrastructure(底層的建立物件)->建立物件後把資料傳過來這裡
        $this->viewData = $transfer;
    }

    public function index()
    {
        //設定組件,需要載入的資料夾，然後再放要載入的檔案
        $components = $this->loadComponent('order', [
            "history"
        ]);
        //載入模板->並開始渲染畫面
        $this->loadTemplate('systemTemplate', [
            //註冊組件
            "components" => $components,
            //設定頁面CSS
            "bodyClass" => "",
            //設定頁面內容
            "pageTitle" => "歷史訂單查詢",
            "upperTitle" => "訂單",
            "mainTitle" => "歷史訂單查詢",

            //使用傳遞資訊(需要對頁面傳送什麼資料都是在這裡設定，例如台電把View內容變成data["XXX"]給前端呼叫< echo $XXX >)
            "account" => $this->viewData['account'],
            "adminType" => $this->viewData['adminType']
        ]);
    }
}
