<div class="login-content">
    <div class="login-form">
        <form id="loginForm" method="post">
            <div class="form-group">
                <label>帳號</label>
                <input name="account" class="form-control" placeholder="Account">
            </div>
            <div class="form-group">
                <label>密碼</label>
                <input name="password" type="password" class="form-control" placeholder="Password">
            </div>
            <div class="g-recaptcha" data-sitekey="6LfW0OEUAAAAAA0DCwxXrww555cCUJF1-BVeLxZG"></div>
            <div class="checkbox">
                <!-- <label>
                    <input type="checkbox"> Remember Me
                </label> -->
            </div>
            <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30" style="margin-top:20px">登入</button>
            <!-- <div class="register-link m-t-15 text-center">
                <p>還沒註冊商家帳號嗎?
                    <a href="<?php echo base_url("Register") ?>">點這裡註冊</a>
                </p>
            </div> -->
        </form>
    </div>
</div>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script type="text/javascript">
    $("form").submit(function(e) {
        e.preventDefault();
        login.submit();
    });

    var login = {
        formDom: $("#loginForm"),
        submit: function() {
            var data = $("#loginForm").getFormObject();
            if (this.checkForm(data)) return;
            ajaxPost(base_url('Login/checkLogin'), data)
                .done(function(e) {
                    if (e.status == 1) {
                        location.href = base_url('Home');
                    } else if (e.status == 2) {
                        swal("失敗", "機器人驗證失敗", "error");
                        return true;
                    } else {
                        swal("失敗", "帳號或密碼錯誤", "error");
                        return true;
                    }
                });
        },
        checkForm: function(formData) {
            if (formData.account == "") {
                swal("錯誤", "帳號不可為空", "error");
                return true;
            } else if (formData.password == "") {
                swal("錯誤", "密碼不可為空", "error");
                return true;
            } else if (formData['g-recaptcha-response'] == " ") {
                swal("錯誤", "請勾選我不是機器人", "error");
                return true;
            } else {
                return false;
            }
        }
    }
</script>