<div class="sufee-login d-flex align-content-center flex-wrap">
    <div class="container">
        <div class="login-content">
            <div class="login-form">
                <form id="registerForm" method="post">
                    <div class="form-group">
                        <label>帳號</label>
                        <input name="account" class="form-control" placeholder="Account">
                    </div>
                    <div class="form-group">
                        <label>信箱</label>
                        <input name="email" type="email" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label>密碼</label>
                        <input name="password" type="password" class="form-control" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label>商家名稱</label>
                        <input name="shop_name" class="form-control" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label>商家地址</label>
                        <input name="shop_address" class="form-control" placeholder="Address">
                    </div>
                    <div class="form-group">
                        <label>商家介紹</label>
                        <textarea name="shop_introduct" class="form-control" placeholder="Introduct" cols="60" rows="5" style="margin-top: 0px; margin-bottom: 0px; height: 115px;"></textarea>
                    </div>
                    <div class="form-group">
                        <label>轉帳銀行帳戶</label>
                        <input name="admin_bank" class="form-control" placeholder="Introduct">
                    </div>
                    <div class="form-group">
                        <label>商家連絡電話</label>
                        <input name="shop_phone" class="form-control" placeholder="Phone">
                    </div>
                    <!-- <div class="checkbox">
                        <label>
                            <input type="checkbox"> Agree the terms and policy
                        </label>
                    </div> -->
                    <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">註冊</button>
                    <!-- <div class="social-login-content">
                        <div class="social-button">
                            <button type="button" class="btn social facebook btn-flat btn-addon mb-3">
                                <i class="fa fa-facebook"></i>Register with facebook</button>
                            <button type="button" class="btn social twitter btn-flat btn-addon mt-2">
                                <i class="fa fa-twitter"></i>Register with twitter</button>
                        </div>
                    </div> -->
                    <div class="register-link m-t-15 text-center">
                        <p>已經有帳號了嗎?
                            <a href="<?php echo base_url("Login") ?>">點這裡登入</a>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("form").submit(function(e) {
        e.preventDefault();
        register.submit();
    });

    var register = {
        formDom: $("#registerForm"),
        submit: function() {
            var data = this.formDom.getFormObject();
            const key = location.pathname.split('/');
            if (key[4] != undefined) {
                data["lineKey"] = key[4];
            } else {}
            if (this.checkForm(data)) return;
            ajaxPost(base_url('register/checkRegister'), data)
                .done(function(e) {
                    if (e.status == 2) {
                        swal("成功", "註冊成功，請重新登入", "success").then((result) => {
                            if (result) {
                                location.href = base_url('Login');
                            } else {
                                location.href = base_url('Login');
                            }
                        });
                    } else if (e.status == 1) {
                        swal("失敗", "帳號或信箱重複註冊，請更換一組帳號", "error");
                        return true;
                    } else {
                        swal("失敗", "註冊失敗，請重新註冊", "error");
                        return true;
                    }
                });
        },
        checkForm: function(formData) {
            if (formData.account == "") {
                swal("錯誤", "帳號不可為空", "error");
                return true;
            } else if (formData.password == "") {
                swal("錯誤", "密碼不可為空", "error");
                return true;
            } else {
                return false;
            }
        }
    }
</script>