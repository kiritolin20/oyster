<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>出貨單列印</title>
    <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print.css"> -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jQuery.print-master/dist/jQuery.print.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <style>
        * {
            font-family: 微軟正黑體 !important;
        }

        table,tr,th,td {
            font-size: 25px;
            border: 3px solid transparent !important;
            /* border: 3px solid !important; */
        }

        .hidden-1{
            visibility:hidden;
        }
        /* .hidden-2{
            visibility:hidden;
        } */
    </style>
</head>

<body>
    <div class="container" id="ele1">
        <div class="row text-center">
            <div class="col">
            </div>
            <div class="col">
                <h1 class="hidden-1">
                    <b>七股文蛤工社</b>
                </h1>
            </div>
            <div class="col">
            </div>
        </div>
        <div class="row">
            <div class="col text-left">
                <h4>
                    <strong class="hidden-1">店址：台南市北區長榮路五段426號</strong>
                </h4>
            </div>
            <div class="col text-right">
                <h4><strong class="hidden-1">聯絡電話：(06)2811-892</strong></h4>
            </div>
        </div>
        <div class="row">
            <div class="col">
            </div>
            <div class="col text-center">
                <h1>
                    <strong class="hidden-1">出貨單</strong>
                </h1>
            </div>
            <div class="col" style="align-items: center;justify-content: right; display: flex;">
                <h4>
                    <strong class="hidden-1">NO.</strong>
                    <strong class="hidden-2">&nbsp;<?php printf("%s%d", date("Ymd"), $order[0]['order_key'])?></strong>
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="col text-left">
                <h4>
                    <strong class="hidden-1">廠商：</strong>
                    <strong class="hidden-2"><?php echo $order[0]['user_name']?></strong>
                </h4>
            </div>
            <div class="col text-right">
                <h4>
                    <strong class="hidden-1">出貨日期：</strong>
                    <strong class="hidden-2">
                        <?php
                            $shipped_time = $order[0]['shipped_time']??"";
                            if(!empty($shipped_time)){
                                $shipped_time = explode(" ",$order[0]['shipped_time'])[0];
                            }
                            echo $shipped_time;
                        ?>
                    </strong>
                </h4>
            </div>
        </div>
        <div class="row text-center">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col" width="25%" class="hidden-1">尺寸</th>
                        <th scope="col" width="25%" class="hidden-1">數量</th>
                        <th scope="col" width="25%" class="hidden-1">單價</th>
                        <th scope="col" width="25%" class="hidden-1">金額</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        for ($i = 0; $i < 5; $i++) {
                            if($i<count($product)){
                                echo '<tr>
                                      <td>'.$product[$i]["unit"].'</td>
                                      <td>'.$product[$i]["amount"].'</td>
                                      <td>'.($product[$i]["price"]/$product[$i]["amount"]).'</td>
                                      <td>'.$product[$i]["price"].'</td>
                                      </tr>';
                                // echo '<tr>
                                // <td><br></td>
                                // <td></td>
                                // <td></td>
                                // <td></td>
                                // </tr>';
                            } else {
                                echo '<tr>
                                      <td><br></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      </tr>';
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col text-left">
                <h4 class="hidden-1">
                    <strong>經手人：________________</strong>
                </h4>
            </div>
            <div class="col text-left">
                <h4>
                    <strong class="hidden-1">合計：</strong>
                    <strong class="hidden-2"><?php echo $order[0]['price']?></strong>
                </h4>
            </div>
        </div>
    </div>
    <!-- <div id="controller" class="no-print">
        <hr class="border-dash" style="border-top:1px dashed #666;">
        <div class="text-center">
            <button id="printing" class="btn btn-primary" style="background-color: #217dbe;border-color: #217dbe;"
                onclick="print()">列印</button>
        </div>
    </div> -->
    <script type="text/javascript">
        // function print() {
        //     $("#ele1").print({
        //         globalStyles: true, //是否包含父文檔的樣式，默認爲true
        //         mediaPrint: false, //是否包含media='print'的鏈接標籤。會被globalStyles選項覆蓋，默認爲false
        //         stylesheet: null, //外部樣式表的URL地址，默認爲null
        //         noPrintSelector: ".no-print", //不想打印的元素的jQuery選擇器，默認爲".no-print"
        //         iframe: true, //是否使用一個iframe來替代打印表單的彈出窗口，true爲在本頁面進行打印，false就是說新開一個頁面打印，默認爲true
        //         append: null, //將內容添加到打印內容的後面
        //         prepend: null, //將內容添加到打印內容的前面，可以用來作爲要打印內容
        //         deferred: $.Deferred() //回調函數
        //     });
        // }

        let pathname = location.pathname;
        let controllerName = new Array();
        url = pathname.split("/");
        const key = url['4']

        function report_print(href) {
            window.open(href, "_blank", "width=1000, height=" + window.innerHeight + ", left=" + window.innerWidth / 4);
        }
        // $("body").on("click", "#printing", function() {
        //     window.print();
        // });
    </script>
</body>

</html>