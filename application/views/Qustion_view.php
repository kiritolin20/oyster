<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Qustion_view extends ViewLoad
{

    private $viewData;

    public function __construct($transfer = array())
    {
        parent::__construct();
        //存放由Controller傳遞過來的資訊

        //他的流程是home(Controller)->Infrastructure(底層的建立物件)->建立物件後把資料傳過來這裡
        $this->viewData = $transfer;
    }

    public function index()
    {
        //設定組件,需要載入的資料夾，然後再放要載入的檔案
        $components = $this->loadComponent('qustion', [
            "qustion"
        ]);
        //載入模板->並開始渲染畫面
        $this->loadTemplate('systemTemplate', [
            //註冊組件
            "components" => $components,
            //設定頁面CSS
            "bodyClass" => "",
            //設定頁面內容
            "pageTitle" => "問卷",
            "upperTitle" => "問卷",
            "mainTitle" => "問卷設定",

            //使用傳遞資訊(需要對頁面傳送什麼資料都是在這裡設定，例如台電把View內容變成data["XXX"]給前端呼叫< echo $XXX >)
            "account" => $this->viewData['account'],
            "adminType" => $this->viewData['adminType']
        ]);
    }

    public function adminSet()
    {
        //設定組件,需要載入的資料夾，然後再放要載入的檔案
        $components = $this->loadComponent('qustion', [
            "adminSet"
        ]);
        //載入模板->並開始渲染畫面
        $this->loadTemplate('systemTemplate', [
            //註冊組件
            "components" => $components,
            //設定頁面CSS
            "bodyClass" => "",
            //設定頁面內容
            "pageTitle" => "管理員問卷設定",
            "upperTitle" => "問卷",
            "mainTitle" => "管理員問卷設定",

            //使用傳遞資訊(需要對頁面傳送什麼資料都是在這裡設定，例如台電把View內容變成data["XXX"]給前端呼叫< echo $XXX >)
            "account" => $this->viewData['account'],
            "adminType" => $this->viewData['adminType']
        ]);
    }

    public function sellerSet()
    {
        //設定組件,需要載入的資料夾，然後再放要載入的檔案
        $components = $this->loadComponent('qustion', [
            "sellerSet"
        ]);
        //載入模板->並開始渲染畫面
        $this->loadTemplate('systemTemplate', [
            //註冊組件
            "components" => $components,
            //設定頁面CSS
            "bodyClass" => "",
            //設定頁面內容
            "pageTitle" => "賣家問卷設定",
            "upperTitle" => "問卷",
            "mainTitle" => "賣家問卷設定",

            //使用傳遞資訊(需要對頁面傳送什麼資料都是在這裡設定，例如台電把View內容變成data["XXX"]給前端呼叫< echo $XXX >)
            "account" => $this->viewData['account'],
            "adminType" => $this->viewData['adminType']
        ]);
    }
    public function reply()
    {
        //設定組件,需要載入的資料夾，然後再放要載入的檔案
        $components = $this->loadComponent('qustion', [
            "reply"
        ]);
        //載入模板->並開始渲染畫面
        $this->loadTemplate('systemTemplate', [
            //註冊組件
            "components" => $components,
            //設定頁面CSS
            "bodyClass" => "",
            //設定頁面內容
            "pageTitle" => "問卷回答",
            "upperTitle" => "問卷",
            "mainTitle" => "問卷回答",

            //使用傳遞資訊(需要對頁面傳送什麼資料都是在這裡設定，例如台電把View內容變成data["XXX"]給前端呼叫< echo $XXX >)
            "account" => $this->viewData['account'],
            "adminType" => $this->viewData['adminType']
        ]);
    }

}
