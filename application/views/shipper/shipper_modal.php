<section class="chart_section" style="min-height:76.6vh">
    <div class="row">

        <div class="col-lg-12 mb-5">
            <div class="card card-shadow">
                <div class="card-header">
                    <div class="card-title">
                        <div class="row">
                            <div class="col-md-10">
                                <h2 style="padding-top:7px"><strong>出貨單</strong></h2>
                            </div>
                            <div class="col-md-2">
                            <a href='javascript:void(0);' class='btn btn-info btn-xs' title='列印報表' onclick='report_print(base_url()+"Shipper/print")'><i class='fa fa-print' aria-hidden='true'></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<div class="modal fade bd-example-modal-lg" id="selectActivityQuestion" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLongTitle">出貨單</h3>
            </div>
            <div class="modal-body">
                <div id="content">
                    <!--整張表格一列有18格-->
                    <table style="border: 0px;">
                        <tr class="table-bd-bottom">
                            <td style="width: 25%;"></td>
                            <td class="table-title text-center" style="width: 40%;">
                                <div style="margin-bottom: 5px;margin-top: 5px;">
                                    =======
                                </div>
                            </td>
                            <td class="text-left" style="width: 10%;">
                                NO.<?php //echo $region_name; ?><br>
                            </td>
                            <td colspan="2" class="higtlight" style="width: 25%;">
                            </td>
                        </tr>
                    </table>

                    <table class="table-bd-bottom">
                        <tr>
                            <td colspan="4" class="text-left higtlight"></td>
                            <td colspan="2">台照</td>
                            <td colspan="2" class="higtlight"><?php //echo $accidentStratDate; ?></td>
                            <td colspan="2">年</td>
                            <td colspan="2" class="higtlight"><?php //echo $accidentStratDate; ?></td>
                            <td colspan="2">月</td>
                            <td colspan="2" class="higtlight"><?php //echo $accidentStratDate; ?></td>
                            <td colspan="2">日</td>
                        </tr>
                    </table>
                    <table class="table-bd-bottom">
                        <tr class="table-bd-lr">
                            <td colspan="6">品名</td>
                            <td colspan="4" class="text-left">數量</td>
                            <td colspan="2" class="text-left">單價</td>
                            <td colspan="2" class="text-left"></td>
                            <td colspan="4" class="text-left">金額</td>
                        </tr>
                    </table>
                    <?php
                        for ($i=0; $i < 10; $i++) { 
                            echo '<table class="table-bd-bottom">
                                    <tr class="table-bd-lr">
                                        <td colspan="6"><br></td>
                                        <td colspan="4" class="text-left"></td>
                                        <td colspan="2" class="text-left"></td>
                                        <td colspan="2" class="text-left"></td>
                                        <td colspan="4" class="text-left"></td>
                                    </tr>
                                </table>';
                        }
                    ?>
                    <table style="border: 0px;margin-top: 20px;">
                        <tr class="table-title" style="border: 0px;">
                            <td>經手人：________________</td>
                            <td>合計：</td>
                        </tr>
                    </table>

                </div>
                <div id="controller">
                    <hr class="border-dash" style="border-top:1px dashed #666;">
                    <div class="text-center">
                        <button id="printing" class="btn btn-primary"
                            style="background-color: #217dbe;border-color: #217dbe;">列印</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="submitQuestion">送出</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function report_print(href){
        window.open(href, "_blank", "width=1000, height="+window.innerHeight+", left="+window.innerWidth/4);
    }
</script>