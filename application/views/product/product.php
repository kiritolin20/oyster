<section class="chart_section" style="min-height:76.6vh">
    <div class="row">

        <div class="col-lg-12 mb-5">
            <div class="card card-shadow">
                <div class="card-header">
                    <div class="card-title">
                        <div class="row">
                            <div class="col-md-4">
                                <h2 style="padding-top:7px"><strong>商品管理</strong></h2>
                            </div>

                            <div class="col-md-6"></div>
                            <div class="col-md-2" style="text-align: right;">
                                <button type="button" class="btn btn-outline-info" onclick="showInsertModal(1)">
                                    商品上架
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="card-body">
                            <table id="productDataTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th style="width:15%">商品圖片</th>
                                        <th>商品名稱</th>
                                        <th>商品介紹</th>
                                        <th>商品價錢</th>
                                        <th>售價單位</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                </tbody>
                            </table>
                            <!-- Large modal -->
                            <!-- <button type="button" class="btn btn-dark" data-toggle="modal" data-target=".bd-example-modal-lg">
                                Large modal
                            </button> -->
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

</section>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="insertModalLabel" aria-hidden="true" id="insertModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="insertModalLabel">新增商品資料</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="insertForm" method="post">
                    <div class="row">
                        <div class="form-group  col-md-12">
                            <label style="font-size:16px">商品展示圖片</label>
                            <div id="insertfile"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>商品名稱</label>
                            <input name="product_name" class="form-control" placeholder="Name">
                        </div>
                        <div class="form-group  col-md-3">
                            <label>商品價錢</label>
                            <input name="ph_price" type="text" class="form-control" placeholder="Price">
                        </div>
                        <div class="form-group  col-md-3">
                            <label>商品單位</label>
                            <input name="product_unit" type="text" class="form-control" placeholder="Price">
                        </div>
                        <div class="form-group  col-md-12">
                            <label>商品介紹</label>
                            <textarea name="product_info" class="form-control" cols="60" rows="5" style="margin-top: 0px; margin-bottom: 0px; height: 115px;"></textarea>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    關閉
                </button>
                <button type="submit" class="btn btn-primary">
                    新增
                </button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="modifyModalLabel" aria-hidden="true" id="modifyModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modifyModalLabel">修改商品資料</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="modifyForm" method="post">
                    <div class="row">
                        <div class="form-group  col-md-12">
                            <label style="font-size:16px">商品展示圖片</label>
                            <div id="fileBox"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>商品名稱</label>
                            <input name="product_name" class="form-control" placeholder="Name">
                        </div>
                        <div class="form-group  col-md-3">
                            <label>商品價錢</label>
                            <input name="ph_price" type="text" class="form-control" placeholder="Price">
                        </div>
                        <div class="form-group  col-md-3">
                            <label>商品單位</label>
                            <input name="product_unit" type="text" class="form-control" placeholder="Price">
                        </div>
                        <div class="form-group  col-md-12">
                            <label>商品介紹</label>
                            <textarea name="product_info" class="form-control" cols="60" rows="5" style="margin-top: 0px; margin-bottom: 0px; height: 115px;"></textarea>
                        </div>
                        <div class="form-group  col-md-12" style="display:none">
                            <label>key</label>
                            <input name="product_key" class="form-control" placeholder="key">
                        </div>

                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    關閉
                </button>
                <button type="submit" class="btn btn-primary">
                    修改
                </button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="imgModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myLargeModalLabel">修改商品圖片</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="modifyImg" method="post">
                    <div class="row">
                        <div class="form-group  col-md-12">
                            <label style="font-size:16px">商品詳細圖片</label>
                            <div id="fileBoxInfo"></div>
                        </div>
                        <div class="form-group  col-md-6" style="display:none">
                            <label>product_key</label>
                            <input name="product_key" class="form-control" placeholder="key">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    關閉
                </button>
                <button type="submit" class="btn btn-primary" onclick="modifyImg()">
                    修改
                </button>
            </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    var datatable = {
        get: function() {
            getDataTable('product', [{
                    "targets": [0,5],
                    "orderable": false,
                },
                {
                    width: '11%',
                    targets: 0
                },
                {
                    width: '13%',
                    targets: 1
                },
                {
                    width: '15%',
                    targets: 2
                },
                {
                    width: '13%',
                    targets: 3
                },
                {
                    width: '13%',
                    targets: 4
                },
                {
                    width: '7%',
                    targets: 5
                }
            ]);
        }
    }



    // basic.pushReady(function() {
    // });
</script>