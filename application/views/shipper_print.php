<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>出貨單列印</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print.css">
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    
    </script>
    <style>
        *{
            font-family:微軟正黑體!important;
        }
        td{
            font-size:26px;
        }
        h1,h2,h3,h4,h5,h6{
            overflow:hidden !important;
        }
        table{
            border: 0px solid #D0D0D0 !important;
        }
        td{
            border: 0px solid #D0D0D0 !important;
        }
        tr{
            border: 0px solid #D0D0D0 !important;
        }
    </style>
</head>

<body>
    <div id="content">
        <!--整張表格一列有18格-->
        <table style="border: 0px;margin-bottom: 15px">
            <tr style="border: 0px;">
                <td style="width: 20%;">

                </td>
                <td class="table-title text-center" style="width: 60%;">
                    <h1 style="visibility:hidden">
                        <b>七股文蛤工社</b>
                    </h1>
                </td>
                <td class="text-left" style="width: 20%;">
                </td>
            </tr>
        </table>
        <table class="table-bd-bottom" style="border: 0px;margin-bottom: 25px">
            <tr  style="border-bottom: 0px;">
                <td colspan="12" class="text-left"><h4 style="visibility:hidden"><strong>店址：台南市北區長榮路五段426號</strong></h4></td>
                <td colspan="12" class="text-right"><h4 style="visibility:hidden"><strong>聯絡電話：(06)2811-892</strong></h4></td>
            </tr>
        </table>
        <table style="border: 0px;margin-bottom: 25px">
            <tr style="border: 0px;">
                <td style="width: 35%;">
                </td>
                <td class="table-title text-center" style="width: 40%;">
                    <h1>
                        <strong style="visibility:hidden">出貨單</strong>
                    </h1>
                </td>
                <td class="text-lefe" style="width: 35%;">
                    <strong style="visibility:hidden">NO.</strong>
                    <strong>&nbsp;<?php printf("%s%d", date("Ymd"), $order[0]['order_key'])?></strong>
                </td>
            </tr>
        </table>

        <table class="table-bd-bottom" style="border: 0px;margin-bottom: 25px">
            <tr  style="border-bottom: 0px;">
                <td colspan="6"><h2><strong style="visibility:hidden">廠商：</strong><strong><?php echo $order[0]['user_name']?></strong></h2></td>
                <td colspan="3"></td>
                <td colspan="7" class="text-lefe"><h2><strong style="visibility:hidden">出貨日期：</strong>
                <strong>
                <?php
                    $shipped_time = $order[0]['shipped_time']??"";
                    if(!empty($shipped_time)){
                        $shipped_time = explode(" ",$order[0]['shipped_time'])[0];
                    }
                    echo $shipped_time;
                ?></strong></h2>
                </td>

            </tr>
        </table>
        <table class="table-bd-bottom" style="border: 3px solid;">
            <tr class="table-bd-lr">
                <td colspan="4" style="text-align: center;font-size:28px"><strong style="visibility:hidden">尺寸</strong></td>
                <td colspan="4" style="text-align: center;font-size:28px"><strong style="visibility:hidden">數量</strong></td>
                <td colspan="4" style="text-align: center;font-size:28px"><strong style="visibility:hidden">單價</strong></td>
                <td colspan="4" style="text-align: center;font-size:28px"><strong style="visibility:hidden">金額</strong></td>
            </tr>
        </table>
        <table class="table-bd-bottom" id="productDetail">

        </table>
        <?php
        for ($i = 0; $i < 5; $i++) {
            if($i<count($product)){
                echo '<table class="table-bd-bottom">
                        <tr class="table-bd-lr">
                            <td colspan="4" style="text-align: center;"><p style="margin: 8px;">'.$product[$i]["unit"].'</p></td>
                            <td colspan="4" style="text-align: center">'.$product[$i]["amount"].'</td>
                            <td colspan="4" style="text-align: center">'.($product[$i]["price"]/$product[$i]["amount"]).'</td>
                            <td colspan="4" style="text-align: center">'.$product[$i]["price"].'</td>
                        </tr>
                    </table>';
            } else {
                echo '<table class="table-bd-bottom">
                        <tr class="table-bd-lr">
                            <td colspan="4" style="text-align: center"><p style="margin: 45px;"></p></td>
                            <td colspan="4" style="text-align: center"></td>
                            <td colspan="4" style="text-align: center"></td>
                            <td colspan="4" style="text-align: center"></td>
                        </tr>
                    </table>';
            }
        }
        ?>
        <table style="border: 0px;margin-top: 0px;">
            <tr class="table-title" style="border: 0px;">
                <td style="visibility:hidden">經手人：________________</td>
                <td class="text-lefe" style="padding-top: 23px;font-size:28px"><strong style="visibility:hidden">合計：</strong><?php echo $order[0]['price']?></td>
            </tr>
        </table>

    </div>

    <div id="controller">
        <hr class="border-dash" style="border-top:1px dashed #666;">
        <div class="text-center">
            <button id="printing" class="btn btn-primary" style="background-color: #217dbe;border-color: #217dbe;">列印</button>
        </div>
    </div>
    <script type="text/javascript">
        let pathname = location.pathname;
        let controllerName = new Array();
        url = pathname.split("/");
        const key = url['4']

        function report_print(href) {
            window.open(href, "_blank", "width=1000, height=" + window.innerHeight + ", left=" + window.innerWidth / 4);
        }
        $("body").on("click", "#printing", function() {
            window.print();
        });
    </script>
</body>

</html>