<section class="chart_section" style="min-height:76.6vh">
    <div class="row">

        <div class="col-lg-7 mb-5">
            <div class="card card-shadow mb-4">
                <div class="card-header">
                    <div class="card-title">
                        個人資料管理
                    </div>
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs mb-4" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show" data-toggle="tab" href="#tab_1">變更密碼</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_2" onclick="getShop()">店家資訊管理</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active show" id="tab_1" role="tabpanel">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12">
                                    <div class="card-body">
                                        <form id="modifyPassword" method="post">
                                            <div class="row">
                                                <div class="form-group  col-md-6">
                                                    <label>目前的密碼</label>
                                                    <input name="admin_password" type="password" class="form-control" placeholder="目前的密碼">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group  col-md-6">
                                                    <label>想要變更的密碼</label>
                                                    <input name="editPassword" type="password" class="form-control" placeholder="想要變更的密碼">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group  col-md-6">
                                                    <label>再次確認密碼</label>
                                                    <input name="reEditPassword" type="password" class="form-control" placeholder="再次確認密碼">
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">
                                        更改
                                    </button>
                                </div>
                                </form>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_2" role="tabpanel">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12">
                                    <div class="card-body">
                                        <form id="modifyShop" method="post">
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label>商家名稱</label>
                                                    <input name="shop_name" class="form-control" placeholder="Name">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>商家地址</label>
                                                    <input name="shop_address" class="form-control" placeholder="Address">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>轉帳銀行帳戶</label>
                                                    <input name="admin_bank" class="form-control" placeholder="Introduct">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>商家連絡電話</label>
                                                    <input name="shop_phone" class="form-control" placeholder="Phone">
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>商家介紹</label>
                                                    <textarea name="shop_introduct" class="form-control" placeholder="Introduct" cols="60" rows="5" style="margin-top: 0px; margin-bottom: 0px; height: 115px;"></textarea>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">
                                            更改
                                        </button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</section>



<script type="text/javascript">
    basic.pushReady(function() {

        $("form[id='modifyPassword']").submit(function(e) {
            e.preventDefault();
            modify.modifyPassword($("#modifyPassword"));
        });

        $("form[id='modifyShop']").submit(function(e) {
            e.preventDefault();
            modify.modifyShop($("#modifyShop"));
        });

    });

    /**
     * 取得要更改的資料進modal
     * @param  String $key,String $datatable,String $controller,String value
     * @return bool
     */
    let getShop = () => {
        ajaxPost(base_url(controllerName['2'] + '/getShop'))
            .done(function(e) {
                console.log(e);
                for (let i = 0; i < Object.keys(e.data).length; i++) {
                    $("#modifyShop > div > div > input[name = '" + Object.keys(e.data)[i] + "']").val(Object.values(e.data)[i]);
                    $("#modifyShop > div > div > textarea[name = '" + Object.keys(e.data)[i] + "']").val(Object.values(e.data)[i]);
                };
            });
    }

    /**
     * 更改帳號資料
     * @param  Object $formDom
     * @return bool
     */
    var modify = {

        modifyPassword: function(formDom) {
            swal("確定要修改嗎?", {
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(function(value) {
                if (value) {
                    var data = formDom.getFormObject();
                    if (data.admin_password == "") {
                        swal("錯誤", "密碼不可為空", "error");
                    } else if (data.editPassword != data.reEditPassword) {
                        swal("錯誤", "請確認想變更的密碼是否輸入相同", "error");
                    } else {
                        ajaxPost(base_url(controllerName['2'] + '/modifyPassword'), data)
                            .done(function(e) {
                                if (e.status == 1) {
                                    swal("修改成功", {
                                        icon: "success",
                                        buttons: true,
                                    }).then(function(value) {
                                        if (value) {
                                            location.reload();
                                        } else {
                                            location.reload();
                                        };
                                    });
                                } else if (e.status == 2) {
                                    swal("失敗", "目前的密碼輸入錯誤，請重新再試", "error");
                                    return true;
                                } else {
                                    swal("失敗", "資料修改失敗，請重新再試", "error");
                                    return true;
                                }
                            });
                    }
                };
            });
        },

        modifyShop: function(formDom) {
            swal("確定要修改嗎?", {
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(function(value) {
                if (value) {
                    var data = formDom.getFormObject();
                    ajaxPost(base_url(controllerName['2'] + '/modifyShop'), data)
                        .done(function(e) {
                            if (e.status == 1) {
                                swal("修改成功", {
                                    icon: "success",
                                    buttons: true,
                                }).then(function(value) {
                                    // if (value) {
                                    //     location.reload();
                                    // } else {
                                    //     location.reload();
                                    // };
                                });
                            } else {
                                swal("失敗", "資料修改失敗，請重新再試", "error");
                                return true;
                            }
                        });
                };
            });
        }
    }
</script>