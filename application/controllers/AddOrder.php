<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AddOrder extends Infrastructure
{

    /**
     * 載入父類別建構方法
     * 預先處理需執行的項目
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model("AddOrder_model", "model", TRUE);
    }

    public function index()
    {
        if (!$this->getLogin()) {
            redirect(base_url("Login"));
        } else {
            // $this->viewInit(array(
            // 	"pageName" => "home",
            // 	"webTitle" => "首頁",
            // 	"bodyClass" => "home-1 home-2",
            // 	"component" => array(
            // 		"header","about","features","step","products","application","blog"
            // 	),
            // ));
            //載入從Controllers要傳入View的資料 ex:session的名子還是id
            $transferData = $this->getTransferData();
            //初始化view->並把Home_View的物件建立起來->建立起來只是在記憶體中並未載入選染0
            $this->initView('AddOrder_view', $transferData);
        }
    }

    public function getUserData(){
        $data = json_decode($_POST["data"], true);
        if ($this->getLogin()) {
            if (!empty($data)) {
                $result = $this->model->getUserDataModel($data);
                echo json_encode($result);
            } else {
                $result = $this->model->getUserDataModel();
                echo json_encode($result);
            }
        } else {

        }
    }

    public function getPrductData(){
        if ($this->getLogin()) {
            $result = $this->model->getPrductDataModel();
            echo json_encode($result);
        } else {

        }
    }

    public function getUserfavorite(){
        $data = json_decode($_POST["data"], true);
        if ($this->getLogin()) {
            $result = $this->model->getUserfavoriteModel($data);
            echo json_encode($result);
        } else {

        }
    }

    public function setOrder(){
        $data = json_decode($_POST["data"], true);
        if ($this->getLogin()) {
            $result = $this->model->setOrderModel($data);
            echo json_encode($result);
        } else {

        }
    }
}
