<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Infrastructure {

	/**
	 * 載入父類別建構方法
	 * 預先處理需執行的項目
	 */
	public function __construct() {
		parent::__construct();
		$this->load->model("Home_model", "model", TRUE);

	}

	public function index(){
		if (!$this->getLogin()) {
			redirect(base_url("Login"));
		} else {
			// $this->viewInit(array(
			// 	"pageName" => "home",
			// 	"webTitle" => "首頁",
			// 	"bodyClass" => "home-1 home-2",
			// 	"component" => array(
			// 		"header","about","features","step","products","application","blog"
			// 	),
			// ));
			//載入從Controllers要傳入View的資料 ex:session的名子還是id
			$transferData = $this->getTransferData();
			//初始化view->並把Home_View的物件建立起來->建立起來只是在記憶體中並未載入選染
			$this->initView('Home_view', $transferData);
		}
	}
	public function mail()
	{
		$this->load->library('email');
		$res = array();
		$adminEmail = $this->model->getAdminEmail();
		for ($i=0; $i < count($adminEmail); $i++) {
			$config['protocol']    = 'smtp';
			$config['smtp_host']    = 'ssl://smtp.gmail.com';
			$config['smtp_port']    = '465';
			$config['smtp_timeout'] = '7';
			$config['smtp_user'] = 'kiritolin20@gmail.com';
			$config['smtp_pass'] = 'kir0910372857';
			$config['charset']    = 'utf-8';
			$config['newline']    = "\r\n";
			$config['mailtype'] = 'text'; // or html
			$config['validation'] = TRUE; // bool whether to validate email or not
			$this->email->initialize($config);
			$this->email->from('kiritolin20@gmail.com', '文蛤系統後台');
			$this->email->to($adminEmail[$i]);
			$this->email->subject('新增賣家或廠商');
			$msg = "帳號：" . "123456". "有新訂單\n請至後台確認";
			$this->email->message($msg);
			$this->email->send();
			$this->email->print_debugger();
		}
	}
}
