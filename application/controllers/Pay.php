<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pay extends Infrastructure
{

    /**
     * 載入父類別建構方法
     * 預先處理需執行的項目
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Pay_model", "model", TRUE);
    }

    public function index()
    {
        if (!$this->getLogin()) {
            redirect(base_url("Login"));
        } else {
            // $this->viewInit(array(
            // 	"pageName" => "home",
            // 	"webTitle" => "首頁",
            // 	"bodyClass" => "home-1 home-2",
            // 	"component" => array(
            // 		"header","about","features","step","products","application","blog"
            // 	),
            // ));
            //載入從Controllers要傳入View的資料 ex:session的名子還是id
            $transferData = $this->getTransferData();
            //初始化view->並把Home_View的物件建立起來->建立起來只是在記憶體中並未載入選染0
            $this->initView('Pay_view', $transferData);
        }
    }
    /**
     * 取得該頁面的datatable
     */
    public function getDataTable()
    {
        //可以被排序的欄位
        $order = array(null, null, null, null, 'order_time', 'user_name', 'user_phone', null);
        //可以被搜索的欄位
        $like = array(null, null, null, null, 'order_time', 'user_name', 'user_phone', null);

        $check = '<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" style="margin:auto" value="[extra]">
                    <span class="custom-control-indicator"></span> <span class="custom-control-description"></span> 
                </label>';
        $key = "<p onclick=checkOrder('[extra]') style='color:blue;text-decoration:underline;cursor: pointer;')>[extra]</p>";
        $type = "[extra]";
        $button = "<button type='button' class='btn btn-outline-info' onclick=pay('[extra]')><i class='fa fa-check' aria-hidden='true' style='width:15px'></i></button>
                   <button type='button' class='btn btn-outline-info'onclick=report_print('".base_url(). "Shipper/print/[extra]')><i class='fa fa-print' aria-hidden='true' style='width:15px'></i></button>
                    <button type='button' class='btn btn-outline-warning' onclick=getModifyOrderData('[extra]')><i class='fa fa-pencil' aria-hidden='true' style='width:15px'></i></button>
                   <button type='button' class='btn btn-outline-danger' onclick=deleteData('[extra]')><i class='fa fa-trash' aria-hidden='true' style='width:15px'></i></button>";
        $extra = array('key', 'key', 'key', 'type', 'key', 'key', 'key', 'key');
        $judge = function ($value, $case) {
            if ($case == 4) {
                if ($value == 0) {
                    return "賣家未審核";
                } else if ($value == 1) {
                    return "訂單已成立";
                } else if ($value == 2) {
                    return "買家已取消";
                } else {
                    return "雙方已取消";
                }
            } else {
                return $value;
            }
        };
        $price = '( SELECT SUM(`order_history`.`order_history_price`)
                    FROM order_history
                    WHERE `order_history`.`order_key` = `order`.`order_key`
                    )';
        $amount = '( SELECT SUM(`order_history`.`order_history_amount`)
                    FROM order_history
                    WHERE `order_history`.`order_key` = `order`.`order_key`
                    )';

        //輸出的順序
        $output = array($check,$key, $price, $type, 'order_time', 'user_name', 'user_phone', $button);
        //資料庫設定
        $this->light_datatables->ci->db->select('(`order_history`.`order_history_key`) as `history_key`,
                                                ' . $price . ',
                                                ' . $amount . ',
                                                `order_history.order_history_product_name` as `name`,
                                                `order_history.order_history_product_unit` as `unit`,
                                                `order_history.order_history_product_img` as `img`,
                                                `order.order_key` as `key`,
                                                `order.order_time` as `order_time`,
                                                `order.order_judge_type` as `type`,
                                                `order.order_user_pay_time` as `pay_time`,
                                                `order.order_display` as `display`,
                                                `user.user_name` as `user_name`,
                                                `user.user_phone` as `user_phone`,
                                                `order.order_shipped_time` as `shipped_time`');
        $this->light_datatables->ci->db->from('order');
        $this->light_datatables->ci->db->where('order.order_display', '1');
        $this->light_datatables->ci->db->where('order.order_judge_type', '1');
        $this->light_datatables->ci->db->where('order.order_admin_pay_time', NULL, FALSE);
        $this->light_datatables->ci->db->join('order_history', 'order.order_key=order_history.order_key');
        $this->light_datatables->ci->db->join('user', 'order.user_key=user.user_key');
        $this->light_datatables->ci->db->group_by('order_history.order_key');

        //輸出整合
        $this->light_datatables->set_querycolumn($order, $like);
        $this->light_datatables->order_by('key', 'DESC');
        $this->light_datatables->set_output($output, $extra, $judge, true);

        //輸出報表
        echo $this->light_datatables->get_datatable();
        // echo $this->light_datatables->ci->db->last_query();
    }
    /**
     * 取得要更改的該筆資料
     */
    public function getModifyData()
    {
        $data = json_decode($_POST["data"], true);
        $user_data = $this->model->getModifyData($data['key'], $data['value']);
        if ($user_data) {
            if ($user_data->num_rows() == 0) {
                $data = array('status' => 0, 'data' => array());
            } else {
                $result = $user_data->row();
                $file_config[] = array("caption" => $result->product_info_img, "height" => "109px", "key" => $data['key'], "filename" => base_url() . "uploads/" .  $result->product_info_img);
                $file_path[] = base_url() . "uploads/" . $result->product_info_img;
                $data = array('status' => 1, 'data' => $result, "overwriteInitial" => true, "initialPreviewConfig" => $file_config, "initialPreview" => $file_path);
            }
            // echo $this->db->last_query();
            echo json_encode($data);
        } else {
            // echo $this->db->last_query();
            echo $this->model->getDBErrJson();
        }
    }
    /**
     * 新增資料
     */
    public function insertProduct()
    {
        $data = $this->xss(json_decode($_POST["data"], true));
        $result = $this->model->insertProduct($data);
        $data = array('status' => 1, "key" => $result);
        echo json_encode($data);
    }
    /**
     * 刪除該筆資料
     */
    public function deleteData()
    {
        $key = json_decode($_POST["data"], true);
        $user_data = $this->model->deleteData($key);
        if ($user_data) {
            $data = array('status' => 1);
            echo json_encode($data);
        } else {
            echo $this->model->getDBErrJson();
        }
    }
    /**
     * 更新資料的方法
     */
    public function modifyData()
    {
        $data = $this->xss(json_decode($_POST["data"], true));
        $this->model->modifyData($data);
        $data = array('status' => '1');
        echo json_encode($data);
    }

    /**
     * [批次上傳附件]
     * @return [type] [description]
     */
    public function fileInput()
    {
        if (empty($_FILES["setFile"])) {/* 如果空就回傳找不到檔案 */
            echo json_encode(["error" => "找不到檔案"]);
            return;
        } else {
            for ($i = 0; $i < count($_FILES["setFile"]["name"]); $i++) {
                $_FILES["tempFile"]["name"] = $_SESSION["member_account"] . "_" . preg_replace("/[\-\[\]\/\{\}:;#%=\(\)\*\+\?\\\\^\$\|<>&\"\' ]/", "_", $_FILES["setFile"]["name"][$i]);
                $_FILES["tempFile"]["type"] = $_FILES["setFile"]["type"][$i];
                $_FILES["tempFile"]["tmp_name"] = $_FILES["setFile"]["tmp_name"][$i];
                $_FILES["tempFile"]["error"] = $_FILES["setFile"]["error"][$i];
                $_FILES["tempFile"]["size"] = $_FILES["setFile"]["size"][$i];
                $config["file_name"] = iconv(mb_detect_encoding($_FILES["tempFile"]["name"]), "UTF-8//Ignore", $_FILES["tempFile"]["name"]);/* 設定編碼 */
                // $config["max_size"]  = 20480;/* 上傳檔案大小限制 */
                $config["detect_mime"] = true;
                $config["upload_path"] = "./uploads/";/* 上傳路徑 */
                $config["allowed_types"] = "bmp|jpg|png|gif|pdf|txt|csv|doc|docx|xls|xlsx|ppt|pptx|zip|rar|tar|gzip|gz|7z";/* 限制上傳格式 */
                $this->load->library("upload", $config);/* CI3檔案上傳lib */
                $this->upload->initialize($config);
                if ($this->upload->do_upload("tempFile")) {/* 檔案上傳 */
                    // $this->db->changeProductImg($_FILES["tempFile"]["name"],)
                    $output = ["name" => $_FILES["tempFile"]["name"]];
                } else {
                    $output = ["error" => "處理上傳失敗 " . $this->upload->display_errors()];
                }
            }
            echo json_encode($output);
        }
    }
    /**
     * 取得要更新的標題圖片
     */
    public function modifyInfoImg()
    {
        $data = $this->xss(json_decode($_POST["data"], true));
        $this->model->modifyInfoImg($data); //取得該筆會員的id
        $data = array('status' => 1);
        echo json_encode($data);
    }
    /**
     * 取得要更新的詳細圖片
     */
    public function getModifyImg()
    {
        $data = json_decode($_POST["data"], true);
        $result = $this->model->getModifyImg($data['key'])->result_array();
        if ($result) {
            if (count($result) == 0) {
                $data = array('status' => 0, 'data' => array());
            } else {
                $file_path = array();
                $file_config = array();
                $file_count = count($result);

                for ($i = 0; $i < count($result); $i++) {
                    $file_config[] = array("height" => "109px", "key" => $result[$i]['key'], "filename" => base_url() . "uploads/" . $result[$i]["name"]);
                    $file_path[] = base_url() . "uploads/" . $result[$i]["name"];
                }
                $data = array("overwriteInitial" => true, "initialPreviewConfig" => $file_config, "initialPreview" => $file_path, "fileCount" => $file_count, "info_key" => $result[0]['info_key']);
            }
            // echo $this->db->last_query();
            echo json_encode($data);
        } else {
            // echo $this->db->last_query();
            echo $this->model->getDBErrJson();
        }
    }
    /**
     * 更新圖片的方法
     */
    public function modifyImg()
    {
        $data = $this->xss(json_decode($_POST["data"], true));
        $this->model->modifyImg($data);
        $data = array('status' => '1');
        echo json_encode($data);
    }
    /**
     * 更新圖片的方法
     */
    public function accessOrder()
    {
        $data = (json_decode($_POST["data"], true));
        $this->model->accessOrder($data);
        $lineKey = $this->model->getLineKey($data)->row()->line_key;
        $this->userJudge((string)$lineKey,(string)(sha1($data)));
        $data = array('status' => '1');
        echo json_encode($data);

    }
    /**
     * 取得當筆訂單
     */
    public function checkOrder()
    {
        $data = json_decode($_POST["data"], true);
        $user_data = $this->model->checkOrder($data);
        if ($user_data[0]) {
            if ($user_data[0]->num_rows() == 0) {
                $data = array('status' => 0, 'product' => array());
            } else {
                $result = $user_data[0]->result_array();
                $result2 = $user_data[1]->result_array();
                $data = array('status' => 1, 'product' => $result, 'order' => $result2);
            }
            // echo $this->db->last_query();
            echo json_encode($data);
        } else {
            // echo $this->db->last_query();
            echo $this->model->getDBErrJson();
        }
    }
    /**
     * 新增付款時間
     *
     * @return void
     */
    public function pay()
    {
        $data = (json_decode($_POST["data"], true));
        $this->model->pay($data);
        $lineKey = $this->model->getLineKey($data)->row()->line_key;
        $this->sendMessage('user', (string) $lineKey, '訂單編號：' . sha1($data) . '賣家以確認付款，如有問題請洽店家。');
        $data = array('status' => '1');
        echo json_encode($data);
    }
    /**
     * 選取付款
     *
     * @return void
     */
    public function selectPay()
    {
        $data = $this->xss(json_decode($_POST["data"], true));
        $this->model->selectPay($data);
        $data = array('status' => '1');
        echo json_encode($data);
    }

}
