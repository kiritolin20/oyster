<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Personal extends Infrastructure
{

    /**
     * 載入父類別建構方法
     * 預先處理需執行的項目
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Personal_model", "model", TRUE);
    }

    public function index()
    {
        if (!$this->getLogin()) {
            redirect(base_url("Login"));
        } else {
            // $this->viewInit(array(
            // 	"pageName" => "home",
            // 	"webTitle" => "首頁",
            // 	"bodyClass" => "home-1 home-2",
            // 	"component" => array(
            // 		"header","about","features","step","products","application","blog"
            // 	),
            // ));
            //載入從Controllers要傳入View的資料 ex:session的名子還是id
            $transferData = $this->getTransferData();
            //初始化view->並把Home_View的物件建立起來->建立起來只是在記憶體中並未載入選染
            $this->initView('Personal_view', $transferData);
        }
    }
    /**
     * 取得該頁面的datatable
     */
    public function getDataTable()
    {
        //可以被排序的欄位
        $order = array('key', 'account', 'email');
        //可以被搜索的欄位
        $like = array('key', 'account', 'email');
        $button = "<button type='button' class='btn btn-outline-warning' onclick=getModifyData('[extra]') id='[extra]'><i class='fa fa-pencil' aria-hidden='true' style='width:15px'></i></button>
		<button type='button' class='btn btn-outline-danger' onclick=deleteData('[extra]')><i class='fa fa-trash' aria-hidden='true' style='width:15px'></i></button>";
        $extra = array('key', 'key', 'key');
        //輸出的順序
        $output = array('key', 'account', 'email', $button);
        //資料庫設定
        $this->light_datatables->ci->db->select('sha1(admin_key) as `key`,`admin_account` as account,`admin_email` as email');
        $this->light_datatables->ci->db->from('admin');
        //輸出整合
        $this->light_datatables->set_querycolumn($order, $like);
        $this->light_datatables->order_by('key', 'DESC');
        $this->light_datatables->set_output($output, $extra);

        //輸出報表
        echo $this->light_datatables->get_datatable();
    }
    /**
     * 取得要更改的該筆資料
     */
    public function getShop()
    {
        $user_data = $this->model->getShop();
        if ($user_data) {
            if ($user_data->num_rows() == 0) {
                $data = array('status' => 0, 'data' => array());
            } else {
                $data = array('status' => 1, 'data' => $user_data->row());
            }
            echo json_encode($data);
        } else {
            echo $this->model->getDBErrJson();
        }
    }
    /**
     * 刪除該筆資料
     */
    public function deleteData()
    {
        $key = json_decode($_POST["data"], true);
        $user_data = $this->model->deleteData($key);
        if ($user_data) {
            $data = array('status' => 1);
            echo json_encode($data);
        } else {
            echo $this->model->getDBErrJson();
        }
    }
    /**
     * 取得要更新的資料
     */
    public function modifyPassword()
    {
        $data = $this->xss(json_decode($_POST["data"], true));
        $check = $this->model->checkPassword($data); 
        if($check){
            if($check->num_rows() == 0) {
                echo json_encode(array('status' => 2, 'data' => array()));
            }else{
                $this->model->modifyPassword($data);
                echo json_encode(array('status' => 1, 'data' => array()));
            }
        }else{
            echo $this->model->getDBErrJson();
        }
        print_r(get_class_methods($data));
    }
    /**
     * 取得要更新的資料
     */
    public function modifyShop()
    {
        $data = $this->xss(json_decode($_POST["data"], true));
        $check = $this->model->modifyShop($data);
        if ($check) {
            echo json_encode(array('status' => 1, 'data' => array()));
        } else {
            echo $this->model->getDBErrJson();
        }
        print_r(get_class_methods($data));
    }
}
