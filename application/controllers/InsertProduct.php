<?php
defined('BASEPATH') or exit('No direct script access allowed');

class InsertProduct extends Infrastructure
{

    /**
     * 載入父類別建構方法
     * 預先處理需執行的項目
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Product_model", "model", TRUE);
    }

    public function index()
    {
        if (!$this->getLogin()) {
            redirect(base_url("Login"));
        } else {
            // $this->viewInit(array(
            // 	"pageName" => "home",
            // 	"webTitle" => "首頁",
            // 	"bodyClass" => "home-1 home-2",
            // 	"component" => array(
            // 		"header","about","features","step","products","application","blog"
            // 	),
            // ));
            //載入從Controllers要傳入View的資料 ex:session的名子還是id
            $transferData = $this->getTransferData();
            //初始化view->並把Home_View的物件建立起來->建立起來只是在記憶體中並未載入選染
            $this->initView('InsertProduct_view', $transferData);
        }
    }

    public function lineInsert()
    {
        if (!$this->getLogin()) {
            redirect(base_url("Login"));
        } else {
            $transferData = $this->getTransferData();
            $this->initView('InsertProduct_view', $transferData);
        }
    }

    /**
     * 取得該頁面的datatable
     */
    public function getDataTable()
    {
        //可以被排序的欄位
        $order = array('key', 'account', 'email');
        //可以被搜索的欄位
        $like = array('key', 'account', 'email');
        $button = "<button type='button' class='btn btn-outline-warning' onclick=getModifyData('[extra]') id='[extra]'>修改</button>
		<button type='button' class='btn btn-outline-danger' onclick=deleteData('[extra]')>刪除</button>";
        $extra = array('key', 'key', 'key');
        //輸出的順序
        $output = array('key', 'account', 'email', $button);
        //資料庫設定
        $this->light_datatables->ci->db->select('sha1(admin_key) as `key`,`admin_account` as account,`admin_email` as email');
        $this->light_datatables->ci->db->from('admin');
        //輸出整合
        $this->light_datatables->set_querycolumn($order, $like);
        $this->light_datatables->order_by('key', 'DESC');
        $this->light_datatables->set_output($output, $extra);

        //輸出報表
        echo $this->light_datatables->get_datatable();
    }
    /**
     * 取得要更改的該筆資料
     */
    public function getModifyData()
    {
        $data = json_decode($_POST["data"], true);
        $user_data = $this->model->getModifyData($data['key']);
        if ($user_data) {
            if ($user_data->num_rows() == 0) {
                $data = array('status' => 0, 'data' => array());
            } else {
                $data = array('status' => 1, 'data' => $user_data->row());
            }
            echo json_encode($data);
        } else {
            echo $this->model->getDBErrJson();
        }
    }
    /**
     * 刪除該筆資料
     */
    public function deleteData()
    {
        $key = json_decode($_POST["data"], true);
        $user_data = $this->model->deleteData($key);
        if ($user_data) {
            $data = array('status' => 1);
            echo json_encode($data);
        } else {
            echo $this->model->getDBErrJson();
        }
    }
    /**
     * 取得要更新的資料
     */
    public function modifyData()
    {
        $data = $this->xss(json_decode($_POST["data"], true));
        $this->model->modifyData($data); //取得該筆會員的id
        $data = array('status' => 1);
        echo json_encode($data);
    }
    /**
     * 新增資料
     */
    public function insertProduct()
    {
        $data = $this->xss(json_decode($_POST["data"], true));
        if (isset($data['lineKey'])) {
            $data['lineKey'] = $this->private_decrypt($data['lineKey']);
        }
        $result = $this->model->insertProduct($data);
        $data = array('status' => 1, "key" => $result);
        echo json_encode($data);
    }
    /**
     * [批次上傳附件]
     * @return [type] [description]
     */
    public function fileInput()
    {
        if (empty($_FILES["setFile"])) {/* 如果空就回傳找不到檔案 */
            echo json_encode(["error" => "找不到檔案"]);
            return;
        } else {
            for ($i = 0; $i < count($_FILES["setFile"]["name"]); $i++) {
                $_FILES["tempFile"]["name"] = $_SESSION["member_account"] . "_" . preg_replace("/[\-\[\]\/\{\}:;#%=\(\)\*\+\?\\\\^\$\|<>&\"\' ]/", "_", $_FILES["setFile"]["name"][$i]);
                $_FILES["tempFile"]["type"] = $_FILES["setFile"]["type"][$i];
                $_FILES["tempFile"]["tmp_name"] = $_FILES["setFile"]["tmp_name"][$i];
                $_FILES["tempFile"]["error"] = $_FILES["setFile"]["error"][$i];
                $_FILES["tempFile"]["size"] = $_FILES["setFile"]["size"][$i];
                $config["file_name"] = iconv(mb_detect_encoding($_FILES["tempFile"]["name"]), "UTF-8//Ignore", $_FILES["tempFile"]["name"]);/* 設定編碼 */
                // $config["max_size"]  = 20480;/* 上傳檔案大小限制 */
                $config["detect_mime"] = true;
                $config["upload_path"] = "./uploads/";/* 上傳路徑 */
                $config["allowed_types"] = "bmp|jpg|jpeg|png|gif|pdf|txt|csv|doc|docx|xls|xlsx|ppt|pptx|zip|rar|tar|gzip|gz|7z";/* 限制上傳格式 */
                $this->load->library("upload", $config);/* CI3檔案上傳lib */
                $this->upload->initialize($config);
                if ($this->upload->do_upload("tempFile")) {/* 檔案上傳 */
                    // $this->db->changeProductImg($_FILES["tempFile"]["name"],)
                    $output = ["name" => $_FILES["tempFile"]["name"]];
                } else {
                    $output = ["error" => "處理上傳失敗 " . $this->upload->display_errors()];
                }
            }
            echo json_encode($output);
        }
    }
    /**
     * 取得要更新的標題圖片
     */
    public function modifyInfoImg()
    {
        $data = $this->xss(json_decode($_POST["data"], true));
        $this->model->modifyInfoImg($data); //取得該筆會員的id
        $data = array('status' => 1);
        echo json_encode($data);
    }
}
