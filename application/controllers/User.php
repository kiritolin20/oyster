<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends Infrastructure
{

    /**
     * 載入父類別建構方法
     * 預先處理需執行的項目
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model("User_model", "model", TRUE);
    }

    public function index()
    {
        if (!$this->getLogin()) {
            redirect(base_url("Login"));
        } else {
            // $this->viewInit(array(
            // 	"pageName" => "home",
            // 	"webTitle" => "首頁",
            // 	"bodyClass" => "home-1 home-2",
            // 	"component" => array(
            // 		"header","about","features","step","products","application","blog"
            // 	),
            // ));
            //載入從Controllers要傳入View的資料 ex:session的名子還是id
            $transferData = $this->getTransferData();
            //初始化view->並把Home_View的物件建立起來->建立起來只是在記憶體中並未載入選染
            $this->initView('User_view', $transferData);
        }
    }
    /**
     * 取得該頁面的datatable
     */
    public function getDataTable()
    {
        
        //可以被排序的欄位
        $order = array('encryption', 'area', 'name','shop_address','priceType','shop_phone',null,'address','member','phone');
        //可以被搜索的欄位
        $like = array('encryption', 'area', 'name','shop_address','priceType', 'shop_phone',null,'address','member','phone');
        $button = "<button type='button' class='btn btn-outline-warning' onclick=getModifyData('[extra]') id='[extra]'><i class='fa fa-pencil' aria-hidden='true' style='width:15px'></i></button>
        <button type='button' class='btn btn-outline-info' onclick=editFavorite('[extra]')><i class='fa fa-plus' aria-hidden='true' style='width:15px'></i></button>
		<button type='button' class='btn btn-outline-danger' onclick=deleteData('[extra]')><i class='fa fa-trash' aria-hidden='true' style='width:15px'></i></button>";
        $extra = array('key', 'key', 'user_key', 'key');
        //輸出的順序
        $output = array('encryption', 'area', 'name','shop_address','priceType', 'shop_phone', $button,'address','member','phone');
        //資料庫設定
        $this->light_datatables->ci->db->select('sha1(`user`.`user_key`) as `key`,
                                                `user_key` as `user_key`,
                                                `user_name` as `name`,
                                                `user_address` as `address`,
                                                `user_area` as `area`,
                                                `user_priceType` as `priceType`,
                                                `user_phone` as `phone`,
                                                `user_encryption` as `encryption`,
                                                `user_member` as `member`,
                                                `user_shop_phone` as `shop_phone`,
                                                `user_shop_address` as `shop_address`');
        $this->light_datatables->ci->db->from('user');
        //輸出整合
        $this->light_datatables->set_querycolumn($order, $like);
        $this->light_datatables->order_by('`user`.`user_key`', 'DESC');
        $this->light_datatables->set_output($output, $extra);

        //輸出報表
        echo $this->light_datatables->get_datatable();
    }
    /**
     * 取得要更改的該筆資料
     */
    public function getModifyData()
    {
        $data = json_decode($_POST["data"], true);
        $user_data = $this->model->getModifyData($data['key']);
        if ($user_data) {
            if ($user_data->num_rows() == 0) {
                $data = array('status' => 0, 'data' => array());
            } else {
                $data = array('status' => 1, 'data' => $user_data->row());
            }
            echo json_encode($data);
        } else {
            echo $this->model->getDBErrJson();
        }
    }
    /**
     * 刪除該筆資料
     */
    public function deleteData()
    {
        $key = json_decode($_POST["data"], true);
        $user_data = $this->model->deleteData($key);
        if ($user_data) {
            $data = array('status' => 1);
            echo json_encode($data);
        } else {
            echo $this->model->getDBErrJson();
        }
    }
    /**
     * 取得要更新的資料
     */
    public function modifyData()
    {
        $data = $this->xss(json_decode($_POST["data"], true));
        $this->model->modifyData($data); //取得該筆會員的id
        $data = array('status' => 1);
        echo json_encode($data);
    }
    /**
     * 新增資料
     */
    public function insertProduct()
    {
        $data = $this->xss(json_decode($_POST["data"], true));
        $result = $this->model->insertProduct($data);
        $data = array('status' => 1, "key" => $result);
        echo json_encode($data);
    }

    public function getPrice()
    {
        $month = date('m');
        $weekArray = $this->thisweek();
        $encryption = ($_POST["data"]);
        $result = $this->model->getPrice($encryption,$month,$weekArray);
        $data = array('price' => $result);
        echo json_encode($data);
    }

    /**
     * 取得該頁面的datatable
     */
    public function getfavoriteDataTable()
    {
        //可以被排序的欄位
        $order = array(null, 'name', 'info', null, 'unit', null);
        //可以被搜索的欄位
        $like = array(null, 'name', 'info', null, 'unit', null);

        $img = '<img src="' . base_url() . 'uploads/[extra]"  class="img-thumbnail" width="100%"/>';

        $check = '<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" style="margin:auto" value="[extra]" >
                    <span class="custom-control-indicator"></span> <span class="custom-control-description"></span> 
                </label>';

        $extra = array('product_key','img');
        $price = '( SELECT price_history.ph_price
                    FROM price_history
                    WHERE `price_history`.product_key = `product`.product_key
                    ORDER BY price_history.ph_time DESC
                    LIMIT 1)';
        $output = array($check , $img, 'name', 'unit', $price, 'info');
        $this->light_datatables->ci->db->select('sha1(`product`.`product_key`) as `key`,
                                                `product_key` as `product_key`,
                                                `product.product_name` as name,
                                                `product.product_info` as info,
                                                `product.product_display` as display,
                                                ' . $price . ',
                                                `product_unit` as unit,
                                                `product_img` as img');
        $this->light_datatables->ci->db->from('product');
        $this->light_datatables->ci->db->where('product.product_display', 1);
        $this->light_datatables->set_querycolumn($order, $like);
        $this->light_datatables->order_by('product.product_key', 'DESC');
        $this->light_datatables->set_output($output, $extra);
        echo $this->light_datatables->get_datatable();
    }

    /**
     * 新增資料
     */
    public function modifyFavorite()
    {
        $data = $this->xss(json_decode($_POST["data"], true));
        $result = $this->model->modifyFavorite($data);
        $data = array('status' => 1);
        echo json_encode($data);
    }

    /**
     * 新增資料
     */
    public function getFavorite()
    {
        $data = $this->xss(json_decode($_POST["data"], true));
        $result = $this->model->getFavorite($data);
        $data = array('status' => 1, "product_key" => $result->result_array());
        echo json_encode($data);
    }

}
