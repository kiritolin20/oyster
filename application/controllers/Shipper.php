<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Shipper extends Infrastructure
{

    /**
     * 載入父類別建構方法
     * 預先處理需執行的項目
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Order_model", "model", TRUE);
    }

    public function index()
    {
        if (!$this->getLogin()) {
            redirect(base_url("Login"));
        } else {
            //載入從Controllers要傳入View的資料 ex:session的名子還是id
            $transferData = $this->getTransferData();
            //初始化view->並把Home_View的物件建立起來->建立起來只是在記憶體中並未載入選染
            $this->initView('Shipper_view2', $transferData);
        }
    }
    public function print($id)
    {
        $user_data = $this->model->checkOrder($id);
        if ($user_data[0]) {
            if ($user_data[0]->num_rows() == 0) {
                $data['product'] = '';
                $data['order'] = '';
            } else {
                $result = $user_data[0]->result_array();
                $result2 = $user_data[1]->result_array();
                $data['product'] = $result;
                $data['order'] = $result2;
            }
            $this->load->view('shipper_print2', $data);
        } else {
            $this->load->view('shipper_print2', $data);
        }
    }

    public function print_str()
    {
        $data = json_decode($_POST["data"], true);
        $str = "";
        for ($i=0; $i < count($data); $i++) {
            if ($i == count($data)-1){
                $str .= $data[$i];
            } else {
                $str .= $data[$i]."-";
            }
        }
        echo json_encode($str);

    }

    public function plural_print($id)
    {
        $id = explode("-", $id);
        $data = [];
        for ($i=0; $i < count($id); $i++) {
            $data['product'][$i] = $this->model->getProductModel($id[$i]);
        }
        $this->load->view('plural_print', $data);
    }
}
