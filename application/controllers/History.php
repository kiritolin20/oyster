<?php
defined('BASEPATH') or exit('No direct script access allowed');

class History extends Infrastructure
{

    /**
     * 載入父類別建構方法
     * 預先處理需執行的項目
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Order_model", "model", TRUE);
    }

    public function index()
    {
        if (!$this->getLogin()) {
            redirect(base_url("Login"));
        } else {
            // $this->viewInit(array(
            // 	"pageName" => "home",
            // 	"webTitle" => "首頁",
            // 	"bodyClass" => "home-1 home-2",
            // 	"component" => array(
            // 		"header","about","features","step","products","application","blog"
            // 	),
            // ));
            //載入從Controllers要傳入View的資料 ex:session的名子還是id
            $transferData = $this->getTransferData();
            //初始化view->並把Home_View的物件建立起來->建立起來只是在記憶體中並未載入選染
            $this->initView('History_view', $transferData);
        }
    }
    /**
     * 取得該頁面的datatable
     */
    public function getDataTable()
    {
        //可以被排序的欄位
        $order = array(null, null, null, 'order_time', 'shipped_time', 'user_name', 'user_phone', null);
        //可以被搜索的欄位
        $like = array(null, null, null, 'order_time', 'shipped_time', 'user_name', 'user_phone', null);

        $key = "<p onclick=checkOrder('[extra]') style='color:blue;text-decoration:underline;cursor: pointer;')>[extra]</p>";
        $type = "[extra]";
        $button = "<button type='button' class='btn btn-outline-info' onclick=report_print('".base_url(). "Shipper/print/[extra]')><i class='fa fa-print' aria-hidden='true' style='width:15px'></i></button>
        <button type='button' class='btn btn-outline-danger' onclick=deleteData('[extra]')><i class='fa fa-trash' aria-hidden='true' style='width:15px'></i></button>";
        $extra = array('key', 'key', 'type', 'key', 'key');
        $judge = function ($value, $case) {
            if ($case == 3) {
                if ($value == 0) {
                    return "賣家未審核";
                } else if ($value == 1) {
                    return "訂單已成立";
                } else if ($value == 2) {
                    return "買家已取消";
                } else {
                    return "雙方已取消";
                }
            } else {
                return $value;
            }
        };
        $price = '( SELECT SUM(`order_history`.`order_history_price`)
                    FROM order_history
                    WHERE `order_history`.`order_key` = `order`.`order_key`
                    )';
        $amount = '( SELECT SUM(`order_history`.`order_history_amount`)
                    FROM order_history
                    WHERE `order_history`.`order_key` = `order`.`order_key`
                    )';

        //輸出的順序
        $output = array($key, $price, $type, 'order_time', 'shipped_time', 'user_name', 'user_phone', $button);
        //資料庫設定
        $this->light_datatables->ci->db->select('(`order_history`.`order_history_key`) as `history_key`,
                                                ' . $price . ',
                                                ' . $amount . ',
                                                `order_history.order_history_product_name` as `name`,
                                                `order_history.order_history_product_unit` as `unit`,
                                                `order_history.order_history_product_img` as `img`,
                                                `order.order_key` as `key`,
                                                `order.order_time` as `order_time`,
                                                `order.order_judge_type` as `type`,
                                                `order.order_admin_pay_time` as `pay_time`,
                                                `order.order_display` as `display`,
                                                `user.user_name` as `user_name`,
                                                `user.user_phone` as `user_phone`,
                                                `order.order_shipped_time` as `shipped_time`');
        $this->light_datatables->ci->db->from('order');
        $this->light_datatables->ci->db->where('order.order_display', '1');
        $this->light_datatables->ci->db->where('order.order_shipped_time IS NOT NULL', NULL, FALSE);
        $this->light_datatables->ci->db->where('order.order_admin_pay_time IS NOT NULL', NULL, FALSE);
        // $this->light_datatables->ci->db->where('order.order_user_pay_time IS NOT NULL', NULL, FALSE);

        $this->light_datatables->ci->db->join('order_history', 'order.order_key=order_history.order_key');
        $this->light_datatables->ci->db->join('user', 'order.user_key=user.user_key');
        $this->light_datatables->ci->db->group_by('order_history.order_key');

        //輸出整合
        $this->light_datatables->set_querycolumn($order, $like);
        $this->light_datatables->order_by('key', 'DESC');
        $this->light_datatables->set_output($output, $extra, $judge, true);

        //輸出報表
        echo $this->light_datatables->get_datatable();
        // echo $this->light_datatables->ci->db->last_query();
    }
    /**
     * 取得要更改的該筆資料
     */
    public function getModifyData()
    {
        $data = json_decode($_POST["data"], true);
        $user_data = $this->model->getModifyData($data['key'], $data['value']);
        if ($user_data) {
            if ($user_data->num_rows() == 0) {
                $data = array('status' => 0, 'data' => array());
            } else {
                $result = $user_data->row();
                $file_config[] = array("caption" => $result->product_info_img, "height" => "109px", "key" => $data['key'], "filename" => base_url() . "uploads/" .  $result->product_info_img);
                $file_path[] = base_url() . "uploads/" . $result->product_info_img;
                $data = array('status' => 1, 'data' => $result, "overwriteInitial" => true, "initialPreviewConfig" => $file_config, "initialPreview" => $file_path);
            }
            // echo $this->db->last_query();
            echo json_encode($data);
        } else {
            // echo $this->db->last_query();getDBErrJson
            echo $this->model->getDBErrJson();
        }
    }

    /**
     * 刪除該筆資料
     */
    public function deleteData()
    {
        $key = json_decode($_POST["data"], true);
        $user_data = $this->model->deleteData($key);
        if ($user_data) {
            $data = array('status' => 1);
            echo json_encode($data);
        } else {
            echo $this->model->getDBErrJson();
        }
    }
    /**
     * 更新資料的方法
     */
    public function modifyData()
    {
        $data = $this->xss(json_decode($_POST["data"], true));
        $this->model->modifyData($data);
        $data = array('status' => '1');
        echo json_encode($data);
    }
    /**
     * 取得當筆訂單
     */
    public function checkOrder()
    {
        $data = json_decode($_POST["data"], true);
        $user_data = $this->model->checkOrder($data);
        if ($user_data[0]) {
            if ($user_data[0]->num_rows() == 0) {
                $data = array('status' => 0, 'product' => array());
            } else {
                $result = $user_data[0]->result_array();
                $result2 = $user_data[1]->result_array();
                $data = array('status' => 1, 'product' => $result, 'order' => $result2);
            }
            // echo $this->db->last_query();
            echo json_encode($data);
        } else {
            // echo $this->db->last_query();
            echo $this->model->getDBErrJson();
        }
    }
}
