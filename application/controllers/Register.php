<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Register extends Infrastructure
{

    /**
     * 建構載入需要預先執行的項目
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Login_model", "model", TRUE);
    }

    public function index(){
        if ($this->getLogin()) {
            redirect(base_url("Home"));
        } else {
            $transferData = $this->getTransferData();
            //初始化view->並把Home_View的物件建立起來->建立起來只是在記憶體中並未載入選染
            $this->initView('Register_view', $transferData);
        }
    }

    /***
	傳入值為：加密過的字串
	將字串解密之後
     ***/
    public function lineRegister(){
        if ($this->getLogin()) {
            redirect(base_url("Home"));
        } else {
            $transferData = $this->getTransferData();
            //初始化view->並把Home_View的物件建立起來->建立起來只是在記憶體中並未載入選染
            $this->initView('Register_view', $transferData);
        }
	}
    /***
	傳入值為：$_Post['account','password','email','lineKey']
	傳入驗證：資料消毒
	成功 echo 2;重複帳號 echo 1;失敗 echo 0
     ***/
    public function checkRegister(){
        $data = $this->xss(json_decode($_POST["data"], true));
        $account = $data['account'];
        $password = $data['password'];  //將密碼使用md5加密法加密
        $email = $data['email'];
        $LoginCheck = $this->model->checkRepeat($account, $email);    //先確認有沒有重複註冊
        if ($LoginCheck) {
            if ($LoginCheck->num_rows() == 1) {
                $data = array('status' => 1, 'data' => array());
            } else {
                if (isset($data['lineKey'])) {
                    $data['lineKey'] = $this->private_decrypt($data['lineKey']);
                    $this->model->insertAdmin($data);
                } else {
                    $this->model->insertAdmin($data);
                }
                
                $data = array('status' =>2);
            }
            echo json_encode($data);
        } else {
            echo $this->model->getDBErrorMSG();
        }
    }
}
