<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LineAPI extends CI_Controller {
	//建構預載入需要執行的model
	public function __construct() {
		parent::__construct();
		$this->load->model("LineAPIModel","model",TRUE);

	}

	public function index() {
		$this->load->view('welcome_message');
	}

	/*=====================================*/
	/*登入功能，搜尋密碼是否存在於User資料表中*/
	/*=====================================*/
	public function login($user_enceyption,$user_line_key){
		$query = $this->model->login($user_enceyption);
		if(count($query)>0 && $query[0]->user_line_key == ""){
			$this->addUserLineKey($query[0]->user_key,$user_line_key);
		}
		echo json_encode($query);
	}

	/*=====================================*/
	/*將user_line_key存入user資料表中*/
	/*=====================================*/
	public function addUserLineKey($user_key,$user_line_key){
		$this->model->addUserLineKey($user_key,$user_line_key);
	}

	/*=====================================*/
	/*登入功能，搜尋密碼是否存在於User資料表中*/
	/*=====================================*/
	public function getProductInfo(){
		$query = $this->model->getProductInfo();
		echo json_encode($query);
	}

	/*=====================================*/
	/*新增購物車功能，將斤數的數量與總金額寫入購物車(cart)資料表中*/
	/*=====================================*/
	public function addCartKG($user_key,$product_key,$product_price,$cart_amount){
		if($this->model->addCartKG($user_key,$product_key,$product_price,$cart_amount)){
			echo json_encode(array("status"=>"success"));
		}else{
			echo json_encode(array("status"=>"error"));
		}
	}

	/*=====================================*/
	/*新增購物車功能，將斤數的數量與總金額寫入購物車(cart)資料表中*/
	/*=====================================*/
	public function addCartPrice($user_key,$product_key,$product_price,$cart_price){
		if($this->model->addCartPrice($user_key,$product_key,$product_price,$cart_price)){
			echo json_encode(array("status"=>"success"));
		}else{
			echo json_encode(array("status"=>"error"));
		}
	}

	/*=====================================*/
	/*查詢購物車功能*/
	/*=====================================*/
	public function getUserCart($user_key){
		$query = $this->model->getUserCart($user_key);
		echo json_encode($query);
	}

	/*=====================================*/
	/*刪除購物車功能*/
	/*=====================================*/
	public function deleteUserCart($productKey,$user_key){
		if($this->model->deleteUserCart($productKey,$user_key)){
			echo json_encode(array("status"=>"success"));
		}else{
			echo json_encode(array("status"=>"error"));
		}
	}

	/*=====================================*/
	/*送出訂單功能*/
	/*=====================================*/
	public function addOrder($user_key) {
		$time = date('Y-m-d H:i:s');
		if(count($this->model->getUserCart($user_key)) > 0) {
			if($this->model->addOrder($user_key,$time)){
				$order_key = $this->db->insert_id()."<br>";
				$user_cart = $this->model->getUserCart($user_key);
				$return_query = true;
				foreach($user_cart as $row) {
					if(!$this->model->addOrderHistory($row->product_price*$row->cart_amount,$row->cart_amount,$row->product_name,$row->product_unit,$row->product_img,$order_key,$row->product_key)) {
						$return_query = false;
					}
				}

				if($return_query) {
					$this->model->clearCart($user_key);
					echo json_encode(array("status"=>"success"));
				} else {
					echo json_encode(array("status"=>"error"));
				}
			} else {
				echo json_encode(array("status"=>"error"));
			}
		} else {
			echo json_encode(array("status"=>"no cart"));
		}
	}

	/*=====================================*/
	/*查詢目前每個訂單的數量*/
	/*=====================================*/
	public function getOrderTotal($user_key) {
		$time = explode("-",date("Y-m-d"));
		$query = $this->model->getOrderTotal($user_key,$time[0],$time[1],$time[2]);

		$noShipped = 0;
		$beginShipped = 0;

		foreach($query as $row) {
			if($row->order_shipped_time == null) {
				$noShipped++;
			} else {
				$beginShipped++;
			}
		}

		// echo json_encode($query);

		echo json_encode(array("noShipped"=>$noShipped,"beginShipped"=>$beginShipped,"data"=>$query));
	}

	public function mail() {
		$this->load->library('email');
		$res = array();
		$adminEmail = $this->model->getAdminEmail();
		for ($i=0; $i < count($adminEmail); $i++) {
			$config['protocol']    = 'smtp';
			$config['smtp_host']    = 'ssl://smtp.gmail.com';
			$config['smtp_port']    = '465';
			$config['smtp_timeout'] = '7';
			$config['smtp_user'] = 'kiritolin20@gmail.com';
			$config['smtp_pass'] = 'kir0910372857';
			$config['charset']    = 'utf-8';
			$config['newline']    = "\r\n";
			$config['mailtype'] = 'text'; // or html
			$config['validation'] = TRUE; // bool whether to validate email or not
			$this->email->initialize($config);
			$this->email->from('s177113230@gmail.com', '文蛤系統後台');
			$this->email->to($adminEmail[$i]);
			$this->email->subject('新增賣家或廠商');
			$msg = "帳號：" . "123456". "有新訂單\n請至後台確認";
			$this->email->message($msg);
			$this->email->send();
			$this->email->print_debugger();
		}
	}
}
