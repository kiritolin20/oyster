<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Statistics extends Infrastructure
{

    /**
     * 載入父類別建構方法
     * 預先處理需執行的項目
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Statistics_model", "model", TRUE);
    }

    public function index()
    {
        if (!$this->getLogin()) {
            redirect(base_url("Login"));
        } else {
            // $this->viewInit(array(
            // 	"pageName" => "home",
            // 	"webTitle" => "首頁",
            // 	"bodyClass" => "home-1 home-2",
            // 	"component" => array(
            // 		"header","about","features","step","products","application","blog"
            // 	),
            // ));
            //載入從Controllers要傳入View的資料 ex:session的名子還是id
            $transferData = $this->getTransferData();
            //初始化view->並把Home_View的物件建立起來->建立起來只是在記憶體中並未載入選染
            $this->initView('Statistics_view', $transferData);
        }
    }
    /**
     * 傳年分到後端取值
     */
    public function getTurnover()
    {
        $data = $this->xss(json_decode($_POST["data"], true));
        echo json_encode($this->model->getTurnOverData($data["year"], $data["month"]));
    }
    /**
     * [取得當月總數]
     *
     * @return void
     */
    public function getTotalAmountChart()
    {
        $data = $this->xss(json_decode($_POST["data"], true));
        echo json_encode($this->model->getTotalAmountChart_model($data["year"], $data["month"]));
    }
}
