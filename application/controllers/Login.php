<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Infrastructure {

	/**
	 * 建構載入需要預先執行的項目
	 */
	public function __construct() {
		parent::__construct();
		$this->load->model("Login_model","model",TRUE);

	}

	public function index(){
		if($this->getLogin()){
			redirect(base_url("Home"));
		}else{
			$transferData = $this->getTransferData();
			//初始化view->並把Home_View的物件建立起來->建立起來只是在記憶體中並未載入選染
			$this->initView('Login_view', $transferData);
		}
	}

	/***
	傳入值為：加密過的字串
	資料庫比對無資料：帶著加密過的字串到註冊頁面；資料庫比對有資料：進入主頁面
	 ***/
	public function lineLogin(){
		if ($this->getLogin()) {
			redirect(base_url("Home"));
		} else {
			$uriArr = explode("/", $_SERVER['PHP_SELF']);
			$r = $this->private_decrypt($uriArr[5]);
			$user_data = $this->model->checkLineLogin($r);
			if($user_data){
				if ($user_data->num_rows() == 1) {
					$this->model->setSession($user_data->row());
					redirect(base_url("Home"));
				} else {
					// echo $this->db->last_query();
					redirect(base_url("Register/lineRegister/" . $uriArr[5]));
				}
			}else{
				// echo $this->db->last_query();
				echo $this->model->getDBErrJson();
			}
			
		}
	}

	/***
	傳入值為：$_Post['account','password','token']
	傳入驗證：資料消毒
	未填寫echo 0; 失敗echo 1; 偽造請求 echo2;成功 echo3;
	 ***/
	public function checkLogin(){
		$this->load->library('ReCaptcha');
		$data = $this->xss(json_decode($_POST["data"],true));
		$account = $data['account'];
		$password = $data['password']; //將密碼使用md5加密法加密
		$secretKey = '6LfW0OEUAAAAAMC0rdz_XyNVWcaM05htJSSfqMgq';
		$captcha = $data['g-recaptcha-response'];
		$ip = $_SERVER['REMOTE_ADDR'];
		// empty response
		$response = null;
		// check secret key
		$reCaptcha = new ReCaptcha();
		$reCaptcha -> setSecret($secretKey);
		$response = $reCaptcha->verifyResponse(
			$_SERVER["REMOTE_ADDR"],
			$data["g-recaptcha-response"]
		);
		$user_data = $this->model->checkLogin($account, $password); //取得該筆會員的id
		if ($user_data) {
			if ($user_data->num_rows() == 0) {
				$data = array('status' => 0, 'data' => array());
			} else {
				if ($response != null && $response->success) {
					$data = array('status' => 1, 'data' => array(), 'key' => $user_data->row()->key);
					$this->model->setSession($user_data->row());
				}else{
					$data = array('status' => 2, 'data' => array());
				}
			}
			echo json_encode($data);
		} else {
			// echo $this->db->last_query();
			echo $this->model->getDBErrJson();
		}
	}

	public function out(){
		session_destroy();
		// redirect(base_url("login"));
		echo json_encode(array('status' => '1'));
	}
}