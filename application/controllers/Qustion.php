<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Qustion extends Infrastructure
{

    /**
     * 載入父類別建構方法
     * 預先處理需執行的項目
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Qustion_model", "model", TRUE);
    }

    public function index()
    {
        $this->adminSet();
    }

    public function adminSet()
    {
        if (!$this->getLogin()) {
            redirect(base_url("Login"));
        } else {
            // $this->viewInit(array(
            // 	"pageName" => "home",
            // 	"webTitle" => "首頁",
            // 	"bodyClass" => "home-1 home-2",
            // 	"component" => array(
            // 		"header","about","features","step","products","application","blog"
            // 	),
            // ));
            //載入從Controllers要傳入View的資料 ex:session的名子還是id
            $transferData = $this->getTransferData();
            //初始化view->並把Home_View的物件建立起來->建立起來只是在記憶體中並未載入選染
            $this->initView('Qustion_view', $transferData, "admin");
        }
    }

    public function sellerSet()
    {
        if (!$this->getLogin()) {
            redirect(base_url("Login"));
        } else {
            // $this->viewInit(array(
            // 	"pageName" => "home",
            // 	"webTitle" => "首頁",
            // 	"bodyClass" => "home-1 home-2",
            // 	"component" => array(
            // 		"header","about","features","step","products","application","blog"
            // 	),
            // ));
            //載入從Controllers要傳入View的資料 ex:session的名子還是id
            $transferData = $this->getTransferData();
            //初始化view->並把Home_View的物件建立起來->建立起來只是在記憶體中並未載入選染
            $this->initView('Qustion_view', $transferData, "seller");
        }
    }

    public function reply()
    {
        if (!$this->getLogin()) {
            redirect(base_url("Login"));
        } else {
            // $this->viewInit(array(
            // 	"pageName" => "home",
            // 	"webTitle" => "首頁",
            // 	"bodyClass" => "home-1 home-2",
            // 	"component" => array(
            // 		"header","about","features","step","products","application","blog"
            // 	),
            // ));
            //載入從Controllers要傳入View的資料 ex:session的名子還是id
            $transferData = $this->getTransferData();
            //初始化view->並把Home_View的物件建立起來->建立起來只是在記憶體中並未載入選染
            $this->initView('Qustion_view', $transferData, "reply");
        }
    }

    /**
     * 取得要更改的該筆資料
     */
    public function getQuestion()
    {
        $user_data = $this->model->getQuestion();
        if ($user_data) {
            if ($user_data->num_rows() == 0) {
                $data = array('status' => 0, 'data' => array());
            } else {
                $result = $user_data->result_array();
                $data = array('status' => 1, 'data' => $result);
            }
            // echo $this->db->last_query();
            echo json_encode($data);
        } else {
            // echo $this->db->last_query();
            echo $this->model->getDBErrJson();
        }
    }
    /**
     * 取得要更改的該筆資料
     */
    public function getSellerQuestion()
    {
        $user_data = $this->model->getSellerQuestion();
        if ($user_data) {
            if ($user_data->num_rows() == 0) {
                $data = array('status' => 0, 'data' => array());
            } else {
                $result = $user_data->result_array();
                $data = array('status' => 1, 'data' => $result);
            }
            // echo $this->db->last_query();
            echo json_encode($data);
        } else {
            // echo $this->db->last_query();
            echo $this->model->getDBErrJson();
        }
    }
    /**
     * 刪除該筆資料
     */
    public function getQuestionTh()
    {
        $user_data = $this->model->getQuestionTh();
        if ($user_data) {
            // if ($user_data->num_rows() == 0) {
                // $data = array('status' => 0, 'data' => array());
            // } else {
                $result = $user_data;
                $data = array('status' => 1, 'data' => $result);
            // }
            // echo $this->db->last_query();
            echo json_encode($data);
        } else {
            // echo $this->db->last_query();
            echo $this->model->getDBErrJson();
        }
    }
    /**
     * 更新資料的方法
     */
    public function modifyQuestion()
    {
        $data = $this->xss(json_decode($_POST["data"], true));
        $this->model->modifyQuestion($data);
        $data = array('status' => '1');
        echo json_encode($data);
    }

    /**
     * 取得該頁面的datatable
     */
    public function getDataTable()
    {
        //可以被排序的欄位
        $order = array(null, null, null);
        //可以被搜索的欄位
        $like = array(null, null, null);
        $answer1 = '[extra]';
        $answer2 = '[extra]';
        $answer3 = '[extra]';
        $answer4 = '[extra]';
        $answer5 = '[extra]';
        
        $extra = array('sa_answer1', 'sa_answer2', 'sa_answer3', 'sa_answer4', 'sa_answer5');
        $output = array($answer1, $answer2, $answer3, $answer4, $answer5);
        $judge = function ($value, $case) {
            for($i = 0 ; $i<5 ;$i++){
                if($case != Null){
                    // array_push($output,'$answer'.$i);
                    return $value . "顆星";
                }else{
                    return '無';
                }
            }
        };
        
        //輸出的順序
        // $output = array($answer1, $answer2, $answer3, $answer4, $answer5);
        //資料庫設定
        $this->light_datatables->ci->db->select('`shop_answer`.`sa_key` as `sa_key`,
                                                 `shop_answer`.`sq_key` as `sq_key`,
                                                 `shop_answer`.`order_key` as `order_key`,
                                                 `shop_answer`.`sa_answer1` as `sa_answer1`,
                                                 `shop_answer`.`sa_answer2` as `sa_answer2`,
                                                 `shop_answer`.`sa_answer3` as `sa_answer3`,
                                                 `shop_answer`.`sa_answer4` as `sa_answer4`,
                                                 `shop_answer`.`sa_answer5` as `sa_answer5`,
                                                 `shop_answer`.`shop_key` as `shop_key`');
        $this->light_datatables->ci->db->from('shop_answer');
        $this->light_datatables->ci->db->where('shop_answer.shop_key', $_SESSION['shop_key']);
        // $this->light_datatables->ci->db->join('shop_question', '`shop_answer`.`sq_key` = `shop_question`.`sq_key`');
        //輸出整合
        $this->light_datatables->set_querycolumn($order, $like);
        $this->light_datatables->order_by('`shop_answer`.`sa_key`', 'DESC');
        $this->light_datatables->set_output($output,$extra, $judge,true);

        //輸出報表
        echo $this->light_datatables->get_datatable();
        // echo $this->light_datatables->ci->db->last_query();

    }
    /**
     * 取得該頁面的datatable
     */
    public function getSecDataTable()
    {
        //可以被排序的欄位
        $order = array(null, null, null);
        //可以被搜索的欄位
        $like = array(null, null, null);
        $answer1 = '[extra]';
        $answer2 = '[extra]';
        $answer3 = '[extra]';
        $answer4 = '[extra]';
        $answer5 = '[extra]';

        $extra = array('aa_answer1', 'aa_answer2', 'aa_answer3', 'aa_answer4', 'aa_answer5');
        $judge = function ($value, $case) {
            for ($i = 0; $i < 5; $i++) {
                if ($case != Null) {
                    // array_push($output, '$answer' . $i);
                    return $value."顆星";
                } else {
                    return '無';
                }
            }
        };
        $output = array($answer1, $answer2, $answer3, $answer4, $answer5);

        //輸出的順序
        // $output = array($answer1, $answer2, $answer3, $answer4, $answer5);
        //資料庫設定
        $this->light_datatables->ci->db->select('`admin_answer`.`aa_key` as `aa_key`,
                                                 `admin_answer`.`aq_key` as `aq_key`,
                                                 `admin_answer`.`order_key` as `order_key`,
                                                 `admin_answer`.`aa_answer1` as `aa_answer1`,
                                                 `admin_answer`.`aa_answer2` as `aa_answer2`,
                                                 `admin_answer`.`aa_answer3` as `aa_answer3`,
                                                 `admin_answer`.`aa_answer4` as `aa_answer4`,
                                                 `admin_answer`.`aa_answer5` as `aa_answer5`');
        $this->light_datatables->ci->db->from('admin_answer');
        $this->light_datatables->ci->db->where('`order`.`shop_key`', $_SESSION['shop_key']);
        // $this->light_datatables->ci->db->join('admin_question', '`admin_answer`.`aq_key` = `admin_question`.`aq_key`');
        $this->light_datatables->ci->db->join('order', '`order`.`order_key` = `admin_answer`.`order_key`');
        //輸出整合
        $this->light_datatables->set_querycolumn($order, $like);
        $this->light_datatables->order_by('`admin_answer`.`aa_key`', 'DESC');
        $this->light_datatables->set_output($output,$extra, $judge, true);

        //輸出報表
        echo $this->light_datatables->get_datatable();
        // print_r($output);
        // echo $this->light_datatables->ci->db->last_query();

    }


}
