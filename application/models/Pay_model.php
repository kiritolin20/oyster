<?php

class Pay_model extends DataBaseError
{

    //DB操作手冊
    //https://codeigniter.org.tw/user_guide/database/active_record.html
    public function __destruct()
    {
        $this->db->close();
    }



    /***
     * 方法功能：從資料庫取得要更改的資料
     * 有查詢到資料就回傳資料,沒有就回傳false
     * @param  String $key
     * @return Object | bool
     ***/
    function getModifyData($key, $info_key)
    {
        $this->db->select('sha1(`product`.`product_key`) as `product_key`,
                            `product.product_name` as product_name,
                            `product_info.product_info_name` as product_info_name,
                            `product.product_introduct` as product_introduce,
                            `product.product_time` as product_time,
                            `product.product_display` as product_display,
                            `product.shop_key`,
                            `product_info_amount` as product_amount,
                            `product_info_price` as product_price,
                            `product_info_display` as product_info_display,
                            sha1(`product_info`.`product_info_key`) as product_info_key,
                            `product_info_img` as product_info_img');
        $this->db->from('product');
        $this->db->where('sha1(product.product_key)', $key);
        $this->db->where('sha1(product_info_key)', $info_key);
        $this->db->join('product_info', 'product_info.product_key=product.product_key');
        if ($result = $this->dbGetCatch()) {
            return $result;
        } else {
            return false;
        }
    }
    /***
     * 方法功能：從資料庫刪除資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  String $key,String $datatable,String $controller
     * @return bool
     ***/
    function deleteData($key)
    {
        $this->db->set('order_display', '0');
        $this->db->where('order_key', $key);
        $this->db->update('order');
        return true;
    }
    /***
     * 方法功能：從資料庫更改資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  String $key,String $datatable,String $controller
     * @return bool
     ***/
    function modifyData($data)
    {
        for ($i = 0; $i < count($data["keyArr"]); $i++) {
            $this->db->set('order_history_amount', $data['amountArr'][$i]);
            $this->db->set('order_history_price', $data['priceArr'][$i]);
            $this->db->where('order_history_key', $data['keyArr'][$i]);
            $this->db->update('order_history');
            $this->db->flush_cache();
        }
        return true;
    }
    /***
     * 方法功能：從資料庫更改資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  String $key,String $datatable,String $controller
     * @return bool
     ***/
    function pay($data)
    {
        $this->db->set('order_admin_pay_time', date("Y-m-d  H:i:s"));
        $this->db->where('order_key', $data);
        $this->db->update('order');
        return true;
    }
    /***
     * 方法功能：從資料庫更改資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  String $key,String $datatable,String $controller
     * @return bool
     ***/
    function selectPay($data)
    {
        for ($i=0; $i < count($data) ; $i++) {
            $this->db->set('order_admin_pay_time', date("Y-m-d  H:i:s"));
            $this->db->where('order_key', $data[$i]);
            $this->db->update('order');
            $this->db->flush_cache();
        }
        return true;
        
    }
    /***
     * 方法功能：從資料庫取訂單資料
     * @param  Object data
     * @return Object
     ***/
    function checkOrder($data)
    {
        $order = array();
        $this->db->select('(`order_history`.`order_history_key`) as `history_key`,
                            `order_history.product_key` as `product_key`,
                            `order_history.order_history_price` as `price`,
                            `order_history.order_history_amount` as `amount`,
                            `order_history.order_history_product_name` as `name`,
                            `order_history.order_history_product_unit` as `unit`,
                            `order_history.order_history_product_img` as `img`,
                            `order.order_key` as `order_key`,
                            `order.order_judge_type` as `type`,
                            `order.order_user_pay_time` as `user_pay_time`,
                            `order.order_admin_pay_time` as `admin_pay_time`,
                            `order.order_shipped_time` as `shipped_time`');
        $this->db->from('order');
        $this->db->where('order.order_key', $data);
        $this->db->join('order_history', 'order.order_key=order_history.order_key');
        if ($result = $this->dbGetCatch()) {
            $order[0] = $result;
        } else {
            $order[0] = false;
        }
        $this->db->flush_cache();
        $price = '( SELECT SUM(`order_history`.`order_history_price`)
                    FROM order_history
                    WHERE `order_history`.`order_key` = `order`.`order_key`
                    ) AS price';
        $amount = '( SELECT SUM(`order_history`.`order_history_amount`)
                    FROM order_history
                    WHERE `order_history`.`order_key` = `order`.`order_key`
                    ) AS amount';
        $this->db->select('(`order_history`.`order_history_key`) as `history_key`,
                            ' . $price . ',
                            ' . $amount . ',
                            `order_history.order_history_product_name` as `name`,
                            `order_history.order_history_product_unit` as `unit`,
                            `order_history.order_history_product_img` as `img`,
                            `order.order_key` as `order_key`,
                            `order.order_time` as `order_time`,
                            `order.order_judge_type` as `type`,
                            `order.order_user_pay_time` as `user_pay_time`,
                            `order.order_admin_pay_time` as `pay_time`,
                            `order.order_display` as `display`,
                            `user_name` as `user_name`,
                            `user_address` as `user_address`,
                            `user_area` as `user_area`,
                            `user_priceType` as `user_priceType`,
                            `user_phone` as `user_phone`,
                            `user_encryption` as `user_encryption`,
                            `user_member` as `user_member`,
                            `user_shop_phone` as `user_shop_phone`,
                            `user_shop_address` as `user_shop_address`,
                            `order.order_shipped_time` as `shipped_time`');
        $this->db->from('order');
        $this->db->where('order.order_key', $data);
        $this->db->join('order_history', 'order.order_key=order_history.order_key');
        $this->db->join('user', 'order.user_key=user.user_key');

        if ($result2 = $this->dbGetCatch()) {
            $order[1] = $result2;
            return $order;
        } else {
            return false;
        }
    }
    /***
     * 方法功能：從資料庫刪除資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  Object data
     * @return bool
     ***/
    function getLineKey($key)
    {
        $this->db->select('`order`.`order_key` as `order_key`,
                           `order`.`user_key` as `user_key`,
                           `user`.`user_line_key` as `line_key`');
        $this->db->from('order');
        $this->db->join('user', '`order`.`user_key`=`user`.`user_key`');
        $this->db->where('order_key', $key);
        if ($result = $this->dbGetCatch()) {
            return $result;
        } else {
            return false;
        }
    }

}
