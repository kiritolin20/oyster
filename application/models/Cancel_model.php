<?php

class Cancel_model extends DataBaseError
{

    //DB操作手冊
    //https://codeigniter.org.tw/user_guide/database/active_record.html
    public function __destruct()
    {
        $this->db->close();
    }


    function insertProduct($data)
    {
        $productData = array(
            'product_name' => $data['product_name'],
            'product_introduct' => $data['product_introduce'],
            'product_display' => $data['product_display'],
            'product_time' => date("Y- m - d  H : i : s"),
            'shop_key' => $_SESSION['shop_key']
        );
        $this->db->insert('product', $productData);
        $key = $this->db->insert_id();

        $this->db->flush_cache();

        $infoData = array(
            'product_info_amount' => $data['product_amount'],
            'product_info_display' => $data['product_info_display'],
            'product_info_name' => $data['product_info_name'],
            'product_info_time' => date("Y- m - d  H : i : s"),
            'product_info_price' => $data['product_price'],
            'product_key' => $key
        );
        $this->db->insert('product_info', $infoData);
        $infoKey = $this->db->insert_id();

        return sha1($infoKey);
    }

    /***
     * 方法功能：從資料庫取得要更改的資料
     * 有查詢到資料就回傳資料,沒有就回傳false
     * @param  String $key
     * @return Object | bool
     ***/
    function getModifyData($key, $info_key)
    {
        $this->db->select('sha1(`product`.`product_key`) as `product_key`,
                            `product.product_name` as product_name,
                            `product_info.product_info_name` as product_info_name,
                            `product.product_introduct` as product_introduce,
                            `product.product_time` as product_time,
                            `product.product_display` as product_display,
                            `product.shop_key`,
                            `product_info_amount` as product_amount,
                            `product_info_price` as product_price,
                            `product_info_display` as product_info_display,
                            sha1(`product_info`.`product_info_key`) as product_info_key,
                            `product_info_img` as product_info_img');
        $this->db->from('product');
        $this->db->where('sha1(product.product_key)', $key);
        $this->db->where('sha1(product_info_key)', $info_key);
        $this->db->join('product_info', 'product_info.product_key=product.product_key');
        if ($result = $this->dbGetCatch()) {
            return $result;
        } else {
            return false;
        }
    }
    /***
     * 方法功能：從資料庫刪除資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  String $key,String $datatable,String $controller
     * @return bool
     ***/
    function deleteData($key)
    {
        $this->db->set('order_display', '0');
        $this->db->where('order_key', $key);
        $this->db->update('order');
        return true;
    }
    /***
     * 方法功能：從資料庫更改資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  String $key,String $datatable,String $controller
     * @return bool
     ***/
    function modifyData($data)
    {
        $this->db->set('product_name', $data['product_name']);
        $this->db->set('product_introduct', $data['product_introduce']);
        $this->db->set('product_display', $data['product_display']);
        $this->db->where('sha1(`product_key`)', $data['product_key']);
        $this->db->update('product');
        $this->db->flush_cache();
        $this->db->set('product_info_amount', $data['product_amount']);
        $this->db->set('product_info_display', $data['product_info_display']);
        $this->db->set('product_info_name', $data['product_info_name']);
        $this->db->set('product_info_price', $data['product_price']);
        $this->db->where('sha1(`product_info_key`)', $data['product_info_key']);
        $this->db->update('product_info');
        return true;
    }
    /***
     * 方法功能：從資料庫刪除資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  Object data
     * @return bool
     ***/
    function modifyInfoImg($data)
    {
        $this->db->set('product_info_img', $data['img']);
        $this->db->where('sha1(`product_info_key`)', $data['key']);
        $this->db->update('product_info');
        return true;
    }
    /***
     * 方法功能：從資料庫找出商品其他照片
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  Object data
     * @return bool
     ***/
    function getModifyImg($key)
    {
        $this->db->select('`product_img_src` as `name`,`product_img_key` as `key`,sha1(`product_info_key`) as `info_key`');
        $this->db->from('product_img');
        $this->db->where('sha1(product_info_key)', $key);
        if ($result = $this->dbGetCatch()) {
            return $result;
        } else {
            return false;
        }
    }
    /***
     * 方法功能：從資料庫更新商品圖片
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  Object data
     * @return bool
     ***/
    function modifyImg($key)
    {
        $this->db->select('`product_img_src` as `name`,`product_img_key` as `key`');
        $this->db->from('product_img');
        $this->db->where('sha1(product_info_key)', $key);
        if ($result = $this->dbGetCatch()) {
            return $result;
        } else {
            return false;
        }
    }
    /***
     * 方法功能：從資料庫刪除資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  Object data
     * @return bool
     ***/
    function accessCancel($data)
    {
        $this->db->set('order_judge_type', '3');
        $this->db->where('`order_key`', $data);
        $this->db->update('order');
        return true;
    }
    /***
     * 方法功能：從資料庫刪除資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  Object data
     * @return bool
     ***/
    function getLineKey($key)
    {
        $this->db->select('`order`.`order_key` as `order_key`,
                           `order`.`user_key` as `user_key`,
                           `user`.`user_line_key` as `line_key`');
        $this->db->from('order');
        $this->db->join('user', '`order`.`user_key`=`user`.`user_key`');
        $this->db->where('order_key', $key);
        if ($result = $this->dbGetCatch()) {
            return $result;
        } else {
            return false;
        }
    }
    /***
     * 方法功能：從資料庫取訂單資料
     * @param  Object data
     * @return Object
     ***/
    function checkOrder($data)
    {
        $order = array();
        $this->db->select('(`order_history`.`order_history_key`) as `history_key`,
                            `order_history.product_key` as `product_key`,
                            `order_history.order_history_price` as `price`,
                            `order_history.order_history_amount` as `amount`,
                            `order_history.order_history_product_name` as `name`,
                            `order_history.order_history_product_unit` as `unit`,
                            `order_history.order_history_product_img` as `img`,
                            `order.order_key` as `order_key`,
                            `order.shop_key` as `shop_key`,
                            `order.order_time` as `order_time`,
                            `order.order_judge_type` as `type`,
                            `order.order_judge_time` as `judge_time`,
                            `order.order_pay_time` as `pay_time`,
                            `order.order_shipped_time` as `shipped_time`');
        $this->db->from('order');
        $this->db->where('order.order_key', $data);
        $this->db->join('order_history', 'order.order_key=order_history.order_key');
        if ($result = $this->dbGetCatch()) {
            $order[0] = $result;
        } else {
            $order[0] = false;
        }
        $this->db->flush_cache();
        $price = '( SELECT SUM(`order_history`.`order_history_price`)
                    FROM order_history
                    WHERE `order_history`.`order_key` = `order`.`order_key`
                    ) AS price';
        $amount = '( SELECT SUM(`order_history`.`order_history_amount`)
                    FROM order_history
                    WHERE `order_history`.`order_key` = `order`.`order_key`
                    ) AS amount';
        $this->db->select('(`order_history`.`order_history_key`) as `history_key`,
                            ' . $price . ',
                            ' . $amount . ',
                            `order_history.order_history_product_name` as `name`,
                            `order_history.order_history_product_unit` as `unit`,
                            `order_history.order_history_product_img` as `img`,
                            `order_history.product_key` as `product_key`,
                            `order.order_key` as `order_key`,
                            `order.user_key` as `user_key`,
                            `order.order_time` as `order_time`,
                            `order.order_judge_type` as `type`,
                            `order.order_judge_time` as `judge_time`,
                            `order.order_pay_time` as `pay_time`,
                            `order.order_display` as `display`,
                            `order.order_address` as `order_address`,
                            `order.order_phone` as `order_phone`,
                            `order.order_paidpay` as `order_paidpay`,
                            `order.order_shipped_time` as `shipped_time`');
        $this->db->from('order');
        $this->db->where('order.shop_key', $_SESSION['shop_key']);
        $this->db->where('order.order_key', $data);
        $this->db->join('order_history', 'order.order_key=order_history.order_key');
        if ($result2 = $this->dbGetCatch()) {
            $order[1] = $result2;
            return $order;
        } else {
            return false;
        }
    }
}
