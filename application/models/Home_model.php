<?php

class Home_model extends DataBaseError
{

    //DB操作手冊
    //https://codeigniter.org.tw/user_guide/database/active_record.html
    public function __destruct()
    {
        $this->db->close();
    }

    /**
     * 取得管理員Email
     *
     * @return void
     */
    function getAdminEmail(){
        $this->db->select('admin.`admin_email`');
        $this->db->from('admin');
        if ($result = $this->dbGetCatch()) {
            return $result->result_array();
        } else {
            return false;
        }
    }
    /***
     * 方法功能：從資料庫刪除資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  String $key,String $datatable,String $controller
     * @return bool
     ***/
    function deleteData($key)
    {
        $this->db->where('SHA1(`admin`.`admin_key`)', $key);
        $this->db->delete('admin');
        return true;
    }
}
