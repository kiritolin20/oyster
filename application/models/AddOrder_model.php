<?php

class AddOrder_model extends DataBaseError
{

    //DB操作手冊
    //https://codeigniter.org.tw/user_guide/database/active_record.html
    public function __destruct()
    {
        $this->db->close();
    }

    function getUserDataModel($area = null){
        if (!empty($area)) {
            if($area == 'e'){
                $area = '東';
            }else if($area == 's'){
                $area = '南';
            }else if ($area == 'w') {
                $area = '西';
            }else if ($area == 'n') {
                $area = '北';
            }
            $this->db->select('SHA1(CONCAT("oyster_", `user`.`user_key`)) as `hash_key`, user_name');
            $this->db->from('user');
            $this->db->where('user_area', $area);
            if ($result = $this->dbGetCatch()) {
                return $result->result_array();
            } else {
                return false;
            }
        } else {
            $this->db->select('SHA1(CONCAT("oyster_", `user`.`user_key`)) as `hash_key`, user_name');
            $this->db->from('user');
            if ($result = $this->dbGetCatch()) {
                return $result->result_array();
            } else {
                return false;
            }
        }
    }

    function getPrductDataModel(){
        $price = '( SELECT price_history.ph_price
                    FROM price_history
                    WHERE `price_history`.product_key = `product`.product_key
                    ORDER BY price_history.ph_time DESC
                    LIMIT 1)';
        $this->db->select('sha1(`product`.`product_key`) as `key`,
                                                `product.product_name` as name,
                                                `product.product_info` as info,
                                                `product.product_display` as display,
                                                '. $price. ' as price,
                                                `product_unit` as unit,
                                                `product_img` as img');
        $this->db->from('product');
        if ($result = $this->dbGetCatch()) {
            return $result->result_array();
        } else {
            return false;
        }
    }


    function getUserfavoriteModel($id){
        $price = '( SELECT price_history.ph_price
                    FROM price_history
                    WHERE `price_history`.product_key = `product`.product_key
                    ORDER BY price_history.ph_time DESC
                    LIMIT 1)';
        $this->db->select('sha1(`product`.`product_key`) as `key`,
                                                `product.product_name` as name,
                                                `product.product_info` as info,
                                                `product.product_display` as display,
                                                '. $price. ' as price,
                                                `product_unit` as unit,
                                                `product_img` as img');
        $this->db->from('product');
        $this->db->join('favorite','`product`.`product_key` = `favorite`.`product_key`');
        $this->db->join('user','`user`.`user_key` = `favorite`.`user_key`');
        $this->db->where('SHA1(CONCAT("oyster_", `user`.`user_key`)) =', $id);
        if ($result = $this->dbGetCatch()) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    private function getUserKey($key){
        $this->db->select('user_key');
        $this->db->from('user');
        $this->db->where('SHA1(CONCAT("oyster_", `user`.`user_key`)) =', $key);
        if ($result = $this->dbGetCatch()) {
            return $result->row()->user_key;
        } else {
            return false;
        }
    }

    private function getPrductKey($key){
        $this->db->select('product_key');
        $this->db->from('product');
        $this->db->where('sha1(`product`.`product_key`) = ', $key);
        if ($result = $this->dbGetCatch()) {
            return $result->row()->product_key;
        } else {
            return false;
        }
    }
    /**
     * [設定訂單]
     *
     * @param [type] $data
     * @return boolean
     */
    public function setOrderModel($data){
        $this->db->set("order_time", date('Y-m-d H:i:s',time()));
        $this->db->set("user_key", $this->getUserKey($data[0]["user"]));
        $this->db->set("order_judge_type", 1);
        $this->db->set("order_display", 1);
        $order_id = $this->dbInsertCatch("order");

        for ($i = 1; $i < count($data); $i++) {
            $result = $this->startTransaction([
                $this->getActivityQueryOrder($order_id, $data[$i])
            ]);
            if(!$result){
                return false;
            }
        }
        return true;
    }
    /**
     * [歷史訂單 串接sql 做事務]
     *
     * @param [type] $productData
     * @param [type] $order_id
     * @param [type] $product_id
     * @return array
     */
    private function getActivityQueryOrder($order_id, $data){
        $activityFiled = "(order_history_price, order_history_amount, order_history_product_name, order_history_product_unit, order_history_product_img, order_key, product_key)";
        $activityValue = "(?, ?, ?, ?, ?, ?, ?)";
        $activityData = [
            (((int)$data["price"]) * ((int)$data["amount"])),
            $data["amount"],
            $data["name"],
            $data["unit"],
            $data["img"],
            $order_id,
            $this->getPrductKey($data["prdouct"])
        ];
        return[
            "sql"  => "INSERT INTO order_history {$activityFiled} VALUES {$activityValue};",
            "data" => $activityData
        ];
    }
}
