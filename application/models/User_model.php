<?php

class User_model extends DataBaseError
{

    //DB操作手冊
    //https://codeigniter.org.tw/user_guide/database/active_record.html
    public function __destruct()
    {
        $this->db->close();
    }
    function shortencrypt($str)
    {
        $code = floatval(sprintf('%u', crc32($str)));

        $sstr = '';

        while ($code) {
            $mod = fmod($code, 62);
            if ($mod > 9 && $mod <= 35) {
                $mod = chr($mod + 55);
            } elseif ($mod > 35) {
                $mod = chr($mod + 61);
            }
            $sstr .= $mod;
            $code = floor($code / 62);
        }
        return $sstr;
    }


    function insertProduct($data)
    {
        if($data['user_area'] == 'e'){
            $data['user_area'] = '東';
        }else if($data['user_area'] == 's'){
            $data['user_area'] = '南';
        }else if ($data['user_area'] == 'w') {
            $data['user_area'] = '西';
        }else if ($data['user_area'] == 'n') {
            $data['user_area'] = '北';
        };
        if($data['user_priceType'] == 'month'){
            $data['user_priceType'] = '月結';
        }else if($data['user_priceType'] == 'week'){
            $data['user_priceType'] = '週結';
        }else if($data['user_priceType'] == 'cash'){
            $data['user_priceType'] = '現金結';
        }
        $productData = array(
            'user_name' => $data['user_name'],
            'user_address' => $data['user_address'],
            'user_phone' => $data['user_phone'],
            'user_area' => $data['user_area'],
            'user_priceType' => $data['user_priceType'],
            'user_member' => $data['user_member'],
            'user_shop_address' => $data['user_shop_address'],
            'user_shop_phone' => $data['user_shop_phone'],
        );
        $this->db->insert('user', $productData);
        $key = $this->db->insert_id();
        $this->db->flush_cache();
        $encry = $this->shortencrypt($key);
        $this->db->set('user_encryption', $encry);
        $this->db->where('user_key',$key);
        $this->db->update('user');
        return $encry;
    }

    /***
     * 方法功能：從資料庫取得要更改的資料
     * 有查詢到資料就回傳資料,沒有就回傳false
     * @param  String $key
     * @return Object | bool
     ***/
    function getModifyData($key)
    {
        $this->db->select('sha1(`user_key`) as `user_key`,
                            `user_name`,
                            `user_address`,
                            `user_area`,
                            `user_priceType`,
                            `user_phone`,
                            `user_encryption`,
                            `user_shop_address`,
                            `user_shop_phone`,
                            `user_member`');
        $this->db->from('user');
        $this->db->where('sha1(`user_key`)', $key);
        if ($result = $this->dbGetCatch()) {
            if($result->row()->user_area == "東"){
                $result->row()->user_area = "e";
            }else if($result->row()->user_area == "南"){
                $result->row()->user_area = "s";
            }else if ($result->row()->user_area == "西") {
                $result->row()->user_area = "w";
            }else if ($result->row()->user_area == "北") {
                $result->row()->user_area = "n";
            }
            if ($result->row()->user_priceType == "月結") {
                $result->row()->user_priceType = "month";
            } else if ($result->row()->user_priceType == "週結") {
                $result->row()->user_priceType = "week";
            } else if ($result->row()->user_priceType == "現金結") {
                $result->row()->user_priceType = "cash";
            }
            return $result;
        } else {
            return false;
        }
    }
    /***
     * 方法功能：從資料庫刪除資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  String $key,String $datatable,String $controller
     * @return bool
     ***/
    function deleteData($key)
    {
        $this->db->where('SHA1(`user`.`user_key`)', $key);
        $this->db->delete('user');
        return true;
    }
    /***
     * 方法功能：從資料庫刪除資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  String $key,String $datatable,String $controller
     * @return bool
     ***/
    function modifyData($data)
    {
        if ($data['user_area'] == 'e') {
            $data['user_area'] = '東';
        } else if ($data['user_area'] == 's') {
            $data['user_area'] = '南';
        } else if ($data['user_area'] == 'w') {
            $data['user_area'] = '西';
        } else if ($data['user_area'] == 'n') {
            $data['user_area'] = '北';
        };
        if ($data['user_priceType'] == 'month') {
            $data['user_priceType'] = '月結';
        } else if ($data['user_priceType'] == 'week') {
            $data['user_priceType'] = '週結';
        } else if ($data['user_priceType'] == 'cash') {
            $data['user_priceType'] = '現金結';
        }
        $this->db->set('user_name', $data['user_name']);
        $this->db->set('user_address', $data['user_address']);
        $this->db->set('user_phone', $data['user_phone']);
        $this->db->set('user_area', $data['user_area']);
        $this->db->set('user_priceType', $data['user_priceType']);
        $this->db->set('user_member', $data['user_member']);
        $this->db->set('user_shop_address', $data['user_shop_address']);
        $this->db->set('user_shop_phone', $data['user_shop_phone']);
        $this->db->where('sha1(`user_key`)', $data['user_key']);
        $this->db->update('user');
        return true;
    }

    public function getPrice($encryption,$month,$weekArray)
    {
        $price = array();
        $this->db->select('SUM(`order_history_price`) as monthPrice,`user`.`user_key` as `user_key`');
        $this->db->from('user');
        $this->db->join('order', '`order`.`user_key` = `user`.`user_key`');
        $this->db->join('order_history','`order`.`order_key` = `order_history`.`order_key`');
        $this->db->where('MONTH(`order`.`order_time`) = ' . $month . '');
        $this->db->where('`order_display` = 1');
        $this->db->where('`user_encryption` = '.$encryption.'');
        if ($result = $this->dbGetCatch()) {
            array_push($price, $result->row()->monthPrice);
        } else {
            return false;
        }

        $this->db->flush_cache();
        
        $this->db->select('SUM(`order_history_price`) as weekPrice,`user`.`user_key` as `user_key`');
        $this->db->from('user');
        $this->db->join('order', '`order`.`user_key` = `user`.`user_key`');
        $this->db->join('order_history', '`order`.`order_key` = `order_history`.`order_key`');
        $this->db->where('(`order`.`order_time`) >= "' . $weekArray[0] . '"');
        $this->db->where('(`order`.`order_time`) <= "' . $weekArray[1] . '"');
        $this->db->where('`order_display` = 1');
        $this->db->where('`user_encryption` = ' . $encryption . '');
        if ($result = $this->dbGetCatch()) {
            array_push($price, $result->row()->weekPrice);
            return $price;
        } else {
            return false;
        }
    }

    public function modifyFavorite($data)
    {
        $this->db->where('`user_key`', $data['user_key']);
        $this->db->delete('favorite');
        $this->db->flush_cache();
        for ($i = 0; $i < count($data['product_key']); $i++) {
            $productData = array(
                'product_key' => $data['product_key'][$i],
                'user_key' => $data['user_key']
            );
            $this->db->insert('favorite', $productData);
            $this->db->insert();
            $this->db->flush_cache();
        }
        return true;
    }

    public function getFavorite($data)
    {
        $this->db->select('`product_key`');
        $this->db->where('user_key',$data['user_key']);
        $this->db->from('favorite');
        if ($result = $this->dbGetCatch()) {
            return $result;
        } else {
            return false;
        }
    }
}
