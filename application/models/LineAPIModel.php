<?php

class LineAPIModel extends CI_Model{
	function login($user_encryption){
		$this->db->select("*");
		$this->db->from("user");
		$this->db->where("user_encryption",$user_encryption);
		return $this->db->get()->result();
	}

	function addUserLineKey($user_key,$user_line_key){
		$data = array(
			'user_line_key' => $user_line_key
		);
		$this->db->where("user_key",$user_key);
		$this->db->update('user',$data);
	}

	function getProductInfo(){
		return $this->db->query("
		 	SELECT `product`.`product_key` , `product`.`product_name`,`product`.`product_img`,`price_history`.`ph_price` as `product_price`,`product`.`product_unit`
		 	FROM `product` 
		 	JOIN `price_history` ON `product`.`product_key` = `price_history`.`product_key` 
		 	WHERE `ph_key` = 
				(SELECT `ph_key`
		    	FROM `price_history`
			    WHERE `price_history`.`product_key` = `product`.`product_key`
				order by `ph_time` DESC
				limit 1)")->result();
			// AND `product`.`product_display` = "1";
		// return $this->db;
	}

	function addCartKG($user_key,$product_key,$product_price,$cart_amount){
		$data = array(
			'user_key' => $user_key,
			'product_key' => $product_key,
			'cart_amount' => $cart_amount,
			'cart_price' => $cart_amount*$product_price
		);
		return $this->db->insert('cart',$data);
	}

	function addCartPrice($user_key,$product_key,$product_price,$cart_price){
		$data = array(
			'user_key' => $user_key,
			'product_key' => $product_key,
			'cart_amount' => $cart_price/$product_price,
			'cart_price' => $cart_price
		);
		return $this->db->insert('cart',$data);
	}

	function getUserCart($user_key){
		return $this->db->query("
		 	SELECT `product`.`product_key`,`product_name`,`product_img`,`ph_price` as `product_price`,`product`.`product_unit`,`cart_amount`,`cart_price`
		 	FROM `cart` 
		 	JOIN `product` ON `cart`.`product_key` = `product`.`product_key`
		 	JOIN `price_history` ON `cart`.`product_key` = `price_history`.`product_key` 
		 	WHERE `ph_key` = 
				(SELECT `ph_key`
		    	FROM `price_history`
			    WHERE `price_history`.`product_key` = `cart`.`product_key`
				order by `ph_time` DESC
				limit 1)
			AND `user_key` = ".$user_key)->result();
	}

	function deleteUserCart($productKey,$user_key){
		$this->db->where("product_key",$productKey);
		$this->db->where("user_key",$user_key);
		return $this->db->delete("cart");
	}

	function addOrder($user_key,$order_time){
		$data = array(
			'order_key' => "",
			'user_key' => $user_key,
			'order_time' => $order_time,
			'order_judge_type' => "1",
			'order_display' => "1"
		);
		return $this->db->insert('order',$data);
	}

	function addOrderHistory($product_price,$cart_amount,$product_name,$product_unit,$product_img,$order_key,$product_key){
		$data = array(
			'order_history_key' => "",
			'order_history_price' => $product_price,
			'order_history_amount' => $cart_amount,
			'order_history_product_name' => $product_name,
			'order_history_product_unit' => $product_unit,
			'order_history_product_img' => $product_img,
			'order_key' => $order_key,
			'product_key' => $product_key
		);
		return $this->db->insert('order_history',$data);
	}

	function clearCart($user_key){
		$this->db->where("user_key",$user_key);
		return $this->db->delete("cart");
	}

	function getOrderTotal($user_key,$year,$month,$day){
		$this->db->select("*");
		$this->db->from("order");
		$this->db->where("user_key",$user_key);
		$this->db->where("year(order_time)",$year);
		$this->db->where("month(order_time)",$month);
		// $this->db->where("day(order_time)",$day);
		return $this->db->get()->result();
	}

	function getAdminEmail(){
        $this->db->select('admin.`admin_email`');
        $this->db->from('admin');
        if ($result = $this->dbGetCatch()) {
            return $result->result_array();
        } else {
            return false;
        }
    }
}