<?php

class Account_model extends DataBaseError
{

    //DB操作手冊
    //https://codeigniter.org.tw/user_guide/database/active_record.html
    public function __destruct()
    {
        $this->db->close();
    }


    function insertAdmin($account, $password, $email)
    {
        $data = array(
            'admin_account' => $account,
            'admin_password' => md5($password),
            'admin_email' => $email,
            'admin_type' => '1',
            'shop_key' => '1'
        );
        $this->db->insert('admin', $data);
    }

    /***
     * 方法功能：從資料庫取得要更改的資料
     * 有查詢到資料就回傳資料,沒有就回傳false
     * @param  String $key
     * @return Object | bool
     ***/
    function getModifyData($key)
    {
        $this->db->select('admin.`admin_account`,
                           admin.`admin_email`,
                           SHA1(`admin`.`admin_key`) as `admin_key`');
        $this->db->from('admin');
        $this->db->where('sha1(`admin_key`)', $key);
        if ($result = $this->dbGetCatch()) {
            return $result;
        } else {
            return false;
        }
    }
    /***
     * 方法功能：從資料庫刪除資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  String $key,String $datatable,String $controller
     * @return bool
     ***/
    function deleteData($key)
    {
        $this->db->where('SHA1(`admin`.`admin_key`)', $key);
        $this->db->delete('admin');
        return true;
    }
    /***
     * 方法功能：從資料庫刪除資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  String $key,String $datatable,String $controller
     * @return bool
     ***/
    function modifyData($data)
    {
        $this->db->set('admin_account', $data['admin_account']);
        $this->db->set('admin_email', $data['admin_email']);
        if(!$data['judge']){
            $this->db->set('admin_password', md5($data['admin_password']));
        }
        $this->db->where('sha1(`admin_key`)', $data['admin_key']);
        $this->db->update('admin');
        return true;
    }
}
