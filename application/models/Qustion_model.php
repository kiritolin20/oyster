<?php

class Qustion_model extends DataBaseError
{

    //DB操作手冊
    //https://codeigniter.org.tw/user_guide/database/active_record.html
    public function __destruct()
    {
        $this->db->close();
    }


    function insertProduct($data)
    {
        if (isset($data['lineKey'])) {
            $this->db->select('`admin`.`line_key` as line_key,
                               `admin`.`shop_key` as shop_key');
            $this->db->from('admin');
            $this->db->where('`admin`.`line_key`', $data['lineKey']);
            if ($result = $this->dbGetCatch()) {
            } else {
                return false;
            }
            $this->db->flush_cache();
            $productData = array(
                'product_name' => $data['product_name'],
                'product_introduct' => $data['product_introduct'],
                'product_display' => $data['product_display'],
                'product_amount' => $data['product_amount'],
                'product_price' => $data['product_price'],
                'product_unit' => $data['product_unit'],
                'product_time' => date("Y-m-d  H:i:s"),
                'shop_key' => $result->row()->shop_key
            );
        } else {
            $productData = array(
                'product_name' => $data['product_name'],
                'product_introduct' => $data['product_introduct'],
                'product_display' => $data['product_display'],
                'product_amount' => $data['product_amount'],
                'product_price' => $data['product_price'],
                'product_unit' => $data['product_unit'],
                'product_time' => date("Y-m-d  H:i:s"),
                'shop_key' => $_SESSION['shop_key']
            );
        }

        $this->db->insert('product', $productData);
        $key = $this->db->insert_id();

        return sha1($key);
    }

    /***
     * 方法功能：從資料庫取得要更改的資料
     * 有查詢到資料就回傳資料,沒有就回傳false
     * @param  String $key
     * @return Object | bool
     ***/
    function getModifyData($key)
    {
        $this->db->select('sha1(`product`.`product_key`) as `product_key`,
                            `product.product_name` as product_name,
                            `product.product_time` as product_time,
                            `product.product_introduct` as product_introduct,
                            `product.product_display` as product_display,
                            `product_amount` as product_amount,
                            `product_price` as product_price,
                            `product_unit` as product_unit,
                            `product_img` as product_img,
                            `product.shop_key`');
        $this->db->from('product');
        $this->db->where('sha1(product.product_key)', $key);
        if ($result = $this->dbGetCatch()) {
            return $result;
        } else {
            return false;
        }
    }
    /***
     * 方法功能：從資料庫刪除資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  String $key,String $datatable,String $controller
     * @return bool
     ***/
    function deleteData($key)
    {
        $this->db->where('sha1(`product_key`)', $key);
        $this->db->delete('product_img');
        $this->db->flush_cache();
        $this->db->where('sha1(`product_key`)', $key);
        $this->db->delete('product');
        return true;
    }
    /***
     * 方法功能：從資料庫更改資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  String $key,String $datatable,String $controller
     * @return bool
     ***/
    function modifyQuestion($data)
    {
        if($data['judge'] == "admin"){
            $this->db->empty_table('admin_question');
            $this->db->flush_cache();
            for ($i = 1; $i <= 5; $i++) {
                if (isset($data['question' . $i])) {
                    $this->db->set("aq_question$i", $data['question' . $i]);
                }
            }
            $this->db->insert('admin_question');
            $key = $this->db->insert_id();
            $this->db->flush_cache();
            $this->db->set("aq_key",$key);
            $this->db->where("aq_key", $key-1);
            $this->db->update("admin_answer");
            // return $this->db->last_query();
        }else {
            $this->db->where("shop_key ",$_SESSION['shop_key']);
            $this->db->delete("shop_question");
            $this->db->flush_cache();
            for ($i = 1; $i <= 5; $i++) {
                if (isset($data['question' . $i])) {
                    $this->db->set("sq_question_$i", $data['question' . $i]);
                }
            }
            $this->db->set("shop_key", $_SESSION['shop_key']);
            $this->db->insert('shop_question');
            $key = $this->db->insert_id();
            $this->db->flush_cache();
            $this->db->set("sq_key", $key);
            $this->db->where("sq_key", $key - 1);
            $this->db->update("shop_answer");
            // return $this->db->last_query();
        }
        
        return true;
    }

    /***
     * 方法功能：從資料庫找出商品其他照片
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  Object data
     * @return bool
     ***/
    function getQuestion()
    {
        $this->db->select('`aq_question1` as `question1`,`aq_question2` as `question2`,`aq_question3` as `question3`,`aq_question4` as `question4`,`aq_question5` as `question5`,sha1(`aq_key`) as `key`');
        $this->db->from('admin_question');
        if ($result = $this->dbGetCatch()) {
            return $result;
        } else {
            return false;
        }
    }
    /***
     * 方法功能：從資料庫找出商品其他照片
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  Object data
     * @return bool
     ***/
    function getSellerQuestion()
    {
        $this->db->select('`sq_question_1` as `question1`,`sq_question_2` as `question2`,`sq_question_3` as `question3`,`sq_question_4` as `question4`,`sq_question_5` as `question5`,sha1(`sq_key`) as `key`');
        $this->db->where("shop_key", $_SESSION['shop_key']);

        $this->db->from('shop_question');
        if ($result = $this->dbGetCatch()) {
            return $result;
        } else {
            return false;
        }
    }
    /***
     * 方法功能：從資料庫找出商品其他照片
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  Object data
     * @return bool
     ***/
    function getQuestionTh()
    {
        $result = array();
        $this->db->select('`aq_question1` as `question1`,`aq_question2` as `question2`,`aq_question3` as `question3`,`aq_question4` as `question4`,`aq_question5` as `question5`,sha1(`aq_key`) as `key`');
        $this->db->from('admin_question');
        if ($result1 = $this->dbGetCatch()) {
            $result[0] = $result1->result_array();
        } else {
            return false;
        }
        $this->db->select('`sq_question_1` as `question1`,`sq_question_2` as `question2`,`sq_question_3` as `question3`,`sq_question_4` as `question4`,`sq_question_5` as `question5`,sha1(`sq_key`) as `key`,`shop_key` as `shop_key`');
        $this->db->from('shop_question');
        if ($result2 = $this->dbGetCatch()) {
            $result[1] = $result2->result_array();
        } else {
            return false;
        }
        return $result;
    }
    /***
     * 方法功能：從資料庫更新商品圖片
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  Object data
     * @return bool
     ***/
    function modifyImg($key)
    {
        $this->db->select('`product_img_src` as `name`,`product_img_key` as `key`');
        $this->db->from('product_img');
        $this->db->where('sha1(product_info_key)', $key);
        if ($result = $this->dbGetCatch()) {
            return $result; 
        } else {
            return false;
        }
    }
}

