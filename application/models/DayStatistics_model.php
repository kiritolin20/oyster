<?php

class DayStatistics_model extends DataBaseError
{

    //DB操作手冊
    //https://codeigniter.org.tw/user_guide/database/active_record.html
    public function __destruct()
    {
        $this->db->close();
    }

    /***
     * 方法功能：從資料庫取得要更改的資料
     * 有查詢到資料就回傳資料,沒有就回傳false
     * @param  String $key
     * @return Object | bool
     ***/
    function getModifyData($key, $info_key)
    {
        $this->db->select('sha1(`product`.`product_key`) as `product_key`,
                            `product.product_name` as product_name,
                            `product_info.product_info_name` as product_info_name,
                            `product.product_introduct` as product_introduce,
                            `product.product_time` as product_time,
                            `product.product_display` as product_display,
                            `product.shop_key`,
                            `product_info_amount` as product_amount,
                            `product_info_price` as product_price,
                            `product_info_display` as product_info_display,
                            sha1(`product_info`.`product_info_key`) as product_info_key,
                            `product_info_img` as product_info_img');
        $this->db->from('product');
        $this->db->where('sha1(product.product_key)', $key);
        $this->db->where('sha1(product_info_key)', $info_key);
        $this->db->join('product_info', 'product_info.product_key=product.product_key');
        if ($result = $this->dbGetCatch()) {
            return $result;
        } else {
            return false;
        }
    }
    /***
     * 方法功能：從資料庫刪除資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  String $key,String $datatable,String $controller
     * @return bool
     ***/
    function deleteData($key)
    {
        $this->db->set('order_display', '0');
        $this->db->where('order_key', $key);
        $this->db->update('order');
        return true;
    }
    /***
     * 方法功能：從資料庫更改資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  String $key,String $datatable,String $controller
     * @return bool
     ***/
    function modifyData($data)
    {
        $this->db->set('product_name', $data['product_name']);
        $this->db->set('product_introduct', $data['product_introduce']);
        $this->db->set('product_display', $data['product_display']);
        $this->db->where('sha1(`product_key`)', $data['product_key']);
        $this->db->update('product');
        $this->db->flush_cache();
        $this->db->set('product_info_amount', $data['product_amount']);
        $this->db->set('product_info_display', $data['product_info_display']);
        $this->db->set('product_info_name', $data['product_info_name']);
        $this->db->set('product_info_price', $data['product_price']);
        $this->db->where('sha1(`product_info_key`)', $data['product_info_key']);
        $this->db->update('product_info');
        return true;
    }
    /**
     * 方法功能：從資料庫取訂單資料(每日各單位金額)
     *
     * @param [type] $year
     * @param [type] $month
     * @param [type] $day
     * @return void
     */
    public function getTurnOverData($year, $month, $day)
    {
        $turnOver = [];
        $this->db->select('SUM(`order_history`.`order_history_price`) as order_history_price, `product`.`product_unit`');
        $this->db->from('order_history');
        $this->db->where('Year(`order`.`order_time`)', $year);
        $this->db->where('Month(`order`.`order_time`)', $month);
        $this->db->where('DAY(`order`.`order_time`)', $day);
        $this->db->where('order_display', '1');
        $this->db->join('order','`order`.`order_key` = `order_history`.`order_key`');
        $this->db->join('product','`order_history`.`product_key` = `product`.`product_key`');
        $this->db->group_by('`product`.`product_unit`');
        if ($result = $this->dbGetCatch()) {
            $result = $result->result_array();
            for ($i=0; $i < count($result); $i++) {
                $turnOver[] = array('unit' => $result[$i]['product_unit'], 'turnover' => $result[$i]['order_history_price']);
            }
        } else {
            return false;
        }
        return $turnOver;
    }

    /**
     * 方法功能：從資料庫取訂單資料(每日各單位總數)
     *
     * @param [type] $year
     * @param [type] $month
     * @param [type] $day
     * @return void
     */
    public function getTotalAmountChart_model($year, $month, $day)
    {
        $turnOver = [];
        $this->db->select('SUM(`order_history`.`order_history_amount`) as order_history_amount, `product`.`product_unit`');
        $this->db->from('order_history');
        $this->db->where('Year(`order`.`order_time`)', $year);
        $this->db->where('Month(`order`.`order_time`)', $month);
        $this->db->where('DAY(`order`.`order_time`)', $day);
        $this->db->where('order_display', '1');
        $this->db->join('order','`order`.`order_key` = `order_history`.`order_key`');
        $this->db->join('product','`order_history`.`product_key` = `product`.`product_key`');
        $this->db->group_by('`product`.`product_unit`');
        if ($result = $this->dbGetCatch()) {
            $result = $result->result_array();
            for ($i=0; $i < count($result); $i++) {
                $turnOver[] = array('unit' => $result[$i]['product_unit'], 'turnover' => $result[$i]['order_history_amount']);
            }
        } else {
            return false;
        }
        return $turnOver;
    }
}
