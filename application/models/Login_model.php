<?php

class Login_model extends DataBaseError{

	//DB操作手冊
	//https://codeigniter.org.tw/user_guide/database/active_record.html
    public function __destruct() {
        $this->db->close();
    }
    
    function checkLogin($account, $password){
        $this->db->select('SHA1(`admin`.`admin_key`) as `key`,
                           admin.`admin_account` as `account`');
        $this->db->from('admin');
        $this->db->where('admin_account',$account);
        $this->db->where('admin_password',md5($password));

        if($result = $this->dbGetCatch()){
            return $result;
        }else{
            return false;
        }
    }

    function checkLineLogin($lineKey)
    {
        $this->db->select('SHA1(`admin`.`admin_key`) as `key`,
                           SHA1(`admin`.`admin_line_key`) as `line_key`,
                           `admin`.`admin_account` as `account`,
                           `admin`.`admin_type` as `type`,
                           `shop`.`shop_key` as `shop_key`');
        $this->db->from('admin');
        $this->db->where('SHA1(`admin`.`admin_line_key`)', sha1($lineKey));
        $this->db->join('shop', '`admin`.`admin_key` = `shop`.`admin_key`');

        if ($result = $this->dbGetCatch()) {
            return $result;
            // return $this->db->last_query();
        } else {
            return false;
        }
    }

    /***
	傳入值為：'account','email'
	方法功能：確認信箱是否重複
	有重複account或email就回傳查詢到的物件 ; 沒有重複回傳0
     ***/
    function checkRepeat($account, $email){
        $judgeAccount = 0;
        $judgeEmail = 0;
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('admin_account', $account);
        if ($accResult = $this->dbGetCatch()) {
            if($accResult->num_rows() == 1){
                $judgeAccount = 1;
            }else{
                $judgeAccount = 0;
            }
        } else {
            return false;
        }
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('admin_email', $email);
        if ($emailResult = $this->dbGetCatch()) {
            if ($emailResult->num_rows() == 1) {
                $judgeEmail = 1;
            } else {
                $judgeEmail = 0;
            }
        } else {
            return false;
        }
        if($judgeAccount == 1){
            return $accResult;
        }else if($judgeEmail == 1){
            return $emailResult;
        }else{
            return $accResult;
        }
        
    }

    function insertAdmin($data){
        if(isset($data['lineKey'])){
            $inserData = array(
                'admin_account' => $data['account'],
                'admin_password' => md5($data['password']),
                'admin_email' => $data['email'],
                'admin_bank' => $data['admin_bank'],
                'admin_type' => '1',
                'admin_line_key' => $data['lineKey']
            );
        }else{
            $inserData = array(
                'admin_account' => $data['account'],
                'admin_password' => md5($data['password']),
                'admin_bank' => $data['admin_bank'],
                'admin_email' => $data['email'],
                'admin_type' => '1'
            );
        }
        $this->db->insert('admin', $inserData);
        $key = $this->db->insert_id();
        $this->db->flush_cache();
        $inserData = array(
            'shop_name' => $data['shop_name'],
            'shop_address' => $data['shop_address'],
            'shop_introduct' => $data['shop_introduct'],
            'shop_phone' => $data['shop_phone'],
            'admin_key' => $key
        );
        $this->db->insert('shop', $inserData);
    }

    function setSession($dataObject){
        $_SESSION['key'] = $dataObject->key;
        $_SESSION['member_account'] = $dataObject->account;
    }

}
