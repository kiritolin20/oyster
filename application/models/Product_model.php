<?php

class Product_model extends DataBaseError
{

    //DB操作手冊
    //https://codeigniter.org.tw/user_guide/database/active_record.html
    public function __destruct()
    {
        $this->db->close();
    }


    function insertProduct($data)
    {
        $productData = array(
            'product_name' => $data['product_name'],
            'product_info' => $data['product_info'],
            'product_display' => 1,
            'product_unit' => $data['product_unit']
        );

        $this->db->insert('product', $productData);
        $key = $this->db->insert_id();
        $this->db->flush_cache();
        $priceData = array(
            'ph_price' => $data['ph_price'],
            'ph_time' => date("Y-m-d  H:i:s"),
            'product_key' => $key
        );

        $this->db->insert('price_history', $priceData);
        return sha1($key);
        
    }

    /***
     * 方法功能：從資料庫取得要更改的資料
     * 有查詢到資料就回傳資料,沒有就回傳false
     * @param  String $key
     * @return Object | bool
     ***/
    function getModifyData($key)
    {
        $this->db->select('sha1(`product`.`product_key`) as `product_key`,
                            `product.product_name` as product_name,
                            `product.product_info` as product_info,
                            `product_unit` as product_unit,
                            ( SELECT price_history.ph_price
                              FROM price_history
                              WHERE `price_history`.product_key = `product`.product_key
                              ORDER BY price_history.ph_time DESC
                              LIMIT 1) as `ph_price`,
                            `product_img` as product_img');
        $this->db->from('product');
        $this->db->join('price_history', '`price_history`.`product_key` = `product`.`product_key`');
        $this->db->where('sha1(product.product_key)', $key);
        if ($result = $this->dbGetCatch()) {
            return $result;
        } else {
            return false;
        }
    }
    /***
     * 方法功能：從資料庫刪除資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  String $key,String $datatable,String $controller
     * @return bool
     ***/
    function deleteData($key)
    {
        $this->db->set('product_display',0);
        $this->db->where('sha1(product_key)',$key);

        $this->db->update('product');
        return true;
    }
    /***
     * 方法功能：從資料庫更改資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  String $key,String $datatable,String $controller
     * @return bool
     ***/
    function modifyData($data)
    {
        $this->db->set('product_name', $data['product_name']);
        $this->db->set('product_info', $data['product_info']);
        $this->db->set('product_unit', $data['product_unit']);
        $this->db->where('sha1(`product_key`)', $data['product_key']);
        $this->db->update('product');
        
        $this->db->flush_cache();

        $this->db->select('`product`.`product_key` as `product_key`');
        $this->db->from('product');
        $this->db->join('price_history', '`price_history`.`product_key` = `product`.`product_key`');
        $this->db->where('sha1(product.product_key)', $data['product_key']);
        if ($result = $this->dbGetCatch()) {
            
        } else {
            return false;
        }

        $this->db->flush_cache();

        $priceData = array(
            'ph_price' => $data['ph_price'],
            'ph_time' => date("Y-m-d  H:i:s"),
            'product_key' => $result->row()->product_key
        );

        $this->db->insert('price_history', $priceData);
        return true;
    }
    /***
     * 方法功能：從資料庫刪除資料
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  Object data
     * @return bool
     ***/
    function modifyInfoImg($data)
    {
        $this->db->set('product_img', $data['img']);
        $this->db->where('sha1(`product_key`)', $data['key']);
        $this->db->update('product');
        return true;
    }
    /***
     * 方法功能：從資料庫找出商品其他照片
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  Object data
     * @return bool
     ***/
    function getModifyImg($key)
    {
        $this->db->select('`product_img_src` as `name`,`product_img_key` as `key`,sha1(`product_key`) as `product_key`');
        $this->db->from('product_img');
        $this->db->where('sha1(`product_key`)', $key);
        if ($result = $this->dbGetCatch()) {
            return $result;
        } else {
            return false;
        }
    }
    /***
     * 方法功能：從資料庫更新商品圖片
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  Object data
     * @return bool
     ***/
    function modifyImg($data)
    {
        $this->db->where('`product_key`', $data->key);
        $this->db->delete('product_img');
        $this->db->flush_cache();
        for($i=0;$i<$data->length;$i++){
            $this->db->set('product_img_src', $data->img[$i]);
            $this->db->set('product_key', $data->key);
            $this->db->insert('product_img');
            $this->db->flush_cache();
        }
        
        return true;
    }
}
