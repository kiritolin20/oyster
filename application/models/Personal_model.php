<?php

class Personal_model extends DataBaseError
{

    //DB操作手冊
    //https://codeigniter.org.tw/user_guide/database/active_record.html
    public function __destruct()
    {
        $this->db->close();
    }

    /***
     * 方法功能：從資料庫取得要更改的資料
     * 有查詢到資料就回傳資料,沒有就回傳false
     * @param  String $key
     * @return Object | bool
     ***/
    function getShop()
    {
        $this->db->select('`shop.shop_name` as shop_name,
                            `shop.shop_address` as shop_address,
                            `shop.shop_introduct` as shop_introduct,
                            `shop.shop_phone` as shop_phone,
                            `admin.admin_bank` as admin_bank');
        $this->db->from('shop');
        $this->db->where('`shop`.`shop_key`', $_SESSION['shop_key']);
        $this->db->join('admin','`admin`.`admin_key` = `shop`.`admin_key`');
        if ($result = $this->dbGetCatch()) {
            return $result;
        } else {
            return false;
        }
    }

    /***
     * 方法功能：從資料庫取得要更改的資料
     * 有查詢到資料就回傳資料,沒有就回傳false
     * @param  String $key
     * @return Object | bool
     ***/
    function checkPassword($data)
    {
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('`admin_password`', md5($data['admin_password']));
        $this->db->where('sha1(`admin_key`)', $_SESSION['key']);
        if ($result = $this->dbGetCatch()) {
            return $result;
        } else {
            return false;
        }
    }

    /***
     * 方法功能：從資料庫更改賣家密碼
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  String $key,String $datatable,String $controller
     * @return bool
     ***/
    function modifyPassword($data)
    {
        $this->db->set('admin_password', md5($data['editPassword']));
        $this->db->where('sha1(`admin_key`)', $_SESSION['key']);
        $this->db->update('admin');
        return true;
    }

    /***
     * 方法功能：從資料庫更改賣家密碼
     * 從資料庫中刪除資料,沒有就回傳false
     * @param  String $key,String $datatable,String $controller
     * @return bool
     ***/
    function modifyShop($data)
    {
        $this->db->set('shop_name', $data['shop_name']);
        $this->db->set('shop_address', $data['shop_address']);
        $this->db->set('shop_introduct', $data['shop_introduct']);
        $this->db->set('shop_phone', $data['shop_phone']);
        $this->db->where('`shop`.`shop_key`', $_SESSION['shop_key']);
        $this->db->update('shop');
        $this->db->flush_cache();
        $this->db->set('admin_bank', $data['admin_bank']);
        $this->db->where('sha1(`admin_key`)', $_SESSION['key']);
        $this->db->update('admin');
        $this->db->flush_cache();
        return true;
    }
}
