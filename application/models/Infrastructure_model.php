<?php

class Infrastructure_model extends DataBaseError{

    public function __construct(){
        parent::__construct();
    }

	//DB操作手冊
	//https://codeigniter.org.tw/user_guide/database/active_record.html
    public function __destruct() {
        $this->db->close();
    }

    function getMenu($permissionKey){
        $this->db->select('menu.`name`,menu.`router`,menu.`icon`');
        $this->db->from('menu');
        $this->db->join('menu_permission','menu_key = menu.`key`');
        $this->db->where('SHA1(menu_permission.permission_key)',$permissionKey);
        $query = $this->db->get();
        $data = array();
        foreach ($query->result() as $row){
        	$data[] = array(
        		'name' => $row->name,
        		'router' => $row->router,
        		'icon' => $row->icon
        	);
		}
        return $data;
    }

    function getKeyDecode($encode,$keyName){
        $this->db->select('`key`');
        $this->db->from($keyName);
        $this->db->where('SHA1(`key`)',$encode);
        if($result = $this->dbGetCatch()){
            return $result->row()->key;
        }else{
            return false;
        } 
    }

}
